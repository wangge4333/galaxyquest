﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreconNormal : GQ_BTPrecondition
{
    protected AI_CargoShip ai_target = null;

    public PreconNormal(AI_CargoShip ai_target)
    {
        this.ai_target = ai_target;
    }

    public override bool Precondition()
    {
        if (ai_target.IsUnderAttack)
            return false;
        return true;
    }
}

public class PreconUnderAttack : GQ_BTPrecondition
{
    protected AI_CargoShip ai_target = null;

    public PreconUnderAttack(AI_CargoShip ai_target)
    {
        this.ai_target = ai_target;
    }

    public override bool Precondition()
    {
        if (ai_target.IsUnderAttack)
            return true;
        return false;
    }
}

public enum AI_DIROffset
{
    UP,DOWN,LEFT,RIGHT,UPLEFT,UPRIGHT,DOWNLEFT,DOWNRIGHT,NAN
}

public class AI_CargoShip : AI_ShipBase
{
    protected ShipCargo cargoHold = null;
    protected Interact interact = null;

    protected PlanetManager marketManager = null;

    [SerializeField]
    protected int holdItemID = 0;
    [SerializeField]
    protected PlanetMarket currentTarget = null;
    protected PlanetMarket nextTarget = null;

    [SerializeField]
    protected bool isArrived = false;
    [SerializeField]
    protected float parkTime = 5.0f;
    protected float parkTimer = 0.0f;

    [SerializeField]
    protected bool isUnderAttack = false;
    public bool IsUnderAttack { get => isUnderAttack; }
    protected bool calledGuard = false;
    public SpaceObject attacker = null;
    protected AI_PlanetStation escapeTo = null;
    protected AI_GuardShip contactGuard = null;
    protected float escapedConfirmTime = 120.0f;
    protected float escapeTimer = 0;

    protected override void Init()
    {
        base.Init();
        cargoHold = GetComponent<ShipCargo>();
        marketManager = FindObjectOfType<PlanetManager>();
        interact = GetComponent<Interact>();

        CargoShipTree();
    }

    protected void CargoShipTree()
    {
        //not completed yet

        //trade status
        PreconNormal tradeCondition = new PreconNormal(this);
        GQ_SequenceNode tradeNode = new GQ_SequenceNode("NormalTrade");
        tradeNode.precondition = tradeCondition;
        GQ_ActionNode choosePlanet = new GQ_ActionNode("NearestPlanet", AI_ChoosePlanet, 0);
        tradeNode.AddChild(choosePlanet);
        GQ_ActionNode sailNode = new GQ_ActionNode("Sail", AI_Sail_Des, 1);
        tradeNode.AddChild(sailNode);
        GQ_ActionNode chooseItemNplanet = new GQ_ActionNode("ChooseItemNPlanet", AI_ChooseItemNPlanet, 2);
        tradeNode.AddChild(chooseItemNplanet);
        GQ_ActionNode buyItem = new GQ_ActionNode("Buy", AI_Buy, 3);
        tradeNode.AddChild(buyItem);
        GQ_ActionNode sailNodeAlt = new GQ_ActionNode("Sail", AI_Sail_Des, 4);
        tradeNode.AddChild(sailNodeAlt);
        GQ_ActionNode sellItem = new GQ_ActionNode("Sell", AI_Sell, 5);
        tradeNode.AddChild(sellItem);

        //escape status
        PreconUnderAttack escapeCondition = new PreconUnderAttack(this);
        GQ_ParallelFlex escapeNode = new GQ_ParallelFlex("Escape");
        escapeNode.precondition = escapeCondition;
        GQ_ActionNode contactGuard = new GQ_ActionNode("ContactGuard", AI_ContactGuard, 0);
        escapeNode.AddChild(contactGuard);
        GQ_ActionNode escapeToStation = new GQ_ActionNode("Escape", AI_Sail_Battle, 1);
        escapeNode.AddChild(escapeToStation);
        GQ_ActionNode checkSafe = new GQ_ActionNode("CheckSafe", AI_CheckEscaped, 2);
        escapeNode.AddChild(checkSafe);

        //entry
        GQ_SelectorNode root = new GQ_SelectorNode("Root");
        root.AddChild(tradeNode);
        root.AddChild(escapeNode);

        entry = root;
    }

    protected override void Start()
    {
        base.Start();
        SetRunningStatus(true);
        spaceObject.onBeingAttacked = BeingAttacked;
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();
        pathFindingTimer = pathFindingGap;
    }

    protected override void ExtraUpdate()
    {
        
    }

    public void Arrived(PlanetMarket market)
    {
        if (currentTarget == market)
        {
            SetArrived(true);
        }
            
    }

    protected void SetArrived(bool isArrived)
    {
        if (!this.isArrived && isArrived) 
        {
            parkTimer = parkTime;
            interact.StartFollowPlanet();
        }
        if (this.isArrived && !isArrived) 
        {
            interact.EndFollowPlanet();
        }
        this.isArrived = isArrived;
    }

    public GQ_RunStatus AI_ChoosePlanet()
    {
        if (marketManager == null)
            marketManager = FindObjectOfType<PlanetManager>();
        if (marketManager == null)
            return GQ_RunStatus.Fail;
        SetArrived(false);
        currentTarget = marketManager.NearestMarket(transform);
        return GQ_RunStatus.Success;
    }

    protected GQ_RunStatus AI_ChooseItemNPlanet()
    {
        ItemNPlanet temp = marketManager.RandomlyGetItemNPlanet(currentTarget.MarketID);
        if (temp == null)
            return GQ_RunStatus.Fail;
        nextTarget = marketManager.GetMarket(temp.planetID);
        holdItemID = temp.itemID;
        return GQ_RunStatus.Success;
    }

    public GQ_RunStatus AI_Buy()
    {
        //taking over and wait for several seconds
        parkTimer -= Time.deltaTime;
        if (parkTimer > 0)
            return GQ_RunStatus.Running;

        while (true)
        {
            int itemAmount = Mathf.Min(cargoHold.MaxBuyableAmount(holdItemID, currentTarget),
                currentTarget.GetSellableAmount(holdItemID));
            currentTarget.Sell(holdItemID, itemAmount, spaceObject.ID);

            if (cargoHold.FreeSpace < 5.0f ||
                currentTarget.GetSellableAmount(holdItemID) == 0 ||
                cargoHold.MaxBuyableAmount(holdItemID, currentTarget) == 0)
                break;
            
        }
        currentTarget = nextTarget;
        SetArrived(false);
        return GQ_RunStatus.Success;
    }

    //for AI, sail to a destination
    public GQ_RunStatus AI_Sail_Des()
    {
        AI_CheckObstacle(currentTarget.gameObject);
        control.SailTo(nextDes);

        if (isArrived)
        {
            control.ResetAllThrottle();
            return GQ_RunStatus.Success;
        }
        return GQ_RunStatus.Running;
    }
    

    public GQ_RunStatus AI_Sell()
    {
        //taking over and wait for several seconds
        parkTimer -= Time.deltaTime;
        if (parkTimer > 0)
            return GQ_RunStatus.Running;

        List<int> inventory = new List<int>();

        foreach (KeyValuePair<int,int> items in cargoHold.EcoItems)
        {
            inventory.Add(items.Key);
        }

        for(int i = 0; i < inventory.Count; ++i)
        {
            while (cargoHold.EcoItems.ContainsKey(inventory[i])){
                int itemAmount = Mathf.Min(cargoHold.EcoItems[inventory[i]],
                    currentTarget.GetBuyableAmount(inventory[i]));
                currentTarget.Buy(inventory[i], itemAmount, spaceObject.ID);
            }
        }

        return GQ_RunStatus.Success;
    }

    //for AI, sail control in battle
    public GQ_RunStatus AI_Sail_Battle()
    {
        //去最近的“警察局”
        AI_CheckObstacle(escapeTo.gameObject);
        control.SailTo(nextDes);

        return GQ_RunStatus.Running;
    }

    //for AI, cargo ship
    public GQ_RunStatus AI_ContactGuard()
    {
        if (contactGuard == null)
            return GQ_RunStatus.Fail;
        contactGuard.GetContact(new AI_ContactPacket(spaceObject, attacker, transform.position));
        return GQ_RunStatus.Running;
    }

    public GQ_RunStatus AI_CheckEscaped()
    {
        escapeTimer -= Time.deltaTime;
        if (escapeTimer < 0)
        {
            AI_SuccessfullyEscaped();
            return GQ_RunStatus.Success;
        }
            
        return GQ_RunStatus.Running;
    }

    protected void AI_SuccessfullyEscaped()
    {
        calledGuard = false;
        isUnderAttack = false;
        attacker = null;
        escapeTo = null;
        contactGuard = null;
        escapeTimer = 0;
    }

    public void BeingAttacked(SpaceObject attacker)
    {
        if (calledGuard)
            return;
        calledGuard = true;
        isUnderAttack = true;
        this.attacker = attacker;
        escapeTo = marketManager.NearestStation(transform);
        contactGuard = escapeTo.AssignGuard(new AI_ContactPacket(spaceObject, attacker, transform.position));
        escapeTimer = escapedConfirmTime;
    }

}
