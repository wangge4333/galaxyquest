﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_ContactPacket
{
    public SpaceObject hostile = null;
    public SpaceObject victim = null;
    public Vector3 location;

    public AI_ContactPacket(SpaceObject victim, SpaceObject hostile,Vector3 location)
    {
        this.victim = victim;
        this.hostile = hostile;
        this.location = location;
    }
}

public class AI_GuardShip : AI_ShipBase
{
    [SerializeField]
    protected AI_PlanetStation belongPlanet = null;
    protected FireControl fireControl = null;

    //for patrol
    [SerializeField]
    protected Vector3 patrolPos = new Vector3(0, 0, 0);
    public Vector3 PatrolPos { get => patrolPos + belongPlanet.transform.position; }
    [SerializeField]
    protected float patrolThrottle = 0.8f;

    //patrol of just stay at there?
    [SerializeField]
    protected bool isPatrolStyle = true;
    [SerializeField, Tooltip("Not in kM. AI will get into alert status if been contacted by cargo ships.")]
    protected float detectRange = 80.0f;
    [SerializeField,
        Tooltip("Not in kM. AI will give up chasing if the distance between itself and target is higher than outRange.")]
    protected float giveUpRange = 100.0f;
    [SerializeField,
        Tooltip("Not in kM. " +
        "AI will give up chasing if the distance between itself and belong planet is higher than this range")]
    protected float maxChasingRange = 200.0f;

    //for investigation
    protected Vector3 location = new Vector3(0, 0, 0);
    protected bool isAlerted = false;                           //been contacted by others
    public bool IsAlerted { get => isAlerted; }
    protected bool foundHostile = false;                        //find any hostile by itself
    public bool FoundHostile { get => foundHostile; }

    //for battle
    protected SpaceObject hostileTarget = null;
    protected Rigidbody hostileRigid = null;
    protected float posChangeTime = 25.0f;
    [SerializeField]
    protected float posChangeTimer = 0.0f;
    [SerializeField]
    protected Vector3 absPos = new Vector3(0, 0, 0);            //the pos guard ship need to be in target's space

    // AI will random find a position away from target in minmaxpair while in battle to sail
    [SerializeField]
    protected float minEngageDis = 5.0f;
    [SerializeField]
    protected float maxEngageDis = 40.0f;
      
    //different from cargoship
    [SerializeField]
    protected bool isArrived = false;

    [SerializeField, Range(0.0f, 1.0f),
        Tooltip("Desire to attack, higher means more frequent.")]
    protected float atkDesire = 0.6f;
    [SerializeField]
    protected float attackGap = 5.0f;
    protected float attackTimer = 0;

    //for optimization
    protected List<Collider> colliders = new List<Collider>();

    protected override void Init()
    {
        base.Init();
        fireControl = GetComponent<FireControl>();
        spaceObject.onDie = OnDie;
        foreach(Collider c in GetComponents<Collider>())
        {
            colliders.Add(c);
        }
        spaceObject.onBeingAttacked = CheckHostileObject;

        GuardTree();
    }

    protected override void ExtraUpdate()
    {
        CheckIfRun();
        base.ExtraUpdate();
        CheckIfFCUpdate();
        SetFireControlTarget();
    }

    public void SetName(string name)
    {
        spaceObject.objName = name;
    }

    protected void GuardTree()
    {
        //Patrol
        Precon_Patrol precon_Patrol = new Precon_Patrol(this);
        GQ_SequenceNode patrolNode = new GQ_SequenceNode("Patrol", 0);
        patrolNode.precondition = precon_Patrol;
        GQ_ActionNode generateDes = new GQ_ActionNode("GenerateDestination", AI_GeneratePatrolDes, 0);
        patrolNode.AddChild(generateDes);
        GQ_ActionNode sail_patrol = new GQ_ActionNode("Sail to Destination", AI_Sail_Patrol, 1);
        patrolNode.AddChild(sail_patrol);

        //being alerted
        Precon_GetContact precon_Alerted = new Precon_GetContact(this);
        GQ_SelectorNode alertedNode = new GQ_SelectorNode("Alerted", 1);
        alertedNode.precondition = precon_Alerted;
        GQ_ActionNode checkDeath_0 = new GQ_ActionNode("CheckTargetDeath", AI_CheckTargetDeath, 0);
        alertedNode.AddChild(checkDeath_0);

        GQ_SequenceNode alerted_actions = new GQ_SequenceNode("AlertedActions", 1);
        alertedNode.AddChild(alerted_actions);

        GQ_ParallelFlex sail_Location = new GQ_ParallelFlex("SailToLocation", 0);
        alerted_actions.AddChild(sail_Location);
        //Sail to location that attack happened
        GQ_ActionNode sail2Location = new GQ_ActionNode("SailToLocaiton", AI_Sail_Location, 1);
        sail_Location.AddChild(sail2Location);
        GQ_ActionNode checkAttacker = new GQ_ActionNode("CheckAttacker", AI_CheckTarget, 0);
        sail_Location.AddChild(checkAttacker);
        //battle alerted
        GQ_SequenceNode alerted_battle = new GQ_SequenceNode("Alerted_Battle", 1);
        alerted_actions.AddChild(alerted_battle);
        GQ_ParallelNode AL_battleChase = new GQ_ParallelNode("AL_chase", 0);
        alerted_battle.AddChild(AL_battleChase);
        GQ_ActionNode AL_chase = new GQ_ActionNode("AL_chase", AI_Chase, 1);
        AL_battleChase.AddChild(AL_chase);
        GQ_ActionNode AL_CheckRange = new GQ_ActionNode("AL_CheckChasingRange", AI_CheckChasingRange, 0);
        AL_battleChase.AddChild(AL_CheckRange);
        //fight
        GQ_ParallelNode AL_battle = new GQ_ParallelNode("AL_Battle", 1);
        alerted_battle.AddChild(AL_battle);
        GQ_ActionNode AL_battle_dis = new GQ_ActionNode("AL_battle_keepdis", AI_KeepRelativePos, 1);
        AL_battle.AddChild(AL_battle_dis);
        GQ_ActionNode AL_fire = new GQ_ActionNode("AL_battle_fire", AI_Fire, 2);
        AL_battle.AddChild(AL_fire);
        GQ_ActionNode AL_firingRange = new GQ_ActionNode("AL_battle_firingRange", AI_CheckFightRange, 0);
        AL_battle.AddChild(AL_firingRange);


        //entry
        GQ_SelectorNode root = new GQ_SelectorNode("Root");
        root.AddChild(patrolNode);
        root.AddChild(alertedNode);
        entry = root;
    }

    public void SetBelongPlanet(AI_PlanetStation planet)
    {
        belongPlanet = planet;
    }

    protected override void Start()
    {
        base.Start();
        SetRunningStatus(true);
    }

    protected bool CheckArrived(Vector3 des)
    {
        if (Algo.IsApproximate(des, transform.position, 2.5f)) 
            return true;
        return false;
    }

    protected void SetArrived(bool isArrived)
    {
        this.isArrived = isArrived;
    }

    protected void OnDie()
    {
        belongPlanet.DeregisterGuard(this);
    }

    protected GQ_RunStatus AI_GeneratePatrolDes()
    {
        if (belongPlanet == null)
            return GQ_RunStatus.Fail;
        patrolPos = belongPlanet.RandomGetPatrolPos();
        SetArrived(false);
        return GQ_RunStatus.Success;
    }

    //different from cargo ship
    protected GQ_RunStatus AI_Sail_Patrol()
    {
        if (isArrived)
            return GQ_RunStatus.Success;

        AI_CheckObstacle(PatrolPos);
        control.SailTo(nextDes, patrolThrottle);

        if (CheckArrived(PatrolPos))
        {
            control.ResetAllThrottle();
            SetArrived(true);
            return GQ_RunStatus.Success;
        }
        SetArrived(false);
        return GQ_RunStatus.Running;
    }

    //this will be excuted with AI_CheckTarget, if guard gets the location and did not find enemy, it wil fail 
    protected GQ_RunStatus AI_Sail_Location()
    {
        AI_CheckObstacle(location);
        control.SailTo(nextDes);

        if (CheckArrived(location))
        {
            control.ResetAllThrottle();
            SetArrived(true);
            isAlerted = false;
            return GQ_RunStatus.Fail;
        }

        return GQ_RunStatus.Running;
    }

    //CHECK IF TARGET IN THE DETECT RANGE
    protected GQ_RunStatus AI_CheckTarget()
    {
        
        if (foundHostile)
            return GQ_RunStatus.Success;

        if (Vector3.Distance(hostileTarget.transform.position, transform.position) < detectRange) 
        {
            foundHostile = true;
            return GQ_RunStatus.Success;
        }
            
        return GQ_RunStatus.Running;
    }


    //Check if any hostile appeared in detect range
    protected GQ_RunStatus AI_CheckHostile()
    {
        //there is a list of pirate

        return GQ_RunStatus.Success;
    }

    //Chase target if target in chasing distance
    protected GQ_RunStatus AI_Chase()
    {
        AI_CheckObstacle(hostileTarget.transform.position);
        control.SailTo(nextDes);

        if (CheckIfInFiringRange(hostileTarget.transform.position))
        {
            control.ResetAllThrottle();
            return GQ_RunStatus.Success;
        }
        return GQ_RunStatus.Running;
    }

    //check if target out of chasing range
    protected GQ_RunStatus AI_CheckChasingRange()
    {
        if(Vector3.Distance(hostileTarget.transform.position,transform.position)> giveUpRange||
            Vector3.Distance(belongPlanet.transform.position, transform.position) > maxChasingRange)
        {
            SetPatrolStatus();
            Debug.Log("Out of chasing area");
            return GQ_RunStatus.Fail;
        }
        return GQ_RunStatus.Success;
    }

    protected bool CheckIfInFiringRange(Vector3 targetPos)
    {
        if (Vector3.Distance(targetPos, transform.position) < maxEngageDis)
            return true;
        return false;
    }

    //keep distance
    protected GQ_RunStatus AI_KeepRelativePos()
    {
        //保持目标点于目标速度方向为正方向的前半球内的位置？
        posChangeTimer -= Time.deltaTime;
        if (posChangeTimer < 0)
        {
            AI_CalBattlePos();
        }

        AI_CheckObstacle(absPos+hostileTarget.transform.position);
        control.SailTo(nextDes);

        if (!CheckIfInFiringRange(hostileTarget.transform.position))
        {
            Debug.Log("Keep_Fail");
            return GQ_RunStatus.Fail;
        }
        
        return GQ_RunStatus.Running;
    }

    //calculate the pos need to sail in battle
    protected void AI_CalBattlePos()
    {
        posChangeTimer = posChangeTime;
        float dis = Random.Range(minEngageDis, maxEngageDis);
        absPos= Quaternion.Euler(Random.Range(-90.0f, 90.0f), Random.Range(-90.0f, 90.0f), 0) *
            hostileRigid.velocity.normalized * dis;
    }

    //check if target out of fire range
    protected GQ_RunStatus AI_CheckFightRange()
    {
        if (CheckIfInFiringRange(hostileTarget.transform.position))
            return GQ_RunStatus.Success;
        Debug.Log("Out of firing range.");
        return GQ_RunStatus.Fail;
    }

    protected GQ_RunStatus AI_CheckTargetDeath()
    {
        if (hostileTarget.IsDead)
        {
            SetPatrolStatus();
            Debug.Log("Target down.");
            return GQ_RunStatus.Success;
        }
           
        return GQ_RunStatus.Fail;
    }

    protected GQ_RunStatus AI_Fire()
    {
        fireControl.AI_FireCannon();
        attackTimer -= Time.deltaTime;

        if (hostileTarget.IsDead)
            return GQ_RunStatus.Success;

        if (attackTimer > 0)
            return GQ_RunStatus.Running;
        if (Random.value < atkDesire)
        {
            fireControl.AI_LaunchMissile();
        }
        return GQ_RunStatus.Running;
    }

    protected void SetPatrolStatus()
    {
        isAlerted = false;
        foundHostile = false;
        hostileTarget = null;
        hostileRigid = null;
        fireControl.ResetLockedTarget();

    }

    protected void SetFireControlTarget()
    {
        if (hostileTarget != null)
        {
            fireControl.SetLockedTarget(hostileTarget.gameObject);
            fireControl.PointPos = fireControl.PredictedPos;
            return;
        }
        fireControl.PointPos = transform.position + transform.forward * 10.0f;
    }

    public void GetContact(AI_ContactPacket info)
    {
        isAlerted = true;
        location = info.location;
        hostileTarget = info.hostile;
        hostileRigid = hostileTarget.GetComponent<Rigidbody>();
    } 

    public SpaceObject ReplyContact()
    {
        return this.GetComponent<SpaceObject>();
    }


    //if no need, dont update firecontrol
    protected void CheckIfFCUpdate()
    {
        if (fireControl.ifRun &&
            Vector3.Distance(transform.position, gameManager.MainPlayer.transform.position) > 200.0f)
        {
            fireControl.ifRun = false;
            return;
        }
        if (!fireControl.ifRun &&
            Vector3.Distance(transform.position, gameManager.MainPlayer.transform.position) <= 200.0f)
        {
            fireControl.ifRun = true;
        }
    }

    protected void CheckIfRun()
    {
        if ((!isRunning && isAlerted)||
            Vector3.Distance(transform.position, belongPlanet.transform.position) > belongPlanet.areaDis) 
        {
            SetRunningStatus(true);
            SetColliders(true);
            return;
        }

        if (isRunning && Vector3.Distance(transform.position, gameManager.MainPlayer.transform.position) > 200.0f)
        {
            SetRunningStatus(false);
            SetColliders(false);
            return;
        }
        if (!isRunning && Vector3.Distance(transform.position, gameManager.MainPlayer.transform.position) <= 200.0f)
        {
            SetRunningStatus(true);
            SetColliders(true);
        }
    }

    protected void SetColliders(bool status)
    {
        foreach(Collider c in colliders)
        {
            c.enabled = status;
        }
    }

    protected void CheckHostileObject(SpaceObject hostileObject)
    {
        isAlerted = true;
        location = hostileObject.transform.position;
        hostileTarget = hostileObject;
        hostileRigid = hostileTarget.GetComponent<Rigidbody>();
    }

}

public class Precon_Patrol : GQ_BTPrecondition
{
    protected AI_GuardShip ai_target = null;

    public Precon_Patrol(AI_GuardShip ai_target)
    {
        this.ai_target = ai_target;
    }

    public override bool Precondition()
    {
        if (ai_target.IsAlerted || ai_target.FoundHostile)
            return false;
        return true;
    }
}

public class Precon_GetContact : GQ_BTPrecondition
{
    protected AI_GuardShip ai_target = null;

    public Precon_GetContact(AI_GuardShip ai_target)
    {
        this.ai_target = ai_target;
    }

    public override bool Precondition()
    {
        if (ai_target.IsAlerted)
            return true;
        return false;
    }
}

public class Precon_SeeHostile : GQ_BTPrecondition
{
    protected AI_GuardShip ai_target = null;

    public Precon_SeeHostile(AI_GuardShip ai_target)
    {
        this.ai_target = ai_target;
    }

    public override bool Precondition()
    {
        if (!ai_target.IsAlerted && ai_target.FoundHostile)
            return true;
        return false;
    }
}