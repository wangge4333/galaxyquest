﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_PlanetStation : MonoBehaviour
{
    protected PlanetAttribute planet = null;

    protected PlanetManager planetManager = null;
    protected GameManager gameManager = null;

    [HideInInspector]
    public float MINDIS = 0;
    [HideInInspector]
    public float MAXDIS = 0;
    public float offset = 0.5f;
    public float areaDis = 20.0f;

    protected List<AI_GuardShip> guardList = new List<AI_GuardShip>();
    protected int generatedNum = 1;

    [SerializeField]
    protected int guardNum = 10;

    //temp
    [SerializeField]
    protected GameObject guardType = null;

    protected void Start()
    {
        Initializaiton();
        GenerateGuards();
    }

    protected void Update()
    {
        SupplementGuard();
    }

    protected void Initializaiton()
    {
        planetManager = GetComponentInParent<PlanetManager>();
        gameManager = FindObjectOfType<GameManager>();
        planet = GetComponent<PlanetAttribute>();
        planetManager.RegisterPlanetStation(planet.PlanetID, this);

        CheckRadius();
    }

    protected void CheckRadius()
    {
        MINDIS = GetComponent<Collider>().bounds.size.x / 2.0f + offset;
        MAXDIS = MINDIS + areaDis;
    }

    //the returned value should be added with the planet's postion
    public Vector3 RandomGetPatrolPos()
    {
        return (Random.onUnitSphere * Random.Range(MINDIS, MAXDIS));
    }

    protected void GenerateGuards()
    {
        if (guardType == null)
            return;
        for(int i = 0; i < guardNum; ++i)
        {
            GenerateGuard();
        }
    }

    protected void SupplementGuard()
    {
        if (guardType == null)
            return;
        if (guardList.Count >= guardNum)
            return;
        GenerateGuard();
    }

    protected void GenerateGuard()
    {
        AI_GuardShip temp =
            Instantiate(guardType, RandomGetPatrolPos() + transform.position, Quaternion.Euler(0, 0, 0), gameManager.NPCRoot).GetComponent<AI_GuardShip>();
        guardList.Add(temp);
        temp.SetBelongPlanet(this);
        temp.SetName(name + "_GuardShip_" + generatedNum.ToString());
        ++generatedNum;
    }

    public AI_GuardShip AssignGuard(AI_ContactPacket message)
    {
        foreach(AI_GuardShip gs in guardList)
        {
            if(!gs.IsAlerted)
            {
                //tell guard
                gs.GetContact(message);
                return gs;
            }
        }
        return null;
    }

    public void DeregisterGuard(AI_GuardShip guard)
    {
        if (!guardList.Contains(guard))
            return;
        guardList.Remove(guard);
    }

}
