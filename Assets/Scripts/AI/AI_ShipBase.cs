﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_ShipBase : GQ_BTCusTick
{
    protected GameManager gameManager = null;

    protected Vector3 nextDes = new Vector3(0, 0, 0);

    protected SpaceshipControl control = null;
    protected SpaceObject spaceObject = null;

    [SerializeField]
    protected float pathFindingGap = 1.0f;
    protected float pathFindingTimer = 0;

    [SerializeField]
    protected LayerMask obstacleLayer = new LayerMask();
    protected AI_DIROffset dirOffset = AI_DIROffset.NAN;

    protected override void Start()
    {
        base.Start();
        ChooseRandomDirection();
    }

    protected override void Init()
    {
        base.Init();
        control = GetComponent<SpaceshipControl>();
        spaceObject = GetComponent<SpaceObject>();
        gameManager = FindObjectOfType<GameManager>();
    }

    protected override void ExtraUpdate()
    {
        base.ExtraUpdate();
        CheckIfEngineEffect();
    }

    //simple path finding, just raycast.
    protected void AI_CheckObstacle(Vector3 desPos)
    {
        pathFindingTimer -= Time.deltaTime;
        if (pathFindingTimer > 0)
            return;
        pathFindingTimer = pathFindingGap;

        Ray ray = new Ray(transform.position, desPos - transform.position);
        RaycastHit hitInfo;
        bool isHit = Physics.Raycast(ray, out hitInfo, Vector3.Distance(transform.position, desPos), obstacleLayer);
        if (isHit)
        {
            nextDes = hitInfo.point + GetDirectionOffset(dirOffset, ray.direction) * 3.0f;
        }
        else
            nextDes = desPos;  
    }

    protected void AI_CheckObstacle(GameObject targetObject)
    {
        pathFindingTimer -= Time.deltaTime;
        if (pathFindingTimer > 0)
            return;
        pathFindingTimer = pathFindingGap;

        Ray ray = new Ray(transform.position, targetObject.transform.position - transform.position);
        RaycastHit hitInfo;
        bool isHit = Physics.Raycast(ray, out hitInfo, float.MaxValue, obstacleLayer);
        if (isHit)
        {
            if (hitInfo.collider.gameObject == targetObject)
                nextDes = hitInfo.point;
            else
                nextDes = hitInfo.point + GetDirectionOffset(dirOffset, ray.direction) * 3.0f;
        }
        else
            nextDes = targetObject.transform.position;             //won't happen, just in case
    }

    protected void ChooseRandomDirection()
    {
        int temp = Random.Range(0, 8);
        switch (temp)
        {
            case 0:
                dirOffset = AI_DIROffset.UP;
                return;
            case 1:
                dirOffset = AI_DIROffset.DOWN;
                return;
            case 2:
                dirOffset = AI_DIROffset.LEFT;
                return;
            case 3:
                dirOffset = AI_DIROffset.RIGHT;
                return;
            case 4:
                dirOffset = AI_DIROffset.UPLEFT;
                return;
            case 5:
                dirOffset = AI_DIROffset.DOWNLEFT;
                return;
            case 6:
                dirOffset = AI_DIROffset.UPRIGHT;
                return;
            case 7:
                dirOffset = AI_DIROffset.DOWNRIGHT;
                return;
            default:
                dirOffset = AI_DIROffset.NAN;
                return;
        }
    }

    protected Vector3 GetDirectionOffset(AI_DIROffset dir, Vector3 currentDir)
    {
        currentDir.Normalize();
        int temp = Random.Range(0, 8);
        Vector3 lr = Vector3.Cross(currentDir, Vector3.up).normalized;      //
        Vector3 ud = Vector3.Cross(lr, currentDir).normalized;

        switch (dir)
        {
            case AI_DIROffset.UP:
                return ud;
            case AI_DIROffset.DOWN:
                return -ud;
            case AI_DIROffset.LEFT:
                return lr;
            case AI_DIROffset.RIGHT:
                return -lr;
            case AI_DIROffset.UPLEFT:
                return (lr + ud).normalized;
            case AI_DIROffset.UPRIGHT:
                return (lr - ud).normalized;
            case AI_DIROffset.DOWNLEFT:
                return (-lr + ud).normalized;
            case AI_DIROffset.DOWNRIGHT:
                return (-lr - ud).normalized;
            default:
                return Vector3.zero;
        }
    }

    protected void CheckIfEngineEffect()
    {
        if(control.ifEngineEffect&&
            Vector3.Distance(transform.position, gameManager.MainPlayer.transform.position) > 100.0f)
        {
            control.ifEngineEffect = false;
            return;
        }
        if (!control.ifEngineEffect &&
            Vector3.Distance(transform.position, gameManager.MainPlayer.transform.position) <= 100.0f)
        {
            control.ifEngineEffect = true;
        }
    }

    
}
