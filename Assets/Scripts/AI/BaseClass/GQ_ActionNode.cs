﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate GQ_RunStatus GQ_Action();

public class GQ_ActionNode : GQ_BTNodeBase
{
    public GQ_Action action = null;


    public GQ_ActionNode() : base()
    {
        name += "ActionNode";
    }

    public GQ_ActionNode(string name, int order = 0) : base(name, order)
    {

    }

    public GQ_ActionNode(string name, GQ_Action action, int order = 0) : base(name, order)
    {
        this.action = action;
    }

    public override GQ_RunStatus Tick()
    {
        if (!Precondition())
            return GQ_RunStatus.Fail;
        status = action();
        return status;
    }

    public override GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick)
    {
        GQ_RunStatus temp = action();
        if (temp == GQ_RunStatus.Running)
            bt_cusTick.ActivedAction.Add(action);
        return temp;
    }
}
