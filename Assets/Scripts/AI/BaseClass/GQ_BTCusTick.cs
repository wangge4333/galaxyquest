﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//for users don't want tree ticks every frame
public class GQ_BTCusTick : MonoBehaviour
{
    public GQ_BTNodeBase entry = null;
    protected bool isRunning = false;

    [SerializeField]
    protected float tickTime = 0.5f;
    private float gq_Timer = 0;
    protected bool tickNow = false;
    protected bool forcedTick = false;

    protected List<GQ_Action> activedAction = new List<GQ_Action>();
    public List<GQ_Action> ActivedAction { get => activedAction; }

    protected void Awake()
    {
        Init();
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {

    }

    // Update is called once per frame
    protected virtual void Update()
    {
        CheckTime();
        RegularTick();
        ExtraUpdate();
    }

    protected virtual void Init()
    {

    }

    protected virtual void OnStartRunning()
    {

    }

    protected virtual void OnEndRunning()
    {

    }

    protected virtual void ExtraUpdate()
    {

    }

    protected virtual void RegularTick()
    {
        if (entry == null || !isRunning)
            return;
        if(forcedTick)
        {
            Tick();
            forcedTick = false;
        }
        if (tickNow)
        {
            Tick();
            tickNow = false;
        }
        else
        {
            ExcuteActions();
        }
    }

    public void ForcedTick()
    {
        forcedTick = true;
        tickNow = true;
    }

    protected void Tick()
    {
        gq_Timer = 0;
        activedAction.Clear();
        
        entry.TickCusTime(this);
    }

    protected void CheckTime()
    {
        if (!isRunning)
            return;
        gq_Timer += Time.deltaTime;
        if (gq_Timer >= tickTime)
        {
            tickNow = true;
        }
    }

    protected void ExcuteActions()
    {
        foreach(GQ_Action act in activedAction)
        {
            if (act() != GQ_RunStatus.Running)
            {
                ForcedTick();
            }
        }
    }

    public void SetRunningStatus(bool status)
    {
        isRunning = status;
        if (status)
        {
            OnStartRunning();
            gq_Timer = tickTime;
        } 
        if (!status)
            OnEndRunning();
    }
}
