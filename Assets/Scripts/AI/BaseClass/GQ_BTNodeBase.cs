﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GQ_RunStatus
{
    Success,
    Fail,
    Running
}

public class GQ_BT
{
    //public GQ_BTNodeBase entry = null;
    //public void ForcedTick();
    public float tickTime = 1.0f;

    public GQ_BT()
    {

    }
}

public abstract class GQ_BTPrecondition
{
    abstract public bool Precondition();
}

public abstract class GQ_BTNodeBase
{
    protected string name = "";
    public string description = "";

    //smaller order means higher priority
    public int order = 0;

    public string Name { get => name; }

    protected GQ_RunStatus status = GQ_RunStatus.Success;
    public GQ_RunStatus Status { get => status; }

    protected GQ_BTNodeBase parent = null;
    public GQ_BTNodeBase Parent { get => parent; }

    public GQ_BTPrecondition precondition = null;

    public GQ_BTNodeBase()
    {
        name = "NoName";
        order = 0;
    }

    public GQ_BTNodeBase(string name, int order = 0)
    {
        this.name = name;
        this.order = order;
    }

    public virtual bool Precondition()
    {
        if (precondition == null)
            return true;
        return precondition.Precondition();
    }

    public virtual GQ_RunStatus Tick()
    {
        return SetAndReturn(GQ_RunStatus.Success);
    }

    public virtual GQ_RunStatus TickCusTime(GQ_BTCusTick bt_cusTick)
    {
        return SetAndReturn(GQ_RunStatus.Success);
    }

    protected virtual GQ_RunStatus SetAndReturn(GQ_RunStatus status)
    {
        this.status = status;
        return status;
    }
}

public abstract class GQ_NONActionNode : GQ_BTNodeBase
{
    public GQ_NONActionNode() : base()
    {
        name += "NonActionNode";
    }

    public GQ_NONActionNode(string name, int order = 0) : base(name, order)
    {

    }
}


