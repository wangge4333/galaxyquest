﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardCamera : MonoBehaviour
{
    protected GameManager gameManager = null;
    protected Camera _camera = null;
    protected InputManager input = null;
    protected UIManager uiManager = null;

    [SerializeField]
    protected SpaceshipControl target = null;
    protected FireControl targetFC = null;
    protected Transform bridgePos = null;
    protected bool isBridgeView = false;

    protected float distance = 0.1f;

    protected float yaw = -90.0f;
    protected float pitch = 0.0f;

    [SerializeField,Range(30.0f,85.0f)]
    protected float maxPitch = 75.0f;
    [SerializeField, Range(-85.0f, 20.0f)]
    protected float minPitch = -75.0f;

    [SerializeField]
    protected float minDistance = 0.08f;
    [SerializeField]
    protected float maxDistance = 0.5f;

    [Range(0.1f, 50.0f)]
    public float horizontalSpeed = 5.0f;
    [Range(0.1f, 50.0f)]
    public float verticalSpeed = 5.0f;

    [Range(0.01f, 1.0f)]
    public float scopeOnSpeed = 0.5f;
    protected bool scopeOn = false;
    [SerializeField, Range(45.0f, 120.0f)]
    protected float originalFOV = 75.0f;
    [SerializeField, Range(1.0f, 16.0f)]
    protected float magnification = 8.0f;

    [Range(0.01f, 1.0f)]
    public float LerpFactor = 0.3f;

    [SerializeField]
    protected LayerMask desIgnoreLayer = new LayerMask();

    //public float MouseDesDis = 50.0f;



    // Start is called before the first frame update
    void Start()
    {
        Initialization();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        ShowCursor();
    }

    protected void LateUpdate()
    {
        Standard3rdCamera();
        //ChangeFOVBySpeed();
    }

    protected void Initialization()
    {
        _camera = GetComponent<Camera>();
        _camera.fieldOfView = originalFOV;

        gameManager = FindObjectOfType<GameManager>();
        input = FindObjectOfType<InputManager>();
        uiManager = FindObjectOfType<UIManager>();

        target = gameManager.MainPlayer.GetComponent<SpaceshipControl>();
        targetFC = target.GetComponent<FireControl>();
        bridgePos = target.GetComponent<SpaceshipControl>().bridgeView;
        distance = minDistance + (maxDistance - minDistance) / 3.0f;

        desIgnoreLayer = ~desIgnoreLayer;

        uiManager.HideScope();
        uiManager.ShowAimCursor();
    }

    protected void Standard3rdCamera()
    {
        if (target == null)
            return;
        SwitchScope();
        if(!scopeOn)
            SwitchBridgeCamera();

        if (Cursor.lockState != CursorLockMode.None)
        {
            yaw -= input.CHorizontal * horizontalSpeed * (scopeOn ? (scopeOnSpeed / magnification) : 1.0f);
            pitch -= input.CVertical * verticalSpeed * (scopeOn ? (scopeOnSpeed / magnification)  : 1.0f);
            distance -= input.CDistance * 0.1f;
        }

        if (yaw > 360.0f)
            yaw -= 360.0f;
        if (yaw < 0.0f)
            yaw += 360.0f;

        pitch = Mathf.Clamp(pitch, minPitch, maxPitch);
        distance = Mathf.Clamp(distance, minDistance, maxDistance);

        Vector3 temp = new Vector3(0, 0, 0);

        //calculate XZ
        if (yaw == 90.0f)
            temp.z = 1;
        else if (yaw == 270.0f)
            temp.z = -1;
        else if (yaw < 90.0f || yaw > 270.0f)
        {
            temp.x = 1;
            temp.z = Mathf.Tan(yaw * Mathf.Deg2Rad);
        }
        else
        {
            temp.x = -1;
            temp.z = -Mathf.Tan(yaw * Mathf.Deg2Rad);
        }

        //calculate Y
        temp.y = Mathf.Tan(pitch * Mathf.Deg2Rad) * temp.magnitude;
        temp.Normalize();
        temp = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * temp;

        //if (!isBridgeView)
        //{
            //temp = target.transform.position + distance * temp.normalized;
            transform.position = target.transform.position + distance * temp;
            transform.LookAt(target.transform.position);
            //transform.Rotate(transform.forward, target.transform.rotation.eulerAngles.z - transform.rotation.eulerAngles.z, Space.World);
            transform.Rotate(new Vector3(DefaultPara.LookUP, 0, 0), Space.Self);
            _camera.fieldOfView = originalFOV;

        //}
        if (isBridgeView) 
        {
            transform.position = bridgePos.position;
            //transform.LookAt(target.MouseDes);
            _camera.fieldOfView = originalFOV / (scopeOn ? magnification : 1.0f);
            
        }

        CalDesitination();
    }

    protected void SwitchBridgeCamera()
    {
        if (input.SwitchViewPos == ButtonState.DOWN)
            isBridgeView = !isBridgeView;
    }

    protected void SwitchScope()
    {
        if (input.SwitchScope == ButtonState.DOWN)
        {
            scopeOn = !scopeOn;
            if (scopeOn == false) 
            {
                isBridgeView = false;
                uiManager.HideScope();
                uiManager.ShowAimCursor();
            }
            else
            {
                isBridgeView = true;
                uiManager.ShowScope();
                uiManager.HideAimCursor();
            }
                
        }
    }


    protected void ChangeFOVBySpeed()
    {
        _camera.fieldOfView = Mathf.Clamp(DefaultPara.FarAIM + target.GetComponent<Rigidbody>().velocity.magnitude / 0.5f * 30.0f, 60.0f, 90.0f);
    }

    protected void ShowCursor()
    {
        if (input.ShowCursor == ButtonState.HOLD)
        {
            Cursor.lockState = CursorLockMode.None;
            input.FreezeShipControl = true;
        }
        if(input.ShowCursor == ButtonState.UP)
        {
            Cursor.lockState = CursorLockMode.Locked;
            input.FreezeShipControl = false;
        }
            
    }

    protected void CalDesitination()
    {
        if (targetFC.LockedObject != null)
        {
            target.MouseDes = transform.position + transform.forward *
                Vector3.Distance(target.transform.position, targetFC.PredictedPos);
            return;
        }

        Ray _ray = new Ray(transform.position, transform.forward);
        RaycastHit _hit;

        bool isHit = Physics.Raycast(_ray, out _hit, DefaultPara.FarAIM, desIgnoreLayer);

        //Debug.DrawRay(_ray.origin, _ray.direction, Color.red);
        if (!isHit)
            target.MouseDes = transform.position + transform.forward * DefaultPara.FarAIM;
        else
        {
            target.MouseDes = _hit.point + 0.01f * (target.transform.position - _hit.point);
        }

    }

}
