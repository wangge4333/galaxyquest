﻿Shader "Custom/AtmoRing"
{
    Properties{
        _Color("Color", Color) = (1,1,1,1)
        _AlphaPower("Alpha Power",Range(1.0,20.0)) = 10.0
        _MaxAlpha("Max Alpha",Range(0.1,1.0)) = 1.0
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _BumpMap("Bump map", 2D) = "bump"{}
        _RingColor("Ring Color", Color) = (1,1,1,1)
        _RingPower("Ring Strength", Range(1.0, 500.0)) = 10.0
        _DisFactor("Distance Factor",Range(0.1,10.0)) = 0.5
    }

    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        //Blend SrcAlpha OneMinusSrcAlpha
        //ZWrite Off
        LOD 200
        Cull Off

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf AtmosphereRing alpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        #include "UnityPBSLighting.cginc"

        sampler2D _MainTex;

        struct Input {
            float4 color : Color;
            float2 uv_MainTex;
            float2 uv_BumpMap;
            float3 viewDir;
            float3 worldPos;
        };

        struct SurfaceOutputCustom
        {
            fixed3 Albedo;      // base (diffuse or specular) color
            fixed3 Normal;      // tangent space normal, if written
            half3 Emission;
            fixed Alpha;        // alpha for transparencies
            //fixed3 viewDir;
        };

        float4 _ColorTint;
        sampler2D _BumpMap;
        float4 _RingColor;
        float _RingPower;
        float _AlphaPower;
        float _MaxAlpha;
        float _DisFactor;

        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

            void surf(Input IN, inout SurfaceOutputCustom o) {

            //o.Albedo = tex2D(_MainText, IN.uv_MainTex).rgb * IN.color;
            //o.Normal =
            float3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
            //normal.z = normal.z / _BumpScale;

            o.Normal = normalize(normal);
            half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
            o.Emission = _RingColor.rgb * pow(rim, _RingPower);

            //// Albedo comes from a texture tinted by color
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            //// Metallic and smoothness come from slider variables
            //o.Metallic = _Metallic;
            //o.Smoothness = _Glossiness;
            if (rim >= 0.9)
                rim = -9 * rim + 9;

            o.Alpha = min(_MaxAlpha,
                pow(rim, _AlphaPower) / max(length(_WorldSpaceCameraPos - IN.worldPos) / _DisFactor, 1));
        }

        inline float4 LightingAtmosphereRing(SurfaceOutputCustom s, fixed3 lightDir, fixed atten) {
            float4 col;
            col.rgb = s.Albedo;

            float itp = dot(normalize(lightDir), s.Normal) / 2 + 0.5;
            itp = smoothstep(0.3, 0.6, itp);

            col.a = s.Alpha * (1 - itp);
            return col;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
