﻿Shader "Custom/Spaceship"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _BumpMap("Bump map", 2D) = "bump"{}
        _Glossiness("Smoothness", Range(0,1)) = 0.5
        _Metallic("Metallic", Range(0,1)) = 0.0
        _MinLighten("Min Lighten",Range(0,0.5)) = 0.15
        _VerticalLight("Min Vertical Lighten",Range(0,0.5)) = 0.1
        _Occlusion("Occlusion", 2D) = "white"{}
        _OcclusionFactor("Occlusion Strength",Range(0,1)) = 1
        _Emission("Emission",2D) = "black"{}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf LightenSpaceship fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        #include "UnityPBSLighting.cginc"

        sampler2D _MainTex;
        sampler2D _BumpMap;
        sampler2D _Occlusion;
        sampler2D _Emission;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpMap;
            float3 viewDir;
        };

        half _Glossiness;
        half _Metallic;
        half _MinLighten;
        half _VerticalLight;
        half _OcclusionFactor;

        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
            o.Albedo = c.rgb;/** step(0.2, abs(dot(normalize(IN.viewDir), o.Normal)))*/
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
            o.Occlusion = tex2D(_Occlusion, IN.uv_MainTex).r/* * 0.5 + 0.5*/;
            o.Emission = tex2D(_Emission, IN.uv_MainTex);
        }

        inline float4 LightingLightenSpaceship(SurfaceOutputStandard s, fixed3 lightDir, fixed atten) {
            float difLight = max(_MinLighten, min(1, dot(s.Normal, lightDir) + _VerticalLight)) 
                * (1 - s.Smoothness);
            float4 col;
            col.rgb = s.Albedo * _LightColor0.rgb * (difLight * atten) 
                * (1 - (1 - s.Occlusion) * _OcclusionFactor);
            col.a = s.Alpha;
            return col;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
