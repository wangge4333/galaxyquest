﻿Shader "Custom/WeaponPosHint"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _Emission("Emission",Color) = (0,0,0,0)

    }
        SubShader
        {
            Tags { "RenderType" = "Opaque"}
            LOD 200
            Cull Off

            CGPROGRAM
            // Physically based Standard lighting model, and enable shadows on all light types
            #pragma surface surf PosHint fullforwardshadows

            // Use shader model 3.0 target, to get nicer looking lighting
            #pragma target 3.0
            #include "UnityPBSLighting.cginc"

            sampler2D _MainTex;
            float4 _Color;
            float4 _Emission;

            struct Input
            {
                float2 uv_MainTex;
            };

            struct SurfaceOutputHint
            {
                fixed3 Albedo;
                fixed3 Normal;
                half3 Emission;
                fixed Alpha;        // alpha for transparencies
            };

            // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
            // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
            // #pragma instancing_options assumeuniformscaling
            UNITY_INSTANCING_BUFFER_START(Props)
                // put more per-instance properties here
            UNITY_INSTANCING_BUFFER_END(Props)

            void surf(Input IN, inout SurfaceOutputHint o)
            {

                float4 c = _Color.rgba;

                o.Albedo = c.rgb;
                o.Alpha = c.a;
                o.Emission = _Emission;
            }

            inline float4 LightingPosHint(SurfaceOutputHint s, fixed3 lightDir, fixed atten) {
                float4 col;
                col.rgb = s.Albedo;
                col.a = s.Alpha;
                return col;
            }

            ENDCG
        }
            FallBack "Diffuse"
}
