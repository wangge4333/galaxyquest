﻿Shader "Custom/WireFrame"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _BumpMap("Bump map", 2D) = "bump"{}
        _Threshold("Threshold",Range(0.001,1.0)) = 0.001
        _CameraDir("Camera Direction",Vector) = (0,1,0,0)
        _Emission("Emission",Range(0.0,1.0)) = 0.0
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 200
        Cull Off

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf DrawLine alpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
        #include "UnityPBSLighting.cginc"

        sampler2D _MainTex;
        sampler2D _BumpMap;
        float4 _Color;
        float4 _CameraDir;

        float _Emission;
        float _Threshold;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpMap;
            float3 worldNormal;
        };

        struct SurfaceOutputLine
        {
            fixed3 Albedo;      // base (diffuse or specular) color
            fixed3 Normal;      // tangent space normal, if written
            half3 Emission;
            fixed Alpha;        // alpha for transparencies
        };

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputLine o)
        {
            // Albedo comes from a texture tinted by color
            float3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
            o.Normal = normalize(normal);

            float4 c = _Color.rgba;
            //float3 wNormal = normalize(UnityObjectToWorldDir(o.Normal));
            float3 wNormal = mul(o.Normal, (float3x3)unity_WorldToObject);

            float temp = dot(normalize(_CameraDir.xyz), wNormal);
            c.a = step(-_Threshold, temp) * step(temp, _Threshold);

            o.Albedo = c.rgb;
            o.Alpha = c.a;
            o.Emission = _Emission;
        }

        inline float4 LightingDrawLine(SurfaceOutputLine s, fixed3 lightDir, fixed atten) {
            float4 col;
            col.rgb = s.Albedo;
            col.a = s.Alpha;
            return col;
        }

        ENDCG
    }
    FallBack "Diffuse"
}
