﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    EcoItem,
    Consumption,
    Weapon
}

//needed? or not? it will make the system more complex
public enum EcoItemType
{
    Food,
    Metal,
    Wood,
    Energy,
    IndusProduct
}

public class Item
{
    protected int itemID = 0;
    protected ItemType itemType = ItemType.EcoItem;

    protected string itemName = "";
    protected string fileName = "";                     //common name of this item, could be the file name of icon, or even mode
    protected string description_0 = "";
    protected string description_1 = "";

    protected int basicValue = 0;
    protected float weight = 0;                         //means tons per unit
    protected float volume = 0;                          //means cubic meter per unit

    public int ItemID { get => itemID; }
    public ItemType Type { get => itemType; }
    public string ItemName { get => itemName; }
    public string FileName { get => fileName; }
    public string Description { get => description_0; }
    public string SubDescription { get => description_1; }
    public int BasicValue { get => basicValue; }
    public float Weight { get => weight; }
    public float Volume { get => volume; }

    public Item(int _itemID, string _itemName = "", ItemType _type = ItemType.EcoItem,
        int _basicValue = 0, float _weight = 0, float _volume = 1,
        string _fileName = "NULL", string _des_0 = "", string _subDes = "")
    {
        itemID = _itemID;
        itemName = _itemName;
        itemType = _type;
        basicValue = _basicValue;
        weight = _weight;
        volume = _volume;

        fileName = _fileName;
        description_0 = _des_0;
        description_1 = _subDes;
    }

    public int GetFinalPrice(float areaFactor)
    {
        if (itemType == ItemType.EcoItem)
            return Mathf.FloorToInt(areaFactor * basicValue);
        else
            return basicValue;
    }

    public int GetFinalPrice(PlanetMarket market)
    {
        if (itemType == ItemType.EcoItem)
            return Mathf.FloorToInt(market.GetPriceFactor(ItemID) * basicValue);
        else
            return basicValue;
    }

}

public class EcoItem : Item
{


    public EcoItem(int _itemID, string _itemName = "", ItemType _type = ItemType.EcoItem,
        int _basicValue = 0, float _weight = 0, float _volume = 1,
        string _fileName = "NULL", string _des_0 = "", string _subDes = "")
        : base(_itemID, _itemName, _type, _basicValue, _weight, _volume, _fileName, _des_0, _subDes)
    {
        
    }
}

public class WeaponItem : Item
{
    //fileName both used as iconName and prefab's name

    protected WeaponType weaponType = WeaponType.BigCalibre;

    public WeaponType WeaponType { get => weaponType; }

    public WeaponItem(int _itemID, int _weaponType, string _itemName = "", ItemType _type = ItemType.Weapon,
        int _basicValue = 0, float _weight = 0, float _volume = 1,
        string _fileName = "NULL", string _des_0 = "", string _subDes = "")
        : base(_itemID, _itemName, _type, _basicValue, _weight, _volume, _fileName, _des_0, _subDes)
    {
        weaponType = (WeaponType)_weaponType;
    }
}
