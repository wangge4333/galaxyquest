﻿using ExcelDataReader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

public class ItemList : MonoBehaviour
{
    [SerializeField]
    protected static string fileName = "ItemList";

    protected static Dictionary<int, EcoItem> ecoItemList = new Dictionary<int, EcoItem>();
    protected static Dictionary<int, WeaponItem> weaponList = new Dictionary<int, WeaponItem>();

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        LoadListFromFile();
    }

    protected void LoadListFromFile()
    {
        FileStream fs = File.Open(Application.dataPath + "/Resources/Item/" + fileName + ".xlsx",
            FileMode.Open, FileAccess.Read, FileShare.Read);
        IExcelDataReader reader;
        reader = ExcelReaderFactory.CreateOpenXmlReader(fs);

        DataSet result = reader.AsDataSet();
        reader.Close();

        //Debug.Log(result.Tables[0].Rows[1][9]);

        for(int i = 1; i < result.Tables[0].Rows.Count; ++i)
        {
            switch (Convert.ToInt32(result.Tables[0].Rows[i][1]))
            {
                case 1:
                    {
                        ecoItemList.Add(Convert.ToInt32(result.Tables[0].Rows[i][0]),
                            new EcoItem(Convert.ToInt32(result.Tables[0].Rows[i][0]),
                            result.Tables[0].Rows[i][2].ToString(),
                            ItemType.EcoItem,
                            Convert.ToInt32(result.Tables[0].Rows[i][6]),
                            Convert.ToSingle(result.Tables[0].Rows[i][7]),
                            Convert.ToSingle(result.Tables[0].Rows[i][8]),
                            result.Tables[0].Rows[i][3].ToString(),
                            result.Tables[0].Rows[i][4].ToString(),
                            result.Tables[0].Rows[i][5].ToString()));
                        break;
                    }
                    
                case 2:
                    {
                        weaponList.Add(Convert.ToInt32(result.Tables[0].Rows[i][0]),
                            new WeaponItem(Convert.ToInt32(result.Tables[0].Rows[i][0]),
                            Convert.ToInt32(result.Tables[0].Rows[i][9]),
                            result.Tables[0].Rows[i][2].ToString(),
                            ItemType.Weapon,
                            Convert.ToInt32(result.Tables[0].Rows[i][6]),
                            Convert.ToSingle(result.Tables[0].Rows[i][7]),
                            Convert.ToSingle(result.Tables[0].Rows[i][8]),
                            result.Tables[0].Rows[i][3].ToString(),
                            result.Tables[0].Rows[i][4].ToString(),
                            result.Tables[0].Rows[i][5].ToString()));
                        break;
                    }
                default:
                    break;
            }
        }
    }



    public static Item GetItem(int itemID)
    {
        int flag = itemID / 1000;
        if (flag == 1 && ecoItemList.ContainsKey(itemID)) 
        {
            return ecoItemList[itemID];
        }
        if (flag == 2 && weaponList.ContainsKey(itemID)) 
        {
            return weaponList[itemID];
        }
        return null;
    }

    public static GameObject GetWeaponPrefab(int itemID)
    {
        if (itemID < 2001)
            return null;
        string path = GQ_Path.WeaponPrefabs;
        WeaponItem temp = weaponList[itemID];
        switch (temp.WeaponType)
        {
            case WeaponType.BigCalibre:
                path += "BigCalibre/";
                break;
            case WeaponType.BMCalibre:
                path += "BMCalibre/";
                break;
            case WeaponType.MediumCalibre:
                path += "MediumCalibre/";
                break;
            case WeaponType.SmallCalibre:
                path += "SmallCalibre/";
                break;
            case WeaponType.Flak:
                path += "Flak/";
                break;
            case WeaponType.Missile:
                path += "Rounds/";
                break;
            case WeaponType.VLS:
                path += "VLS/";
                break;
            default:
                break;
        }

        GameObject result = (GameObject)Resources.Load(path + temp.FileName);
        if (result == null)
            return null;
        result.GetComponent<WeaponBase>().weaponID = itemID;
        return result;
    }
}
