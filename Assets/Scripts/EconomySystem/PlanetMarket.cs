﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;

public class PlanetMarket : MonoBehaviour
{

    protected PlanetManager marketManager = null;

    protected float updateTime = 0;
    public float UpdateTime { get => updateTime; }

    protected int marketID = 10003;
    public int MarketID { get => marketID; }

    //about ecoItem
    [Tooltip("According to the itemlist.xlsx to edit this." +
        "First int is itemID, second means damand amount.(per time unit)")]
    protected Dictionary<int, int> demandList = new Dictionary<int, int>();

    [Tooltip("According to the itemlist.xlsx to edit this." +
        "First int is itemID, second means output amount.(per time unit)")]
    protected Dictionary<int, int> outputList = new Dictionary<int, int>();

    [SerializeField, Tooltip("Every timeUnit(sec), demand and output will update for once.")]
    protected float timeUnit = 900.0f;
    protected float lastUpdate = 0.0f;

    protected Dictionary<int, int> storageList = new Dictionary<int, int>();
    public Dictionary<int,int> StorageList { get => storageList; }

    protected Dictionary<int, float> priceFactors = new Dictionary<int, float>();

    [SerializeField]
    protected int updatePrice = 30;
    //ecoItem over


    //weapon Item
    [Tooltip("According to the itemlist.xlsx to edit this.")]
    protected RatePairs<int> weaponApRate = new RatePairs<int>();
    //total amount of weapons in this market
    [SerializeField]
    protected int weaponAmount = 5;

    [SerializeField]
    protected List<int> weaponList = new List<int>();
    public List<int> WeaponList { get => weaponList; }
    //weapon Item over


    protected Dictionary<int, ShipCargo> tradeTargets = new Dictionary<int, ShipCargo>();


    protected void Awake()
    {
        marketID = GetComponent<PlanetAttribute>().PlanetID;
    }

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        MarketRefresh();
    }

    protected void Initialization()
    {
        marketManager = GetComponentInParent<PlanetManager>();
        
        UpdatePriceFactors();
        RefreshWeapons();
    }

    public ShipCargo GetTradeTarget(int _id)
    {
        if (!tradeTargets.ContainsKey(_id))
            return null;
        return tradeTargets[_id];
    }

    protected void MarketRefresh()
    {
        if ((Time.time - lastUpdate < timeUnit))
            return;
        lastUpdate = Time.time;
        List<int> tempList = new List<int>();

        foreach(KeyValuePair<int,int> t in storageList)
        {
            tempList.Add(t.Key);
        }

        for(int i = 0; i < tempList.Count; ++i)
        {
            int output = 0, demand = 0;
            if (outputList.ContainsKey(tempList[i]))
                output = outputList[tempList[i]];
            if (demandList.ContainsKey(tempList[i]))
                demand = demandList[tempList[i]];
            storageList[tempList[i]] += Mathf.RoundToInt(
                Algo.StandardNormalRandom(1.0f, DefaultPara.OutputSigma) * output -
                demand);
            UpdatePriceFactor(tempList[i]);
        }

        RefreshWeapons();

    }

    protected void RefreshWeapons()
    {
        //upate weapons
        weaponList.Clear();
        for (int i = 0; i < weaponAmount; ++i)
            RandomlyProduceAWeapon();
    }


    protected void RandomlyProduceAWeapon()
    {
        weaponList.Add(weaponApRate.GetRandomResult());
    }

    protected void UpdateMarket()
    {
        updateTime = Time.time;
    }

    protected void UpdatePriceFactors()
    {
        foreach (KeyValuePair<int, int> t in storageList)
        {
            //if (priceFactors.ContainsKey(t.Key))
            //    priceFactors[t.Key] = CalPriceFactor(storageList[t.Key]);
            //else
            //    priceFactors.Add(t.Key, CalPriceFactor(storageList[t.Key]));
            UpdatePriceFactor(t.Key);
        }
        updateTime = Time.time;
    }

    protected void UpdatePriceFactor(int itemID)
    {
        if (!storageList.ContainsKey(itemID))
            storageList.Add(itemID, 0);

        float tempFactor = CalPriceFactor(storageList[itemID]);
        if (priceFactors.ContainsKey(itemID))
            priceFactors[itemID] = tempFactor;
        else
            priceFactors.Add(itemID, tempFactor);
        updateTime = Time.time;

        //tell the marketManager
        marketManager.UpdatePriceFactor(marketID, itemID, tempFactor);

    }

    protected float CalPriceFactor(int storage)
    {
        if (storage >= updatePrice)
        {
            int temp = (storage - 1) / updatePrice;
            return Mathf.Max(DefaultPara.LowestPriceFactor,
                (DefaultPara.LowestPriceFactor - 1) / 9.0f * (float)temp +
                (10.0f - DefaultPara.LowestPriceFactor) / 9.0f);
        }
        else
        {
            int temp = (-storage - 1) / updatePrice + 2;
            return Mathf.Pow(DefaultPara.PriceUpFactor, temp);
        }
    }

    public void SetState(int itemID, int demand, int output, int storage)
    {
        demandList.Add(itemID, demand);
        outputList.Add(itemID, output);
        storageList.Add(itemID, storage);
        priceFactors.Add(itemID, 1);
    }

    public void SetState(int weaponID, float rate)
    {
        weaponApRate.AddPair(weaponID, rate);
    }

    //means this market can sell to player
    public int GetSellableAmount(int itemID)
    {
        int flag = itemID / 1000;
        switch (flag)
        {
            case 1:
                {
                    if (!storageList.ContainsKey(itemID) || storageList[itemID] <= 0)
                        return 0;
                    else
                        return (storageList[itemID] % updatePrice) == 0 ?
                            updatePrice : (storageList[itemID] % updatePrice);
                }
            case 2:
                {
                    if (weaponList.Contains(itemID))
                        return 1;
                    else
                        return 0;
                }
            default:
                return 0;
        }

        

    }

    //means this market can buy from player
    public int GetBuyableAmount(int itemID)
    {
        int flag = itemID / 1000;
        switch (flag)
        {
            case 1:
                {
                    if (storageList[itemID] <= 0)
                    {
                        return ((-storageList[itemID] % updatePrice) == 0) ?
                             updatePrice : (-storageList[itemID] % updatePrice);
                    }
                    else
                    {
                        return (storageList[itemID] % updatePrice == 0) ?
                             updatePrice : (updatePrice - storageList[itemID] % updatePrice);
                    }
                }
            case 2:
                return 1;
            default:
                return 0;
        }

        
    }

    /// <summary>
    /// means this market buy anything from player or NPC
    /// </summary>
    /// <param name="itemID"></param>
    /// <param name="amount"></param>
    /// <param name="_id">id of spaceObject(means a NPC or a player)</param>
    /// <returns></returns>
    public bool Buy(int itemID, int amount,int _id)
    {
        if (amount > GetBuyableAmount(itemID) || !tradeTargets.ContainsKey(_id))
            return false;
        int flag = itemID / 1000;
        switch (flag)
        {
            case 1:
                {
                    if (storageList.ContainsKey(itemID))
                    {
                        storageList[itemID] += amount;
                    }
                    else
                    {
                        storageList.Add(itemID, amount);
                    }
                    tradeTargets[_id].Sell(itemID, amount, this);
                    UpdatePriceFactor(itemID);
                    break;
                }
            case 2:
                {
                    weaponList.Add(itemID);
                    tradeTargets[_id].Sell(itemID, 1, this);
                    UpdatePriceFactors();
                    break;
                }
            default:
                return false;
        }

       
        return true;
    }

    /// <summary>
    /// means this market sell anything to player or NPC
    /// </summary>
    /// <param name="itemID"></param>
    /// <param name="amount"></param>
    /// <param name="_id">id of spaceObject(means a NPC or a player)</param>
    /// <returns></returns>
    public bool Sell(int itemID, int amount,int _id)
    {
        if (amount > GetSellableAmount(itemID)|| !tradeTargets.ContainsKey(_id))
            return false;
        int flag = itemID / 1000;
        switch (flag)
        {
            case 1:
                {
                    storageList[itemID] -= amount;
                    tradeTargets[_id].Buy(itemID, amount, this);
                    UpdatePriceFactor(itemID);
                    break;
                }
            case 2:
                {
                    weaponList.RemoveAt(weaponList.IndexOf(itemID));
                    tradeTargets[_id].Buy(itemID, 1, this);
                    UpdatePriceFactors();
                    break;
                }
            default:
                return false;
        }
        return true;
    }

    public float GetPriceFactor(int itemID)
    {
        if (priceFactors.ContainsKey(itemID))
            return priceFactors[itemID];
        else
        {
            storageList.Add(itemID, 0);
            priceFactors.Add(itemID, CalPriceFactor(0));
            return priceFactors[itemID];
        }
    }

    public int GetPrice(int _itemID)
    {
        return ItemList.GetItem(_itemID).GetFinalPrice(this);
    }

    public int GetStorage(int itemID)
    {
        return storageList[itemID];
    }

    public void AddTradeTarget(GameObject target)
    {
        if (tradeTargets.ContainsKey(target.GetComponent<SpaceObject>().ID))
            return;
        tradeTargets.Add(target.GetComponent<SpaceObject>().ID, target.GetComponent<ShipCargo>());
    }

    public void RemoveTradeTarget(GameObject target)
    {
        if (!tradeTargets.ContainsKey(target.GetComponent<SpaceObject>().ID))
            return;
        tradeTargets.Remove(target.GetComponent<SpaceObject>().ID);
    }
}
