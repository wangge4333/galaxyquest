﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCargo : MonoBehaviour
{
    [SerializeField]
    protected int money = 0;
    public int Money { get => money; }

    [SerializeField]
    protected float baseSpace = 64.0f;
    protected float fMaxSpace = 0.0f;
    protected float usedSpace = 0.0f;

    protected GQ_UpgradeTree upTree = null;

    protected float totalWeight = 0.0f;

    public float CargoWeight { get => totalWeight; }
    public float UsedSpace { get => usedSpace; }
    public float FinalMaxSpace { get => fMaxSpace; }
    public float FreeSpace { get => (fMaxSpace - usedSpace); }

    protected Dictionary<int, int> ecoItems;
    public Dictionary<int,int> EcoItems { get => ecoItems; }

    [SerializeField]
    protected List<int> weaponItems;
    public List<int> WeaponItems { get => weaponItems; }

    protected SpaceshipControl _control = null;

    protected PlanetMarket inteMarket = null;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        _control = GetComponent<SpaceshipControl>();

        ecoItems = new Dictionary<int, int>();
        weaponItems = new List<int>();
        if (tag == "Player")
            TestIni();

        upTree = GetComponent<GQ_UpgradeTree>();
    }

    public void TestIni()
    {
        Add(2002, 1);
        Add(2702, 1);
    }

    protected float GetFinalMaxSpace()
    {
        if (upTree == null)
            return baseSpace;
        return upTree.GetFinalHoldSpace(baseSpace);
    }

    public void UpdateSpace()
    {
        fMaxSpace = GetFinalMaxSpace();
    }

    //might be out of max space, return rest amount
    public int Add(int _itemID,int _amount)
    {
        int flag = _itemID / 1000;
        switch (flag)
        {
            case 1:
                {
                    EcoItem temp = (EcoItem)ItemList.GetItem(_itemID);
                    int canBeAdded = Mathf.FloorToInt(
                        (fMaxSpace - usedSpace) / temp.Volume);

                    if (ecoItems.ContainsKey(_itemID))
                    {
                        ecoItems[_itemID] += Mathf.Min(canBeAdded, _amount);
                    }
                    else
                    {
                        ecoItems.Add(_itemID, Mathf.Min(canBeAdded, _amount));
                    }

                    ChangeWeight(Mathf.Min(canBeAdded, _amount) * temp.Weight);
                    usedSpace += Mathf.Min(canBeAdded, _amount) * temp.Volume;

                    return Mathf.Max(0, _amount - canBeAdded);
                }
            case 2:
                {
                    WeaponItem temp = (WeaponItem)ItemList.GetItem(_itemID);
                    if ((usedSpace + temp.Volume) > fMaxSpace)
                        return 1;
                    weaponItems.Add(_itemID);

                    ChangeWeight(temp.Weight);
                    usedSpace += temp.Volume;

                    weaponItems.Sort();
                    return 0;
                }
            default:
                return 0;
        }
    }

    public bool Discard(int _itemID, int _amount)
    {
        int flag = _itemID / 1000;
        switch (flag)
        {
            case 1:
                {
                    EcoItem temp = (EcoItem)ItemList.GetItem(_itemID);
                    if (ecoItems.ContainsKey(_itemID))
                    {
                        if (ecoItems[_itemID] <= _amount)
                        {
                            ChangeWeight(-ecoItems[_itemID] * temp.Weight);
                            usedSpace -= ecoItems[_itemID] * temp.Volume;
                            ecoItems.Remove(_itemID);
                        }
                        else
                        {
                            ChangeWeight(-_amount * temp.Weight);
                            usedSpace -= _amount * temp.Volume;
                            ecoItems[_itemID] -= _amount;
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            case 2:
                {
                    if (!weaponItems.Contains(_itemID))
                        return false;
                    WeaponItem temp = (WeaponItem)ItemList.GetItem(_itemID);
                    weaponItems.RemoveAt(weaponItems.IndexOf(_itemID));
                    ChangeWeight(-temp.Weight);
                    usedSpace -= temp.Volume;
                    weaponItems.Sort();
                    return true;
                }
            default:
                return false;
        }
    }

    /// <summary>
    /// Sell to planet market
    /// </summary>
    /// <param name="_itemID"></param>
    /// <param name="_amount"></param>
    /// <param name="pm"></param>
    public bool Sell(int _itemID, int _amount, PlanetMarket pm)
    {
        if (!Discard(_itemID, _amount))
            return false;
        money += ItemList.GetItem(_itemID).GetFinalPrice(pm) * _amount;
        return true;
    }

    public int MaxAddableAmount(int itemID)
    {
        return Mathf.FloorToInt(FreeSpace / ItemList.GetItem(itemID).Volume);
    }

    public int MaxBuyableAmount(int itemID, PlanetMarket pm)
    {
        return Mathf.Min(MaxAddableAmount(itemID), Mathf.FloorToInt(money / pm.GetPrice(itemID)));
    }

    public bool CanAdd(int itemID)
    {
        return CanAdd(ItemList.GetItem(itemID).Volume);
    }

    public bool CanAdd(int itemID,int amount)
    {
        return CanAdd(ItemList.GetItem(itemID).Volume * amount);
    }

    public bool CanAdd(float neededSpace)
    {
        return (neededSpace > FreeSpace) ? false : true;
    }

    public bool CanBuy(int _itemID, int _amount, PlanetMarket pm)
    {
        if (ItemList.GetItem(_itemID).GetFinalPrice(pm) <= money)
            return true;
        return false;
    }

    /// <summary>
    /// Buy from planet market
    /// </summary>
    /// <param name="_itemID"></param>
    /// <param name="_amount"></param>
    /// <param name="pm"></param>
    /// <returns></returns>
    public bool Buy(int _itemID, int _amount, PlanetMarket pm)
    {
        if(!CanBuy(_itemID,_amount,pm))
            return false;
        Add(_itemID, _amount);
        money -= ItemList.GetItem(_itemID).GetFinalPrice(pm.GetPriceFactor(_itemID)) * _amount;

        return true;
    }

    

    protected void ChangeWeight(float weight)
    {
        totalWeight += weight;
        if (_control == null)
            return;
        _control.ResetMass();
    }

    protected void LostAllGoods()
    {     
        //instantiate goods in scene?
    }
}
