﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShipCategory
{
    Destroyer,                  //1
    LightCruiser,               //2
    HeavyCruiser,               //3
    BattleCruise,               //4
    BattleShip,                 //5
    Interceptor,                //6
    CargoShip,                  //7
    NaN
}

public class SpaceshipType
{
    protected int typeID = 0;
    protected string typeName = "";
    protected ShipCategory category = ShipCategory.NaN;

    public int TypeID { get => typeID; }
    public string TypeName { get => typeName; }
    public ShipCategory Category { get => category; }

    public SpaceshipType(int _typeID,string _typeName, ShipCategory _category)
    {
        typeID = _typeID;
        typeName = _typeName;
        category = _category;
    }

    static public SpaceshipType TestScharnhorst()
    {
        return new SpaceshipType(3001, "Scharnhorst", ShipCategory.HeavyCruiser);
    }

    static public SpaceshipType TestOther()
    {
        return new SpaceshipType(2001, "UnknownTest", ShipCategory.LightCruiser);
    }
}
