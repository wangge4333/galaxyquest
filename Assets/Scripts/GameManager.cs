﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //for star system
    [SerializeField,Range(0.01f,10.0f)]
    [Tooltip("the scale of time, 1 means 1 day per second(in real world)\n" +
        "Do not change this when game is running.")]

    //the scale of time, 1 means 1 day per second(in real world)
    protected float starMoveTimeScale = 1.0f;

    public float GetTimeScale() { return starMoveTimeScale; }
    public void SetTimeScale(float newScale) { starMoveTimeScale = newScale; }

    private bool isUndestroyed = false;

    private GameObject mainPlayer;

    public GameObject MainPlayer { get => mainPlayer; }

    private Camera mainCamera;
    public Camera MainCamera { get => mainCamera; }

    protected Dictionary<int, SpaceObject> spaceObjectList = new Dictionary<int, SpaceObject>();

    public Dictionary<int,SpaceObject> SpaceObjectList { get => spaceObjectList; }

    [SerializeField]
    protected List<GameObject> rootObjs = null;
    public Transform roundRoot = null;
    public Transform NPCRoot = null;

    protected void Awake()
    {
        if (!isUndestroyed)
        {
            DontDestroyOnLoad(this);
            isUndestroyed = true;
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    protected void Initialization()
    {
        InitRoots();

        //Find main player, this can be replaced later
        foreach (SpaceObject obj in FindObjectsOfType<SpaceObject>())
        {
            if(obj.tag=="Player")
            {
                spaceObjectList.Add(obj.ID, obj);
                mainPlayer = obj.gameObject;
                break;
            }
            if (obj.tag == "NPC"|| obj.tag == "Enemy")
            {
                spaceObjectList.Add(obj.ID, obj);
            }
        }

        foreach (Camera c in FindObjectsOfType<Camera>())
        {
            if (c.tag == "MainCamera")
            {
                mainCamera = c;
                break;
            }
        }
    }

    public void RegisterSpaceObject(int ID,SpaceObject obj)
    {
        if (obj.tag == "Player")
        {
            spaceObjectList.Add(obj.ID, obj);
            mainPlayer = obj.gameObject;
            return;
        }
        if (obj.tag == "NPC" || obj.tag == "Enemy")
        {
            spaceObjectList.Add(obj.ID, obj);
        }
    }

    protected void InitRoots()
    {
        SceneManager.GetActiveScene().GetRootGameObjects(rootObjs);
        foreach (GameObject obj in rootObjs)
        {
            if (obj.tag == "RoundRoot")
                roundRoot = obj.transform;
            if (obj.tag == "NPCRoot")
                NPCRoot = obj.transform;
        }
    }
}
