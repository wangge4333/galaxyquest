﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum ButtonState
{
    NA,
    DOWN,
    HOLD,
    UP
}

public class InputManager : MonoBehaviour
{
    public bool FreezeShipControl = false;
    public bool FreezeControl = false;

    public bool MoveDockCamera = false;

    //space ship manipulate
    //movement
    protected float throttle = 0.0f;
    protected float horizontal = 0.0f;
    protected float vertical = 0.0f;

    //rotation
    protected float pitchAxis = 0.0f;
    protected float yawAxis = 0.0f;
    protected float rollAxis = 0.0f;

    //buttons
    protected KeyCode kc_switchControlMode = KeyCode.Tab;
    protected ButtonState switchControlMode = ButtonState.NA;
    //fire
    protected KeyCode kc_mainFire = KeyCode.Mouse0;
    protected ButtonState mainFire = ButtonState.NA;
    protected KeyCode kc_lockOn = KeyCode.Mouse2;
    protected ButtonState lockOn = ButtonState.NA;
    protected KeyCode kc_missileFire = KeyCode.R;
    protected ButtonState missileFire = ButtonState.NA;

    public float Throttle { get => throttle; }
    public float Horizontal { get => horizontal; }
    public float Vertical { get => vertical; }
    public float PitchAxis { get => pitchAxis; }
    public float YawAxis { get => yawAxis; }
    public float RollAxis { get => rollAxis; }
    public KeyCode KC_SwitchControlMode { get => kc_switchControlMode; }
    public ButtonState SwitchControlMode { get => switchControlMode;}
    public KeyCode KC_MainFire { get => kc_mainFire; }
    public ButtonState MainFire { get => mainFire; }
    public KeyCode KC_LockOn { get => kc_lockOn; }
    public ButtonState LockOn { get => lockOn; }
    public KeyCode KC_missileFire { get => kc_missileFire; }
    public ButtonState MissileFire { get => missileFire; }

    //camera
    protected float cHorizontal = 0.0f;
    protected float cVertical = 0.0f;
    protected float cDistance = 0.0f;

    [SerializeField]
    protected float xAxisSensitivity = 0.5f;
    [SerializeField]
    protected float yAxisSensitivity = 0.5f;

    //for dock camera
    protected KeyCode kc_dock_rotate = KeyCode.Mouse0;
    protected ButtonState dock_rotate = ButtonState.NA;
    //over

    protected KeyCode kc_switchViewPos = KeyCode.Mouse1;
    protected ButtonState switchViewPos = ButtonState.NA;
    protected KeyCode kc_showCursor = KeyCode.LeftAlt;
    protected ButtonState showCursor = ButtonState.NA;
    protected KeyCode kc_switchScope = KeyCode.LeftShift;
    protected ButtonState switchScope = ButtonState.NA;

    public float CHorizontal { get => cHorizontal; }
    public float CVertical { get => cVertical; }
    public float CDistance { get => cDistance; }

    public ButtonState SwitchViewPos { get => switchViewPos; }
    public ButtonState ShowCursor { get => showCursor; }
    public ButtonState SwitchScope { get => switchScope; }

    public KeyCode KC_Dock_rotate { get => kc_dock_rotate; }
    public ButtonState Dock_rotate { get => dock_rotate; }

    public KeyCode KC_SwitchViewPos { get => kc_switchViewPos; }
    public KeyCode KC_ShowCursor { get => kc_showCursor; }
    public KeyCode KC_SwitchScope { get => kc_switchScope; }

    //interact
    protected KeyCode kc_interact = KeyCode.F;
    protected ButtonState interact = ButtonState.NA;
    public ButtonState Interact { get => interact; }
    public KeyCode KC_Interact { get => kc_interact; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpaceShipControl();
        CommonCamera();
        CameraInUI();
    }

    protected void SpaceShipControl()
    {
        ButtonMonitor(ref interact, kc_interact);

        if (FreezeShipControl || FreezeControl)
            return;

        throttle = Input.GetAxis("Throttle");
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        pitchAxis = Input.GetAxis("PitchAxis");
        yawAxis = Input.GetAxis("YawAxis");
        rollAxis = Input.GetAxis("RollAxis");

        //ButtonMonitor(ref switchControlMode, "SwitchMode");
        //ButtonMonitor(ref mainFire, "Fire");
        //ButtonMonitor(ref lockOn, "LockOn");

        //ButtonMonitor(ref interact, "Interact");

        ButtonMonitor(ref switchControlMode, kc_switchControlMode);
        ButtonMonitor(ref mainFire, kc_mainFire);
        ButtonMonitor(ref lockOn, kc_lockOn);
        ButtonMonitor(ref missileFire, kc_missileFire);

        
    }

    protected void CommonCamera()
    {
        if (FreezeControl)
            return;

        cHorizontal = Input.GetAxis("MouseX") * xAxisSensitivity;
        cVertical = Input.GetAxis("MouseY") * yAxisSensitivity;
        cDistance = Input.GetAxis("MouseScrollWheel");

        //ButtonMonitor(ref switchViewPos, "SwitchViewPos");
        //ButtonMonitor(ref showCursor, "ShowCursor");
        //ButtonMonitor(ref switchScope, "SwitchScope");

        ButtonMonitor(ref switchViewPos, kc_switchViewPos);
        ButtonMonitor(ref showCursor, kc_showCursor);
        ButtonMonitor(ref switchScope, kc_switchScope);
    }

    protected void CameraInUI()
    {
        if (!MoveDockCamera)
            return;
        cHorizontal = Input.GetAxis("MouseX") * xAxisSensitivity;
        cVertical = Input.GetAxis("MouseY") * yAxisSensitivity;
        ButtonMonitor(ref dock_rotate, kc_dock_rotate);
    }

    protected void ButtonMonitor(ref ButtonState button,string buttonName)
    {
        if (button == ButtonState.UP)
            button = ButtonState.NA;
        if (button == ButtonState.NA && Input.GetButtonDown(buttonName))
            button = ButtonState.DOWN;
        else if (button == ButtonState.DOWN && Input.GetButton(buttonName))
            button = ButtonState.HOLD;
        else if (button == ButtonState.HOLD && Input.GetButtonUp(buttonName))
            button = ButtonState.UP;
    }

    protected void ButtonMonitor(ref ButtonState button, KeyCode kc)
    {
        if (button == ButtonState.UP)
            button = ButtonState.NA;
        if (button == ButtonState.NA && Input.GetKeyDown(kc))
            button = ButtonState.DOWN;
        else if (button == ButtonState.DOWN && Input.GetKey(kc))
            button = ButtonState.HOLD;
        else if (button == ButtonState.HOLD && Input.GetKeyUp(kc))
            button = ButtonState.UP;
    }
}
