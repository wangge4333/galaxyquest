﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetAttribute : MonoBehaviour
{
    [SerializeField]
    protected int planetID = 10003;
    public int PlanetID { get => planetID; }
}
