﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetInteract : MonoBehaviour
{
    protected PlanetMarket market = null;

    // Start is called before the first frame update
    void Start()
    {
        market = GetComponent<PlanetMarket>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<ShipCargo>() == null)
            return;
        if (other.GetComponent<SpaceObject>() == null)
            return;
        if (market == null)
            return;

        market.AddTradeTarget(other.gameObject);
        other.GetComponent<Interact>().SetInteractTarget(market.gameObject);
        if (other.GetComponent<AI_CargoShip>() != null)
            other.GetComponent<AI_CargoShip>().Arrived(market);
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ShipCargo>() == null)
            return;
        if (other.GetComponent<SpaceObject>() == null)
            return;
        if (other.GetComponent<Interact>() == null)
            return;
        if (other.GetComponent<Interact>().InInteracting)
            return;
        market.RemoveTradeTarget(other.gameObject);
        other.GetComponent<Interact>().ResetSetInteractTarget();
    }
}
