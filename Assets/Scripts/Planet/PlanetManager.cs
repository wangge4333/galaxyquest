﻿using ExcelDataReader;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

public class ItemNPlanet
{
    public int planetID = 0;
    public int itemID = 0;

    public ItemNPlanet(int planetID,int itemID)
    {
        this.planetID = planetID;
        this.itemID = itemID;
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        ItemNPlanet temp = obj as ItemNPlanet;
        if (temp == null)
            return false;
        if (temp.planetID == planetID && temp.itemID == itemID)
            return true;
        return false;
    }

    public override int GetHashCode()
    {
        return planetID * 10000 + itemID;
    }
}

public class ItemPricePair
{
    public int itemID = 0;
    public float priceFactor = 0;
    public ItemPricePair(int itemID,float priceFactor)
    {
        this.itemID = itemID;
        this.priceFactor = priceFactor;
    } 
}

public class PlanetManager : MonoBehaviour
{
    [SerializeField]
    protected string fileName = "";

    //markets
    protected Dictionary<int, PlanetMarket> markets = new Dictionary<int, PlanetMarket>();
    protected Dictionary<int, Dictionary<int, float>> marketStatus = new Dictionary<int, Dictionary<int, float>>();
    public Dictionary<int, Dictionary<int, float>> MarketStatus { get => marketStatus; }

    protected Dictionary<int, AI_PlanetStation> stations = new Dictionary<int, AI_PlanetStation>();
    public Dictionary<int ,AI_PlanetStation> Stations { get => stations; }

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        foreach(PlanetMarket pm in GetComponentsInChildren<PlanetMarket>())
        {
            markets.Add(pm.MarketID, pm);
            marketStatus.Add(pm.MarketID, new Dictionary<int, float>());
        }
        LoadFromFile();
    }

    protected void LoadFromFile()
    {
        FileStream fs = File.Open(Application.dataPath + "/Resources/Item/" + fileName + "Production.xlsx",
            FileMode.Open, FileAccess.Read, FileShare.Read);
        IExcelDataReader reader;
        reader = ExcelReaderFactory.CreateOpenXmlReader(fs);

        DataSet result = reader.AsDataSet();
        reader.Close();

        for (int i = 1; i < result.Tables[0].Rows.Count; ++i)
        {
            int starID = Convert.ToInt32(result.Tables[0].Rows[i][0]);
            int itemType = Convert.ToInt32(result.Tables[0].Rows[i][1]) / 1000;
            if (!markets.ContainsKey(starID))
                continue;
            if (itemType == 1)
            {
                markets[starID].SetState(Convert.ToInt32(result.Tables[0].Rows[i][1]),
                    Convert.ToInt32(result.Tables[0].Rows[i][2]),
                    Convert.ToInt32(result.Tables[0].Rows[i][3]),
                    Convert.ToInt32(result.Tables[0].Rows[i][4]));
            }
            else if (itemType == 2)
            {
                markets[starID].SetState(Convert.ToInt32(result.Tables[0].Rows[i][1]),
                    Convert.ToSingle(result.Tables[0].Rows[i][3]));
            }
        }
    }

    public PlanetMarket GetPlanetMarket(int planetID)
    {
        if (markets.ContainsKey(planetID))
            return markets[planetID];
        return null;
    }

    public void UpdatePriceFactor(int planetID,int itemID,float priceFactor)
    {
        //demandList.Add(demandPair);
        if (!marketStatus[planetID].ContainsKey(itemID))
            marketStatus[planetID].Add(itemID, priceFactor);
        else
            marketStatus[planetID][itemID] = priceFactor;
    }

    public PlanetMarket GetMarket(int planetID)
    {
        if (!markets.ContainsKey(planetID))
            return null;
        return markets[planetID];
    }

    public ItemNPlanet RandomlyGetItemNPlanet(int originPlanetID)
    {
        if (!markets.ContainsKey(originPlanetID))
            return null;
        //if (demandDictionary[planetID].Count == 0) 
        //    return null;

        //this planet also needs this kind of item, can buy
        List<int> exclusivePlanets = new List<int>();
        exclusivePlanets.Add(originPlanetID);
        while (exclusivePlanets.Count != markets.Count)
        {
            int targetPlanet = RandomMarketExcept(exclusivePlanets);
            int itemID = RandomTradeItem(originPlanetID, targetPlanet);
            if (itemID != 0)
                return new ItemNPlanet(targetPlanet, itemID);
            exclusivePlanets.Add(targetPlanet);
        }

        return null;
    }

    protected int RandomMarketExcept(List<int> exclusivePlanetID)
    {
        List<int> tempList = new List<int>();
        Dictionary<int, PlanetMarket> tempMarkets = new Dictionary<int, PlanetMarket>(markets);
        
        foreach(int id in exclusivePlanetID)
        {
            tempMarkets.Remove(id);
        }

        foreach (KeyValuePair<int, PlanetMarket> pm in tempMarkets)
        {
            tempList.Add(pm.Key);
        }
        return tempList[UnityEngine.Random.Range(0, tempList.Count)];
    }

    protected int RandomTradeItem(int originPlanetID,int targetPlanetID)
    {
        List<int> avaList = new List<int>();
        foreach(KeyValuePair<int,float> price in marketStatus[originPlanetID])
        {
            float targetPriceFactor = 0;
            if (marketStatus[targetPlanetID].ContainsKey(price.Key))
                targetPriceFactor = marketStatus[targetPlanetID][price.Key];
            else
                targetPriceFactor = markets[targetPlanetID].GetPriceFactor(price.Key);
            if (price.Value >= targetPriceFactor)
                continue;
            avaList.Add(price.Key);
        }
        if (avaList.Count == 0)
            return 0;
        return avaList[UnityEngine.Random.Range(0, avaList.Count)];
    }

    public PlanetMarket NearestMarket(Transform trans)
    {
        float minDis = int.MaxValue;
        PlanetMarket target = null;
        foreach (KeyValuePair<int,PlanetMarket> pm in markets)
        {
            float tempDis = Vector3.Distance(pm.Value.transform.position, trans.position);
            if (tempDis < minDis)
            {
                minDis = tempDis;
                target = pm.Value;
            }
        }
        return target;
    }

    public AI_PlanetStation NearestStation(Transform trans)
    {
        float minDis = int.MaxValue;
        AI_PlanetStation target = null;
        foreach (KeyValuePair<int, AI_PlanetStation> ps in stations)
        {
            float tempDis = Vector3.Distance(ps.Value.transform.position, trans.position);
            if (tempDis < minDis)
            {
                minDis = tempDis;
                target = ps.Value;
            }
        }
        return target;
    }

    public void RegisterPlanetStation(int ID, AI_PlanetStation station)
    {
        stations.Add(ID, station);
    }
}
