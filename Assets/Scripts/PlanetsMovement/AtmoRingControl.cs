﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtmoRingControl : MonoBehaviour
{
    [SerializeField]
    protected Renderer _renderer = null;
    protected GameManager gameManager = null;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        SetShaderProp();
    }

    protected void Initialization()
    {
        _renderer = GetComponent<Renderer>();
        gameManager = FindObjectOfType<GameManager>();
    }

    protected void SetShaderProp()
    {
        if (_renderer == null)
            return;
        _renderer.material.SetVector("_CameraDir", gameManager.MainCamera.transform.forward.normalized);
    }
}
