﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPos : MonoBehaviour
{
    public Transform followTarget = null;

    // Start is called before the first frame update
    void Start()
    {
        if (followTarget != null)
            transform.localScale = new Vector3(1, 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (followTarget != null)
            transform.position = followTarget.position;
    }
}
