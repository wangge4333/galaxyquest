﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRevolution : MonoBehaviour
{
    
    public Transform centralStarPos;

    //The offset of the stellar's position with the centre of the ellipse
    public Vector3 centralStarPosOffset = new Vector3(0, 0, 0);

    //The angular offset of the orbit plane with the stellar
    //yaw pitch roll
    public Vector3 orbitAngularOffset = new Vector3(0, 0, 0);

    //This planet's angular velocity, per day.
    public float angularVelocity = 360.0f / 365.0f;

    //The semi-major of the orbit
    public float orbitSemiMajor = 300.0f;

    //The semi-minor of the orbit
    public float orbitSemiMinor = 300.0f;

    //The direction of orbit
    public bool isClockwise = true;

    //The position of the planet when game starts
    [Range(0.0f,360.0f)]
    public float initialPlanetAngle = 0.0f;

    //planet's pos
    [SerializeField]
    protected float currentAngle = 0.0f;

    //draw orbit
    public LineRenderer orbit = null;
    public bool ifDrawOrbit = true;
    public float orbitWidth = 0.5f;
    public Color32 orbitColor = new Color32(255, 255, 255, 255);

    protected GameManager gameManager = null;
    protected Camera mainCamera = null;

    protected float scaleFactor = 1.0f;

    protected Vector3 lastPos = new Vector3(0, 0, 0);
    protected Vector3 velocity = new Vector3(0, 0, 0);
    public Vector3 Velocity { get => velocity; }

    protected void Awake()
    {
        Initialization();
        InitPosition();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Revolution();
        DrawOrbit();
    }

    protected void Initialization()
    {
        gameManager = FindObjectOfType<GameManager>();

        currentAngle = initialPlanetAngle;
        mainCamera = FindObjectOfType<Camera>();

        scaleFactor = GetComponentInParent<Transform>().lossyScale.x / GetComponentInParent<Transform>().localScale.x;
        InitializeOrbit();

    }

    protected void InitializeOrbit()
    {
        //draw orbit
        if (orbit == null) 
        {
            ifDrawOrbit = false;
            return;
        }
            
        orbit.positionCount = 360;
        for (int i = 0; i < 360; i++)
        {
            orbit.SetPosition(i, (Quaternion.Euler(orbitAngularOffset) *
                   ((GetCurrentPos(i / 180.0f * Mathf.PI) + centralStarPosOffset))));
        }
        orbit.startWidth = orbitWidth;
        orbit.endWidth = orbitWidth;
        orbit.startColor = orbitColor;
        orbit.endColor = orbitColor;
        orbit.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        orbit.receiveShadows = false;
        orbit.loop = true;
    }

    protected void InitPosition()
    {
        Vector3 absPos = GetCurrentPos(initialPlanetAngle / 180.0f * Mathf.PI);
        transform.position = centralStarPos.position + (Quaternion.Euler(orbitAngularOffset) *
            (/*Quaternion.Inverse(centralStarPos.localRotation) **/ (absPos + centralStarPosOffset)))
            * scaleFactor;

    }

    protected void Revolution()
    {
        lastPos = transform.position;

        if (isClockwise)
            currentAngle -= Time.deltaTime * gameManager.GetTimeScale() * angularVelocity;
        else
            currentAngle += Time.deltaTime * gameManager.GetTimeScale() * angularVelocity;

        if (currentAngle < 0.0f)
            currentAngle += 360.0f;
        if (currentAngle > 360.0f)
            currentAngle -= 360.0f;

        Vector3 absPos= GetCurrentPos(currentAngle / 180.0f * Mathf.PI);

        transform.position = centralStarPos.position + (Quaternion.Euler(orbitAngularOffset) * 
            (/*Quaternion.Inverse(centralStarPos.localRotation) **/ (absPos + centralStarPosOffset)))
            * scaleFactor;

        velocity = (transform.position - lastPos) / Time.deltaTime;
    }

    protected Vector3 GetCurrentPos(float currentR)
    {
        Vector3 temp = new Vector3(0, 0, 0);
        if (currentR == Mathf.PI / 2.0f)
        {
            temp.z = orbitSemiMinor;
        }
        else if (currentR == Mathf.PI / 2.0f * 3.0f) 
        {
            temp.z = -orbitSemiMinor;
        }
        else if ((currentR >= 0 && currentR < Mathf.PI / 2.0f) || currentR > Mathf.PI / 2.0f * 3.0f)
        {
            temp.x = orbitSemiMajor * orbitSemiMinor /
            Mathf.Sqrt(orbitSemiMajor * orbitSemiMajor * Mathf.Tan(currentR) * Mathf.Tan(currentR)
            + orbitSemiMinor * orbitSemiMinor);
            temp.z = (orbitSemiMajor * orbitSemiMinor) * Mathf.Tan(currentR) /
            Mathf.Sqrt(orbitSemiMajor * orbitSemiMajor * Mathf.Tan(currentR) * Mathf.Tan(currentR)
            + orbitSemiMinor * orbitSemiMinor);
        }
        else
        {
            temp.x -= orbitSemiMajor * orbitSemiMinor /
            Mathf.Sqrt(orbitSemiMajor * orbitSemiMajor * Mathf.Tan(currentR) * Mathf.Tan(currentR)
            + orbitSemiMinor * orbitSemiMinor);
            temp.z -= (orbitSemiMajor * orbitSemiMinor) * Mathf.Tan(currentR) /
            Mathf.Sqrt(orbitSemiMajor * orbitSemiMajor * Mathf.Tan(currentR) * Mathf.Tan(currentR)
            + orbitSemiMinor * orbitSemiMinor);
            
        }

        return temp;
    }

    protected void DrawOrbit()
    {
        if (orbit == null)
            return;
        //if (mainCamera != null)
        //{
        //    float factor = (mainCamera.transform.position - centralStarPos.position).magnitude / ((orbitSemiMajor + orbitSemiMinor) / 2.0f) / scaleFactor;
        //    Debug.Log(name + factor);
        //    factor = Mathf.Pow(factor, 5);
        //    if (factor < 1.0f)
        //    {
        //        orbit.startWidth=Mathf.Min(15.0f, 0.1f / factor);
        //        orbit.endWidth = Mathf.Min(15.0f, 0.1f / factor);
        //    }
        //    else
        //    {
        //        orbit.startWidth = Mathf.Min(15.0f, 0.01f * factor);
        //        orbit.endWidth = Mathf.Min(15.0f, 0.01f * factor);
        //    }
        //}
        orbit.enabled = ifDrawOrbit;
    }
}
