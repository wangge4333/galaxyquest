﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotation : MonoBehaviour
{
    public Vector3 axisAngle = new Vector3(-23.43f, 0, 0);

    public bool isClockwise = false;

    public float angularVelocity = 1.0f;

    protected GameManager gameManager;



    // Start is called before the first frame update
    void Start()
    {
        
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        SlefRotate();
    }

    protected void Initialization()
    {
        gameManager = FindObjectOfType<GameManager>();
        //initial rotation axis
        transform.rotation = (Quaternion.Euler(axisAngle));
    }

    protected void SlefRotate()
    {
        if (isClockwise)
        {
            transform.Rotate(transform.up, Time.deltaTime * angularVelocity * gameManager.GetTimeScale(), Space.World);
        }
        else
        {
            transform.Rotate(transform.up, -Time.deltaTime * angularVelocity * gameManager.GetTimeScale(), Space.World);
            
        }
        
    }

}
