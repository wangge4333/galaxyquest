﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType
{
    AP,
    HEAT,
    CRASH
}

public delegate void AI_OnBeingAttacked(SpaceObject attacker);
public delegate void OnDie();

public class SpaceObject : MonoBehaviour
{
    protected GameManager gameManager = null;
    protected UIManager uiManager = null;

    protected GQ_UpgradeTree upTree = null;

    [SerializeField]
    protected int id = 10001;
    public int ID { get => id; }

    public string objName = "";
    //temp, clan should be a class
    [SerializeField]
    protected string clan = "";
    //temp
    [SerializeField]
    protected SpaceshipType shipType = null;

    protected int level = 0;
    protected float baseHP = 100000;
    protected float fMaxHP = 100000;
    protected float currentHP = 0;

    //auto repair
    [SerializeField]
    protected float autoRepair = 50;
    protected float lastDamage = 0.0f;
    protected float lastRepair = 0.0f;
    protected float startAutoRepair = 5.0f;

    //this means percent to reduce the damage
    protected float def = 0.0f;
    protected float researchPoint = 0;
    protected float damageInAWhile = 0;
    protected float repairInAWhile = 0;

    protected bool isShown = false;
    protected float showDamageTime = 3.0f;
    protected float showRepairTime = 3.0f;

    public string ShipType { get => shipType.TypeName; }
    public string Name { get => objName; set => objName = value; }
    public string Clan { get => clan; set => clan = value; }

    public int Level { get => level; }
    public float CurrentHP { get => currentHP; }
    public float BaseHP { get => baseHP; }                //加入升级树，通过升级树来获取最终值
    public float FinalMaxHP { get => fMaxHP; }
    public float DEF { get => def; }
    public float DamageInAWhile { get => damageInAWhile; }
    public float RepairInAWhile { get => repairInAWhile; }

    protected bool isDead = false;
    public bool IsDead { get => isDead; }

    public AI_OnBeingAttacked onBeingAttacked = null;
    public OnDie onDie = null;

    [SerializeField]
    protected AudioClip APDamageSFX = null;
    [SerializeField]
    protected AudioClip HEATDamageSFX = null;
    protected float audioOnceTime = 3.0f;
    protected float audioTimer = 0;

    protected void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        CheckLastDamagedRepaired();
        AutoRepair();
        DeathCheck();
        CheckIfShowInfo();
        AudioTimer();
    }

    protected void Initialization()
    {
        //these will be replaced by loading
        level = 1;
        baseHP = 100000;
        def = 0.0f;
        researchPoint = 0;
        fMaxHP = GetFinalMaxHP();
        currentHP = fMaxHP;
        uiManager = FindObjectOfType<UIManager>();
        uiManager.RegisterSpaceship(this);
        gameManager = FindObjectOfType<GameManager>();
        upTree = GetComponent<GQ_UpgradeTree>();

        //test
        if (tag == "Player")
        {
            shipType = SpaceshipType.TestScharnhorst();
        }
        else
        {
            shipType = SpaceshipType.TestOther();
        }
    }

    protected float GetFinalMaxHP()
    {
        if (upTree == null)
            return baseHP;
        return upTree.GetFinalHP(baseHP);
    }

    protected void CheckLastDamagedRepaired()
    {
        if (Time.time - lastDamage > showDamageTime)
            damageInAWhile = 0;
        if (Time.time - lastRepair > showRepairTime)
            repairInAWhile = 0;
    }

    public void BeDamaged(float value, float penetration, GameObject damageOrigin, DamageType type)
    {
        //penetration means defense ignore
        //show on UI
        int finalDamage = Mathf.RoundToInt(value * (100.0f - def * (1.0f - penetration)) / 100.0f);
        currentHP -= finalDamage;
        currentHP = currentHP < 0 ? 0 : currentHP;
        lastDamage = Time.time;

        damageInAWhile += finalDamage;
        onBeingAttacked?.Invoke(damageOrigin.GetComponent<SpaceObject>());

        DamagedAudio(type);
    }

    protected void DamagedAudio(DamageType type)
    {
        if (audioTimer > 0)
            return;
        switch (type)
        {
            case DamageType.AP:
                if (APDamageSFX != null)
                    AudioSource.PlayClipAtPoint(APDamageSFX, transform.position);
                break;
            case DamageType.HEAT:
                if(HEATDamageSFX!=null)
                    AudioSource.PlayClipAtPoint(HEATDamageSFX, transform.position);
                break;
            case DamageType.CRASH:
                break;
        }
        audioTimer = audioOnceTime;
    }

    public void Repair(float value, bool show)
    {
        //show on UI

        currentHP += value;
        currentHP = currentHP > fMaxHP ? fMaxHP : currentHP;
        lastRepair = Time.time;
        repairInAWhile += value;
    }

    protected void AutoRepair()
    {
        if (currentHP == fMaxHP)
            return;
        if ((Time.time - lastDamage) < startAutoRepair)
            return;
        Repair(Time.deltaTime * autoRepair, false);
    }

    protected void DeathCheck()
    {
        if (currentHP > 0)
            return;

        isDead = true;
        onDie?.Invoke();

        if (tag == "Player")
        {
            //if player
            isDead = false;
            //Debug.Log("Player's ship sunk.");
            return;
        }
        //if enemy
        uiManager.DeregisterNPCSimpleInfo(this);
        //Debug.Log(name + "'s ship sunk.");
        //effect

        //destroy
        StartCoroutine(DelaySelfDestroy());
    }

    protected void SelfDestroy()
    {
        Destroy(gameObject);
    }

    protected IEnumerator DelaySelfDestroy()
    {
        WaitForSeconds delay = new WaitForSeconds(3.0f);
        yield return delay;
        SelfDestroy();
    }

    public float GetHPPercent()
    {
        return currentHP / ((float)fMaxHP);
    }

    public float GetDelayHPPercent()
    {
        return (currentHP + damageInAWhile - repairInAWhile) / ((float)fMaxHP);
    }

    protected void CheckIfShowInfo()
    {
        if (tag == "Player")
            return;
        if (!isShown &&
            Vector3.Distance(gameManager.MainPlayer.transform.position, transform.position)
            < DefaultPara.ShowInfoDistance) 
        {
            isShown = true;
            uiManager.RegisterNPCSimpleInfo(this);
        }
        if(isShown&&
            Vector3.Distance(gameManager.MainPlayer.transform.position, transform.position)
            > DefaultPara.HideInfoDistance)
        {
            isShown = false;
            uiManager.DeregisterNPCSimpleInfo(this);
        }
    }

    protected void AudioTimer()
    {
        if (audioTimer <= 0)
            return;
        audioTimer -= Time.deltaTime;
    }

    public void UpdateMaxHP()
    {
        fMaxHP = GetFinalMaxHP();
    }
}
