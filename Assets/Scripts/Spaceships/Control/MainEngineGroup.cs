﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainEngineGroup : MonoBehaviour
{
    public GameObject mainEngineGroup = null;
    public GameObject subEngineGroup = null;

    protected EngineEffect[] mainEngines;
    protected EngineEffect[] subEngines;

    [SerializeField]
    protected float boostSubEngine = 0.5f;
    [SerializeField]
    protected float maxMainEngine = 1.1f;
    [SerializeField]
    protected float maxEngineOutput = 1.3f;

    public float engineThrust = 20.0f;

    [SerializeField, Range(0.0f, 1.5f)]
    protected float engineOutput = 1.0f;

    public float MaxEngineOutput { get => maxEngineOutput; }

    // Start is called before the first frame update
    void Awake()
    {
        if (mainEngineGroup != null)
            mainEngines = mainEngineGroup.GetComponentsInChildren<EngineEffect>();
        if (subEngineGroup != null)
            subEngines = subEngineGroup.GetComponentsInChildren<EngineEffect>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeEngineOutput();
    }

    public void SetEngineOutput(float _engineOutput)
    {
        engineOutput = _engineOutput;
    }

    protected void ChangeEngineOutput()
    {
        foreach (EngineEffect ee in mainEngines)
        {
            ee.EngineOutput = Mathf.Min(maxMainEngine, engineOutput);
        }
        foreach (EngineEffect ee in subEngines)
        {
            ee.EngineOutput = Mathf.Max(0.0f, Mathf.Min(maxEngineOutput - boostSubEngine,
                engineOutput - boostSubEngine));
        }

    }


}
