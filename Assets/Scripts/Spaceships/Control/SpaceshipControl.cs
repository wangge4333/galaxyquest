﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveAxis
{
    Throttle,
    Horizontal,
    Vertical,
    PitchAxis,
    YawAxis,
    Roll
}

public enum ControlMode
{
    Auto,              //This mode means ship's direction will follow the camera's direction

    SemiAuto,                 //Ship's direction can be controled by button
                            //It will still automaticly make the velocity direction as same as ship's direction

    Battle,                  //This mode means ship will maintain its direction, player can use mouse to aim
                            //It won't automaticly change the direction
    TakingOver,             //totally automatic
    MAX
}

public class ThrustState
{
    public float currentPt = 0.0f;
    public float targetPt = 0.0f;
    public float maxPt = 1.0f;
    public float minPt = -1.0f;

    public ThrustState(float _min,float _max)
    {
        maxPt = _max;
        minPt = _min;
    }
}

public class SpaceshipControl : MonoBehaviour
{
    public bool inControl = true;
    protected ControlMode controlMode = ControlMode.SemiAuto;
    protected bool isKeepHorizontal = true;

    public Transform bridgeView = null;

    protected ShipCargo cargo = null;
    protected VectorEngineGroup veControl = null;
    protected MainEngineGroup meControl = null;
    protected Dictionary<MoveAxis, ThrustState> thrustState;

    [SerializeField]
    protected float emptyMass = 1000.0f;
    public float EmptyMass { get => emptyMass; }

    public Transform centreOfMass = null;
    protected Rigidbody _rigidbody = null;

    [SerializeField, Range(0.0f,10.0f)]
    protected float ve2MaxThrSpeed = 0.5f;

    [SerializeField, Range(0.0f, 10.0f)]
    protected float me2MaxThrSpeed = 0.3f;

    [Range(0.1f,20.0f)]
    public float KeyAccel = 3.0f;
    [Range(0.0f, 0.1f)]
    public float throttleThreshold = 0.05f;

    [Range(1.0f, 500.0f), Tooltip("KM/s")]
    public float MaxSpeed = 45.0f;
    [Range(0.1f, 30.0f)]
    public float MaxAngularVelocity = 3.0f;

    //for player's ship only
    protected Vector3 mouseDes = new Vector3(0, 0, 0);      //world space
    public Vector3 MouseDes { get => mouseDes; set => mouseDes = value; }

    protected Vector3 takingOverDes = new Vector3(0, 0, 0);

    public ControlMode CurrentControlMode { get => controlMode; }
    public float MainThrottle { get => thrustState[MoveAxis.Throttle].targetPt; }


    //for AI
    protected Vector3 currentDes = new Vector3(0, 0, 0);
    public Vector3 CurrentDes { get => currentDes; set => currentDes = value; }

    [HideInInspector]
    public bool ifEngineEffect = true;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inControl)
            return;
        UpdateEngineState();
    }

    protected void FixedUpdate()
    {
        AddEngineForce();
    }

    protected void Initialization()
    {
        cargo = GetComponent<ShipCargo>();
        meControl = GetComponentInChildren<MainEngineGroup>();
        veControl = GetComponentInChildren<VectorEngineGroup>();

        thrustState = new Dictionary<MoveAxis, ThrustState>();
        thrustState.Add(MoveAxis.Throttle, new ThrustState(-1.0f, meControl.MaxEngineOutput));
        thrustState.Add(MoveAxis.Horizontal, new ThrustState(-1.0f, 1.0f));
        thrustState.Add(MoveAxis.Vertical, new ThrustState(-1.0f, 1.0f));
        thrustState.Add(MoveAxis.PitchAxis, new ThrustState(-1.0f, 1.0f));
        thrustState.Add(MoveAxis.YawAxis, new ThrustState(-1.0f, 1.0f));
        thrustState.Add(MoveAxis.Roll, new ThrustState(-1.0f, 1.0f));

        _rigidbody = GetComponent<Rigidbody>();

        //reset centre of mass
        if (centreOfMass != null)
        {
            _rigidbody.centerOfMass = centreOfMass.localPosition;
            _rigidbody.ResetInertiaTensor();
        }

        _rigidbody.maxAngularVelocity = MaxAngularVelocity * Mathf.Deg2Rad;
        CalMaxSpeed();
        //cargo.TestIni();
        ResetMass();
    }

    protected void CalMaxSpeed()
    {
        float temp = meControl.engineThrust *
            meControl.MaxEngineOutput / _rigidbody.mass /
            (MaxSpeed / DefaultPara.ToKM) / 1.2f;
        _rigidbody.drag = temp;
        _rigidbody.angularDrag = temp * 10.0f;
    }

    /// <summary>
    /// receive order from inputHandle
    /// </summary>
    /// <param name="axes">order is { Throttle, Horizontal, Vertical, Yaw, Pitch, Roll }</param>
    /// <param name="switchMode"></param>
    public void InputHandle_Player(List<float> axes,bool switchMode)
    {
        if (axes.Count < 6)
            return;
        thrustState[MoveAxis.Throttle].targetPt += axes[0] * Time.deltaTime;
        thrustState[MoveAxis.Throttle].targetPt = Mathf.Clamp(
            thrustState[MoveAxis.Throttle].targetPt, -1.0f, meControl.MaxEngineOutput);     //limit of mainThrottle

        if (controlMode != ControlMode.Battle)
        {
            List<float> am = new List<float>();
            AutoVeloDirCorrect(ref am);


            thrustState[MoveAxis.Horizontal].targetPt = (axes[1] == 0) ? am[0] : axes[1];
            thrustState[MoveAxis.Vertical].targetPt = (axes[2] == 0) ? am[1] : axes[2];
            thrustState[MoveAxis.YawAxis].targetPt = (axes[3] == 0) ? am[2] : axes[3];
            thrustState[MoveAxis.PitchAxis].targetPt = (axes[4] == 0) ? am[3] : axes[4];
            thrustState[MoveAxis.Roll].targetPt = (axes[5] == 0) ? am[4] : axes[5];
        }
        else
        {
            thrustState[MoveAxis.Horizontal].targetPt = axes[1];
            thrustState[MoveAxis.Vertical].targetPt = axes[2];
            thrustState[MoveAxis.YawAxis].targetPt = axes[3];
            thrustState[MoveAxis.PitchAxis].targetPt = axes[4];
            thrustState[MoveAxis.Roll].targetPt = axes[5];
        }

        SwitchControlMode(switchMode);

    }

    //protected void InputHandle()
    //{
    //    if (inputManager == null)
    //        return;

    //    //if(controlMode==ControlMode.Cruise)

    //    thrustState[MoveAxis.Throttle].targetPt += inputManager.Throttle * Time.deltaTime * KeyAccel;
    //    thrustState[MoveAxis.Throttle].targetPt = Mathf.Clamp(
    //        thrustState[MoveAxis.Throttle].targetPt, -1.0f, meControl.MaxEngineOutput);

    //    if (controlMode != ControlMode.Battle)
    //    {
    //        List<float> am = new List<float>();
    //        AutoVeloDirCorrect(ref am);


    //        thrustState[MoveAxis.Horizontal].targetPt = (inputManager.Horizontal == 0) ? am[0] : inputManager.Horizontal;
    //        thrustState[MoveAxis.Vertical].targetPt = (inputManager.Vertical == 0) ? am[1] : inputManager.Vertical;
    //        thrustState[MoveAxis.YawAxis].targetPt = (inputManager.YawAxis == 0) ? am[2] : inputManager.YawAxis;
    //        thrustState[MoveAxis.PitchAxis].targetPt = (inputManager.PitchAxis == 0) ? am[3] : inputManager.PitchAxis;
    //        thrustState[MoveAxis.Roll].targetPt = (inputManager.RollAxis == 0) ? am[4] : inputManager.RollAxis;
    //    }
    //    else
    //    {
    //        thrustState[MoveAxis.Horizontal].targetPt = inputManager.Horizontal;
    //        thrustState[MoveAxis.Vertical].targetPt = inputManager.Vertical;
    //        thrustState[MoveAxis.YawAxis].targetPt = inputManager.YawAxis;
    //        thrustState[MoveAxis.PitchAxis].targetPt = inputManager.PitchAxis;
    //        thrustState[MoveAxis.Roll].targetPt = inputManager.RollAxis;
    //    }

    //    SwitchControlMode();

    //}

    protected void UpdateEngineState()
    {
        foreach(KeyValuePair<MoveAxis,ThrustState> ts in thrustState)
        {
            if (ts.Key == MoveAxis.Throttle)
            {
                if (Mathf.Abs(ts.Value.currentPt - ts.Value.targetPt) < (me2MaxThrSpeed * Time.deltaTime))
                {
                    ts.Value.currentPt = ts.Value.targetPt;
                }
                else
                    ts.Value.currentPt += ts.Value.currentPt > ts.Value.targetPt ? 
                        -me2MaxThrSpeed * Time.deltaTime : +me2MaxThrSpeed * Time.deltaTime;

                
            }
            else
            {
                if (Mathf.Abs(ts.Value.currentPt - ts.Value.targetPt) < (ve2MaxThrSpeed * Time.deltaTime))
                {
                    ts.Value.currentPt = ts.Value.targetPt;
                }
                else
                    ts.Value.currentPt += ts.Value.currentPt > ts.Value.targetPt ?
                        -ve2MaxThrSpeed * Time.deltaTime : +ve2MaxThrSpeed * Time.deltaTime;

            }
            if (ts.Value.currentPt > ts.Value.maxPt || ts.Value.currentPt < ts.Value.minPt)
            {
                ts.Value.currentPt = ts.Value.targetPt;
            }

        }

        if (!ifEngineEffect)
            return;
        meControl.SetEngineOutput(Mathf.Max(0.0f, thrustState[MoveAxis.Throttle].currentPt));
        veControl.Move(MoveType.MoveBack, -Mathf.Min(0.0f, thrustState[MoveAxis.Throttle].currentPt));
        veControl.Move(MoveType.MoveLeft, -Mathf.Min(0.0f, thrustState[MoveAxis.Horizontal].currentPt));
        veControl.Move(MoveType.MoveRight, Mathf.Max(0.0f, thrustState[MoveAxis.Horizontal].currentPt));
        veControl.Move(MoveType.MoveDown, -Mathf.Min(0.0f, thrustState[MoveAxis.Vertical].currentPt));
        veControl.Move(MoveType.MoveUp, Mathf.Max(0.0f, thrustState[MoveAxis.Vertical].currentPt));
        veControl.Move(MoveType.RotateLeft, -Mathf.Min(0.0f, thrustState[MoveAxis.YawAxis].currentPt));
        veControl.Move(MoveType.RotateRight, Mathf.Max(0.0f, thrustState[MoveAxis.YawAxis].currentPt));
        veControl.Move(MoveType.RotateDown, -Mathf.Min(0.0f, thrustState[MoveAxis.PitchAxis].currentPt));
        veControl.Move(MoveType.RotateUp, Mathf.Max(0.0f, thrustState[MoveAxis.PitchAxis].currentPt));
        veControl.Move(MoveType.RollRight, Mathf.Max(0.0f, thrustState[MoveAxis.Roll].currentPt));
        veControl.Move(MoveType.RollLeft, -Mathf.Min(0.0f, thrustState[MoveAxis.Roll].currentPt));
    }

    protected float EngineThreshold(float currentPt)
    {
        if (currentPt < throttleThreshold && currentPt > -throttleThreshold)
            return 0.0f;
        else
            return currentPt;
    }

    protected void AddEngineForce()
    {
        //FORCE of total
        //forward
        Vector3 addedForce = new Vector3(0, 0, 0);
        addedForce += transform.forward * Mathf.Max(0.0f, EngineThreshold(thrustState[MoveAxis.Throttle].currentPt)) * meControl.engineThrust;
        addedForce -= transform.forward * (-Mathf.Min(0.0f, EngineThreshold(thrustState[MoveAxis.Throttle].currentPt)) * veControl.Groups[MoveType.MoveBack].Thrust);
        
        //use this cuz left and right thrust can be different
        addedForce += transform.right * (Mathf.Max(0.0f, thrustState[MoveAxis.Horizontal].currentPt) * veControl.Groups[MoveType.MoveRight].Thrust);
        addedForce -= transform.right * (-Mathf.Min(0.0f, thrustState[MoveAxis.Horizontal].currentPt) * veControl.Groups[MoveType.MoveLeft].Thrust);
        addedForce += transform.up * (Mathf.Max(0.0f, thrustState[MoveAxis.Vertical].currentPt) * veControl.Groups[MoveType.MoveUp].Thrust);
        addedForce -= transform.up * (-Mathf.Min(0.0f, thrustState[MoveAxis.Vertical].currentPt) * veControl.Groups[MoveType.MoveDown].Thrust);

        Vector3 addedTorque = new Vector3(0, 0, 0);
        addedTorque += Vector3.up * (Mathf.Max(0.0f, thrustState[MoveAxis.YawAxis].currentPt) * veControl.Groups[MoveType.RotateRight].Thrust);
        addedTorque += Vector3.up * (Mathf.Min(0.0f, thrustState[MoveAxis.YawAxis].currentPt) * veControl.Groups[MoveType.RotateLeft].Thrust);
        addedTorque -= Vector3.right * (Mathf.Max(0.0f, thrustState[MoveAxis.PitchAxis].currentPt) * veControl.Groups[MoveType.RotateUp].Thrust);
        addedTorque -= Vector3.right * (Mathf.Min(0.0f, thrustState[MoveAxis.PitchAxis].currentPt) * veControl.Groups[MoveType.RotateDown].Thrust);
        addedTorque -= Vector3.forward * (Mathf.Min(0.0f, thrustState[MoveAxis.Roll].currentPt) * veControl.Groups[MoveType.RollLeft].Thrust);
        addedTorque -= Vector3.forward * (Mathf.Max(0.0f, thrustState[MoveAxis.Roll].currentPt) * veControl.Groups[MoveType.RollRight].Thrust);


        transform.Rotate(centreOfMass.up, addedTorque.y / _rigidbody.mass * 50.0f, Space.World);
        transform.Rotate(centreOfMass.right, addedTorque.x / _rigidbody.mass * 50.0f, Space.World);
        transform.Rotate(centreOfMass.forward, addedTorque.z / _rigidbody.mass * 50.0f, Space.World);
        //_rigidbody.AddRelativeTorque(_rigidbody.inertiaTensorRotation * addedTorque, ForceMode.Force);

        _rigidbody.AddForce(addedForce);

        //Debug.Log("Throttle: " + (thrustState[MoveAxis.Throttle].targetPt * 100.0f).ToString("#0") + "%");
    }

    protected void AutoVeloDirCorrect(ref List<float> _aOut)
    {
        _aOut.Add(0); _aOut.Add(0);

        _aOut.Add(0); _aOut.Add(0); _aOut.Add(0);

        Vector3 localV = transform.worldToLocalMatrix * _rigidbody.velocity;
        //Vector3 localAV = transform.worldToLocalMatrix * _rigidbody.angularVelocity;

        if(localV.magnitude!=0.0f&& _rigidbody.velocity.normalized != transform.forward)
        {
            _aOut[0] = Mathf.Clamp(-localV.x * 10.0f, -1.0f, 1.0f);
            _aOut[1] = Mathf.Clamp(-localV.y * 10.0f, -1.0f, 1.0f);
        }
        //if(_rigidbody.angularVelocity.magnitude != 0.0f)
        //{
            //if (controlMode == ControlMode.Cruise)
            //{
            //    _aOut[2] = -Mathf.Clamp(localAV.y /** Mathf.Rad2Deg / 30.0f*/, -1.0f, 1.0f);
            //    _aOut[3] = Mathf.Clamp(localAV.x /** Mathf.Rad2Deg / 30.0f*/, -1.0f, 1.0f);
            //}
            if (controlMode == ControlMode.Auto)
            {
                Vector3 temp;
                AutoFollowDir(mouseDes, out temp);
                _aOut[2] = Mathf.Clamp(temp.x /** Mathf.Rad2Deg / 60.0f*/, -1.0f, 1.0f);
                _aOut[3] = Mathf.Clamp(temp.y /** Mathf.Rad2Deg / 60.0f*/, -1.0f, 1.0f);
                _aOut[4] = Mathf.Clamp(temp.z, -1.0f, 1.0f);
            }
            else if(controlMode == ControlMode.TakingOver)
            {
                Vector3 temp;
                AutoFollowDir(takingOverDes, out temp);
                _aOut[2] = Mathf.Clamp(temp.x /** Mathf.Rad2Deg / 60.0f*/, -1.0f, 1.0f);
                _aOut[3] = Mathf.Clamp(temp.y /** Mathf.Rad2Deg / 60.0f*/, -1.0f, 1.0f);
                _aOut[4] = Mathf.Clamp(temp.z, -1.0f, 1.0f);

            }



            //keep horizontal
            
        //}
        //if (transform.localRotation.z != 0.0f)
        //{
        //    _aOut[4] = Mathf.Clamp(transform.localRotation.z * Mathf.Rad2Deg / 70.0f, -1.0f, 1.0f);
        //}

    }

    protected void AutoVeloDirCorrect(ref List<float> _aOut,Vector3 targetPos)
    {
        _aOut.Add(0); _aOut.Add(0);

        _aOut.Add(0); _aOut.Add(0); _aOut.Add(0);

        Vector3 localV = transform.worldToLocalMatrix * _rigidbody.velocity;
        //Vector3 localAV = transform.worldToLocalMatrix * _rigidbody.angularVelocity;

        if (localV.magnitude != 0.0f && _rigidbody.velocity.normalized != transform.forward)
        {
            _aOut[0] = Mathf.Clamp(-localV.x * 10.0f, -1.0f, 1.0f);
            _aOut[1] = Mathf.Clamp(-localV.y * 10.0f, -1.0f, 1.0f);
        }

        Vector3 temp;
        AutoFollowDir(targetPos, out temp);
        _aOut[2] = Mathf.Clamp(temp.x /** Mathf.Rad2Deg / 60.0f*/, -1.0f, 1.0f);
        _aOut[3] = Mathf.Clamp(temp.y /** Mathf.Rad2Deg / 60.0f*/, -1.0f, 1.0f);
        _aOut[4] = Mathf.Clamp(temp.z, -1.0f, 1.0f);


    }

    protected float AutoFollowPos(Vector3 destination)
    {


        return 0;
    }

    protected void AutoFollowDir(Vector3 destination, out Vector3 _out)
    {
        Vector3 nextDir = destination - transform.position;
        nextDir = (transform.worldToLocalMatrix * nextDir).normalized;
        _out = Vector3.zero;

        if (nextDir == Vector3.zero)
            return;

        _out.x = nextDir.x * 2.0f;
        _out.y = nextDir.y * 2.0f;

        if (isKeepHorizontal)
        {
            if (transform.rotation.eulerAngles.z > 180.0f)
                _out.z = (transform.rotation.eulerAngles.z - 360.0f) / 10.0f;
            else
                _out.z = transform.rotation.eulerAngles.z / 10.0f;
        }
    }

    protected void SwitchControlMode(bool switchMode)
    {
        if (switchMode)
        {
            controlMode++;
            if (controlMode == ControlMode.TakingOver)
                controlMode++;
        }
        if (controlMode == ControlMode.MAX)
            controlMode = 0;
    }

    public void ResetMass()
    {
        if (cargo == null)
        {
            GetComponent<Rigidbody>().mass = emptyMass;
            return;
        }
        GetComponent<Rigidbody>().mass = emptyMass + cargo.CargoWeight;
    }

    public float GetMouseDesDis()
    {
        return Vector3.Distance(mouseDes, transform.position);
    }

    public void SetControlMode(ControlMode cm)
    {
        controlMode = cm;
        if (controlMode == ControlMode.TakingOver)
            thrustState[MoveAxis.Throttle].targetPt = 0.0f;
    }

    public void SetTakingOverDes(Vector3 des)
    {
        takingOverDes = des;
    }

    public void SailTo(Vector3 destination)
    {
        controlMode = ControlMode.Auto;

        thrustState[MoveAxis.Throttle].targetPt = CalMainThrottle(destination);
        thrustState[MoveAxis.Throttle].targetPt = Mathf.Clamp(
            thrustState[MoveAxis.Throttle].targetPt, -1.0f, meControl.MaxEngineOutput);     //limit of mainThrottle

        List<float> am = new List<float>();
        AutoVeloDirCorrect(ref am, destination);


        thrustState[MoveAxis.Horizontal].targetPt = am[0];
        thrustState[MoveAxis.Vertical].targetPt = am[1];
        thrustState[MoveAxis.YawAxis].targetPt = am[2];
        thrustState[MoveAxis.PitchAxis].targetPt = am[3];
        thrustState[MoveAxis.Roll].targetPt = am[4];

    }

    public void SailTo(Vector3 destination, float maxThrottle)
    {
        controlMode = ControlMode.Auto;

        thrustState[MoveAxis.Throttle].targetPt = Mathf.Min(maxThrottle, CalMainThrottle(destination));
        thrustState[MoveAxis.Throttle].targetPt = Mathf.Clamp(
            thrustState[MoveAxis.Throttle].targetPt, -1.0f, meControl.MaxEngineOutput);     //limit of mainThrottle

        List<float> am = new List<float>();
        AutoVeloDirCorrect(ref am, destination);


        thrustState[MoveAxis.Horizontal].targetPt = am[0];
        thrustState[MoveAxis.Vertical].targetPt = am[1];
        thrustState[MoveAxis.YawAxis].targetPt = am[2];
        thrustState[MoveAxis.PitchAxis].targetPt = am[3];
        thrustState[MoveAxis.Roll].targetPt = am[4];

    }


    public void ResetAllThrottle()
    {
        thrustState[MoveAxis.Throttle].targetPt = 0;
        thrustState[MoveAxis.Horizontal].targetPt = 0;
        thrustState[MoveAxis.Vertical].targetPt = 0;
        thrustState[MoveAxis.YawAxis].targetPt = 0;
        thrustState[MoveAxis.PitchAxis].targetPt = 0;
        thrustState[MoveAxis.Roll].targetPt = 0;
    }

    protected float CalMainThrottle(Vector3 destination)
    {
        float maxBackAccel = veControl.Groups[MoveType.MoveBack].Thrust / _rigidbody.mass +
            _rigidbody.velocity.magnitude * _rigidbody.drag;
        float preDistance = _rigidbody.velocity.magnitude * _rigidbody.velocity.magnitude / 2.0f / maxBackAccel;
        if (preDistance >= (Vector3.Distance(transform.position, destination))) 
            return float.MinValue;
        else
            return float.MaxValue;

    }
}
