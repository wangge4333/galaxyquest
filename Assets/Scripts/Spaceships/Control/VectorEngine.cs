﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnginePosition1
{
    Left,
    Middle,
    Right
};

public enum EnginePosition2
{
    Top,
    Middle,
    Bottom
};

public enum EnginePosition3
{
    Front,
    Middle,
    Back
};

public enum EngineDirection
{
    Front,
    Back,
    Left,
    Right,
    Up,
    Down
};

public class VectorEngine : MonoBehaviour
{
    protected EngineEffect[] vectorEngines;

    [SerializeField, Range(0.0f, 1.0f)]
    protected float engineOutput = 0.0f;

    public float engineThrust = 1.0f;

    //description of engine's position
    public EnginePosition1 position1 = EnginePosition1.Left;
    public EnginePosition2 position2 = EnginePosition2.Top;
    public EnginePosition3 position3 = EnginePosition3.Front;
    public EngineDirection direction = EngineDirection.Back;

    public float EngineOutput { get => engineOutput; }

    // Start is called before the first frame update
    void Start()
    {
        vectorEngines = GetComponentsInChildren<EngineEffect>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeEngineOutput();
    }

    public void SetEngineOutput(float _engineOutput)
    {
        engineOutput = Mathf.Clamp(_engineOutput, 0.0f, 1.0f);
    }

    public void ChangeEngineOutput()
    {
        foreach (EngineEffect ee in vectorEngines)
        {
            ee.EngineOutput = engineOutput;
        }
    }
}
