﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveType
{
    MoveForward,
    MoveBack,
    MoveLeft,
    MoveRight,
    MoveUp,
    MoveDown,
    RotateLeft,
    RotateRight,
    RotateUp,
    RotateDown,
    RollLeft,
    RollRight
}

public class GroupOfEngines
{
    public List<VectorEngine> Engines;
    public float Thrust = 0.0f;
    public float Percent = 0.0f;

    public GroupOfEngines()
    {
        Engines = new List<VectorEngine>();
        Thrust = 0.0f;                              //this kind of things mean this group of engines total max force
        Percent = 0.0f;                             //this kind of things mean this group of engines' current thrust of their max thrust
    }

    public void UpdateEngines()
    {
        for(int i = 0; i < Engines.Count; i++)
        {
            Engines[i].SetEngineOutput(Percent);
        }
    }
}

public class VectorEngineGroup : MonoBehaviour
{
    //engine groups
    protected Dictionary<MoveType, GroupOfEngines> groups;

    public Dictionary<MoveType, GroupOfEngines> Groups { get => groups; }

    protected List<VectorEngine> allEngines;

    // Start is called before the first frame update
    void Awake()
    {
        AssignEngines();
    }

    // Update is called once per frame
    void Update()
    {
        ResetPercent();
        AdjustEngine();
    }

    protected void AssignEngines()
    {
        allEngines = new List<VectorEngine>();

        groups = new Dictionary<MoveType, GroupOfEngines>();
        groups.Add(MoveType.MoveBack, new GroupOfEngines());
        groups.Add(MoveType.MoveLeft, new GroupOfEngines());
        groups.Add(MoveType.MoveRight, new GroupOfEngines());
        groups.Add(MoveType.MoveUp, new GroupOfEngines());
        groups.Add(MoveType.MoveDown, new GroupOfEngines());
        groups.Add(MoveType.RotateLeft, new GroupOfEngines());
        groups.Add(MoveType.RotateRight, new GroupOfEngines());
        groups.Add(MoveType.RotateUp, new GroupOfEngines());
        groups.Add(MoveType.RotateDown, new GroupOfEngines());
        groups.Add(MoveType.RollLeft, new GroupOfEngines());
        groups.Add(MoveType.RollRight, new GroupOfEngines());

        //this can be replaced by manual assignment, consider about optimization
        VectorEngine[] temp = GetComponentsInChildren<VectorEngine>();
        foreach (VectorEngine ve in temp)
        {
            allEngines.Add(ve);
            if (ve.direction == EngineDirection.Front)
            {
                groups[MoveType.MoveBack].Engines.Add(ve);
                groups[MoveType.MoveBack].Thrust += ve.engineThrust;
            }
            if (ve.direction == EngineDirection.Right)
            {
                groups[MoveType.MoveLeft].Engines.Add(ve);
                groups[MoveType.MoveLeft].Thrust += ve.engineThrust;
            }
            if (ve.direction == EngineDirection.Left)
            {
                groups[MoveType.MoveRight].Engines.Add(ve);
                groups[MoveType.MoveRight].Thrust += ve.engineThrust;
            }
            if (ve.direction == EngineDirection.Down)
            {
                groups[MoveType.MoveUp].Engines.Add(ve);
                groups[MoveType.MoveUp].Thrust += ve.engineThrust;
            }
            if (ve.direction == EngineDirection.Up)
            {
                groups[MoveType.MoveDown].Engines.Add(ve);
                groups[MoveType.MoveDown].Thrust += ve.engineThrust;
            }

            if (ve.position1 == EnginePosition1.Right&&ve.direction==EngineDirection.Right)
            {
                if (ve.position3 == EnginePosition3.Front)
                {
                    groups[MoveType.RotateLeft].Engines.Add(ve);
                    groups[MoveType.RotateLeft].Thrust += ve.engineThrust;
                } 
                else if (ve.position3 == EnginePosition3.Back)
                {
                    groups[MoveType.RotateRight].Engines.Add(ve);
                    groups[MoveType.RotateRight].Thrust += ve.engineThrust;
                }
                    
            }
            if(ve.position1 == EnginePosition1.Left && ve.direction == EngineDirection.Left)
            {
                if (ve.position3 == EnginePosition3.Front)
                {
                    groups[MoveType.RotateRight].Engines.Add(ve);
                    groups[MoveType.RotateRight].Thrust += ve.engineThrust;
                }
                    
                else if (ve.position3 == EnginePosition3.Back)
                {
                    groups[MoveType.RotateLeft].Engines.Add(ve);
                    groups[MoveType.RotateLeft].Thrust += ve.engineThrust;
                }
                    
            }

            if (ve.position2 == EnginePosition2.Bottom && ve.direction == EngineDirection.Down)
            {
                if (ve.position3 == EnginePosition3.Front)
                {
                    groups[MoveType.RotateUp].Engines.Add(ve);
                    groups[MoveType.RotateUp].Thrust += ve.engineThrust;
                }
                    
                else if (ve.position3 == EnginePosition3.Back)
                {
                    groups[MoveType.RotateDown].Engines.Add(ve);
                    groups[MoveType.RotateDown].Thrust += ve.engineThrust;
                }
                    
            }
            if (ve.position2 == EnginePosition2.Top && ve.direction == EngineDirection.Up)
            {
                if (ve.position3 == EnginePosition3.Front)
                {
                    groups[MoveType.RotateDown].Engines.Add(ve);
                    groups[MoveType.RotateDown].Thrust += ve.engineThrust;
                }
                else if (ve.position3 == EnginePosition3.Back)
                {
                    groups[MoveType.RotateUp].Engines.Add(ve);
                    groups[MoveType.RotateUp].Thrust += ve.engineThrust;
                }
            }

            //rolling on the way
            if(ve.position2 == EnginePosition2.Top)
            {
                if(ve.direction == EngineDirection.Left)
                {
                    groups[MoveType.RollRight].Engines.Add(ve);
                    groups[MoveType.RollRight].Thrust += ve.engineThrust;
                }
                else if (ve.direction == EngineDirection.Right)
                {
                    groups[MoveType.RollLeft].Engines.Add(ve);
                    groups[MoveType.RollLeft].Thrust += ve.engineThrust;
                }
            }

            if (ve.position2 == EnginePosition2.Bottom)
            {
                if (ve.direction == EngineDirection.Right)
                {
                    groups[MoveType.RollRight].Engines.Add(ve);
                    groups[MoveType.RollRight].Thrust += ve.engineThrust;
                }
                else if (ve.direction == EngineDirection.Left) 
                {
                    groups[MoveType.RollLeft].Engines.Add(ve);
                    groups[MoveType.RollLeft].Thrust += ve.engineThrust;
                }
            }

        }
    }

    public void Move(MoveType type,float thrustPt)
    {
        groups[type].Percent = thrustPt;
    }

    
    protected void ResetPercent()
    {
        foreach (VectorEngine ve in allEngines)
            ve.SetEngineOutput(0.0f);
    }

    protected void AdjustEngine()
    {
        foreach(KeyValuePair<MoveType,GroupOfEngines> ge in groups)
        {
            foreach(VectorEngine ve in ge.Value.Engines)
            {
                ve.SetEngineOutput(Mathf.Max(ve.EngineOutput, ge.Value.Percent));
            }
        }
    }

}
