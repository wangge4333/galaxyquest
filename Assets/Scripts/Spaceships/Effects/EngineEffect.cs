﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineEffect : MonoBehaviour
{
    protected float engineOutput = 0.0f;

    [Range(0.0f,10.0f)]
    public float vibrationAmp = 0.1f;

    public float minEffcet = 0.3f;
    public float threshold = 0.1f;

    public GameObject effect = null;
    protected MeshRenderer effRenderer = null;
    public Light flameLight = null;
    [Range(1.0f, 20.0f)]
    public float lightRange = 5.0f;

    public float EngineOutput { get => engineOutput; set => engineOutput = value; }

    // Start is called before the first frame update
    void Start()
    {
        if (effect != null)
            effRenderer = effect.GetComponent<MeshRenderer>();
        if (flameLight != null)
            flameLight.range = lightRange / 300.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (effect == null)
            return;
        EffectLength();
        Vibration();
    }

    protected void Vibration()
    {
        if (!effect.activeInHierarchy)
            return;
        Vector3 temp = new Vector3(Random.Range(-engineOutput, engineOutput),
            Random.Range(-engineOutput, engineOutput),
            Random.Range(-engineOutput, engineOutput));
        temp *= vibrationAmp / 15.0f;
        effect.transform.localPosition = temp;
        
    }

    protected void EffectLength()
    {
        if (engineOutput <= threshold)
        {
            effect.SetActive(false);
            return;
        }
        else
        {
            effect.SetActive(true);
            effRenderer.enabled = true;
        }
        Vector3 temp = new Vector3(1, 1, 1);
        temp.z = Mathf.Max(minEffcet, Mathf.Pow(engineOutput + 0.2f, 2));
        effRenderer.material.color = new Color(1, 1, 1, engineOutput);
        effect.transform.localScale = temp;
        if (flameLight == null)
            return;
        flameLight.intensity = engineOutput/** temp.z*/;
    }
}
