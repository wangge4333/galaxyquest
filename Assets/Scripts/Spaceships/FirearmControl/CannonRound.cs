﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonRound : MonoBehaviour
{
    public float damage = 0;
    public float maxLifeTime = 0.0f;
    public float penetration = 0.0f;

    protected float lifeTime = 0.0f;
    protected GameObject origin = null;
    public GameObject Origin { get => origin; }
    protected Rigidbody _rigidbody = null;

    public Vector3 iniVelocity = new Vector3(0, 0, 0);

    protected bool touched = false;
    protected bool exploded = false;
    protected GameObject fusedTarget = null;

    [SerializeField]
    protected Transform fuse = null;

    [SerializeField]
    protected LayerMask ignoreMask = new LayerMask();
    protected LayerMask exIgnoreMask = new LayerMask();

    protected List<Collider> colliders = new List<Collider>();
    protected bool unlocked = false;

    [SerializeField]
    protected GameObject sparkle_VFX = null;
    //protected LayerMask targetLayer

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
        
    }

    // Update is called once per frame
    void Update()
    {
        CauseDamage();
        MaintainDirection();
        CheckFuse();
        SelfDestroyCheck();
        SelfRotate();
    }

    public void SetOrigin(GameObject origin)
    {
        this.origin = origin;
        exIgnoreMask = (1 << origin.layer) | (1 << gameObject.layer);
        ignoreMask = ignoreMask | exIgnoreMask;
    }

    protected void Initialization()
    {
        lifeTime = 0.0f;
        _rigidbody = GetComponent<Rigidbody>();
        foreach (Collider c in GetComponents<Collider>())
            colliders.Add(c);
        SetCollidersEnable(false);
    }

    protected void SelfDestroyCheck()
    {
        lifeTime += Time.deltaTime;
        if(!unlocked&&lifeTime>0.1f)
        {
            unlocked = true;
            SetCollidersEnable(true);
        }
        if (lifeTime >= maxLifeTime) 
        {
            Explode();
        }
    }

    protected void SelfRotate()
    {
        transform.Rotate(Vector3.up, 720.0f * Time.deltaTime, Space.Self);
    }

    protected void Explode()
    {
        //add some effect?

        Destroy(gameObject);
    }

    //ricochet
    protected void OnCollisionEnter(Collision collision)
    {
        touched = true;
        GenSparkle(collision.GetContact(0));
    }

    protected void GenSparkle(ContactPoint contact)
    {
        Instantiate(sparkle_VFX, contact.point, Quaternion.LookRotation(contact.normal, transform.up), transform.parent);
    }

    //fuse
    //protected void OnTriggerEnter(Collider other)
    //{
    //if (origin.tag == other.tag || other.tag == "Planet" || other.tag == "Star")
    //    return;
    //if (exploded)
    //    return;
    //if (other.GetComponent<SpaceObject>() != null)
    //    other.GetComponent<SpaceObject>().BeDamaged(damage, penetration, origin);

    //Explode();
    //}

    protected void CheckFuse()
    {
        Ray ray = new Ray(fuse.position, _rigidbody.velocity.normalized);
        RaycastHit hitInfo;
        bool isHit = Physics.Raycast(ray, out hitInfo, _rigidbody.velocity.magnitude * Time.deltaTime, ~ignoreMask);
        if (isHit)
        {
            exploded = true;
            fusedTarget = hitInfo.collider.gameObject;
        }
    }

    //useless now
    public void Triggered(GameObject target)
    {
        exploded = true;
        fusedTarget = target;
    }

    protected void CauseDamage()
    {
        if (!exploded || fusedTarget == null)
            return;
        if (fusedTarget.GetComponent<SpaceObject>())
            fusedTarget.GetComponent<SpaceObject>().BeDamaged(damage, penetration, origin, DamageType.AP);
        Explode();
    }

    public void IniRoundDireciton(Vector3 target)
    {
        Vector3 z = target - transform.position;
        z.Normalize();
        Vector3 x = Vector3.Cross(Vector3.up, z);
        Vector3 y = Vector3.Cross(z, x);

        Matrix4x4 rotation = new Matrix4x4(x, z, y, new Vector4(0, 0, 0, 1));
        transform.rotation = rotation.rotation;
    }

    protected void MaintainDirection()
    {
        Vector3 z;
        if (!touched)
            z = iniVelocity.normalized;
        else
            z = _rigidbody.velocity.normalized;
        Vector3 x = Vector3.Cross(Vector3.up, z);
        Vector3 y = Vector3.Cross(z, x);

        Matrix4x4 rotation = new Matrix4x4(x, z, y, new Vector4(0, 0, 0, 1));
        transform.rotation = rotation.rotation;
    }

    protected void SetCollidersEnable(bool isActived)
    {
        foreach (Collider c in colliders)
        {
            c.enabled = isActived;
        }
    }

    
}
