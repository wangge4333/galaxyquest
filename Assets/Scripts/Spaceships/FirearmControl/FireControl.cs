﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponStatus
{
    Main,
    Secondary,
    Flak,
    VLS,                 //Vertical Launching System
    Missile,
    NaN
}

public class FireControl : MonoBehaviour
{
    protected UIManager uiManager = null;
    protected SpaceshipControl _control = null;
    protected Rigidbody _rigidbody = null;

    protected GQ_UpgradeTree upTree = null;
    public GQ_UpgradeTree UpgradeTree { get => upTree; }

    protected Vector3 pointPos = new Vector3(0, 0, 0);
    public Vector3 PointPos { get => pointPos; set => pointPos = value; }

    [SerializeField]
    protected GameObject mainWeaponGroup;
    [SerializeField]
    protected GameObject secondWeaponGroup;
    [SerializeField]
    protected GameObject flakGroup;
    [SerializeField]
    protected GameObject vlsGroup;

    [SerializeField]
    protected Collider raderReflection = null;

    [SerializeField]
    protected LayerMask targetLayer = new LayerMask();
    public LayerMask TargetLayer { get => targetLayer; }

    protected List<WeaponSlotBase> mainWeapons;
    protected List<WeaponSlotBase> secondWeapons;
    protected List<WeaponSlotBase> flaks;
    protected List<WeaponSlotBase> vls;

    public List<WeaponSlotBase> MainWeapons { get => mainWeapons; }
    public List<WeaponSlotBase> SecondWeapons { get => secondWeapons; }
    public List<WeaponSlotBase> Flaks { get => flaks; }
    public List<WeaponSlotBase> VLS { get => vls; }

    [SerializeField, Tooltip("Not in kM")]
    protected float maxRadarDis = 80.0f;


    public GameObject LockedObject = null;
    protected Rigidbody lockedTarget = null;
    protected Vector3 predictedPos = new Vector3(0, 0, 0);
    public Vector3 PredictedPos { get => predictedPos; }

    //upgrade
    protected float cDamageFactor = 1.0f;               //cannon round damage factor
    protected float mDamageFactor = 1.0f;               //missile damage factor
    protected float cReloadFactor = 1.0f;               //cannon turret reloading time factor
    protected float mReloadFactor = 1.0f;               //vls long reloading time factor

    public float CannonDamageFactor { get => cDamageFactor; }
    public float MissileDamageFactor { get => mDamageFactor; }
    public float CannonReloadFactor { get => cReloadFactor; }
    public float MissileReloadFactor { get => mReloadFactor; }

    //for optimization
    [HideInInspector]
    public bool ifRun = true;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        if (!ifRun)
            return;
        if(tag=="Player")
            UpdatePointPos();
        PredictTargetPos();
        UpdateAllWeaponStatus();
    }

    protected void Initialization()
    {
        _control = GetComponent<SpaceshipControl>();
        _rigidbody = GetComponent<Rigidbody>();
        uiManager = FindObjectOfType<UIManager>();

        upTree = GetComponent<GQ_UpgradeTree>();
        if (tag == "Player")
            raderReflection.enabled = false;
        mainWeapons = new List<WeaponSlotBase>();
        secondWeapons = new List<WeaponSlotBase>();
        flaks = new List<WeaponSlotBase>();
        vls = new List<WeaponSlotBase>();
        ResetAllWeapons();
    }

    protected void UpdateAllWeaponStatus()
    {
        foreach(WeaponSlotBase w in mainWeapons)
        {
            w.UpdateByFireControl();
        }
        foreach (WeaponSlotBase w in vls)
        {
            w.UpdateByFireControl();
        }
        foreach (WeaponSlotBase w in flaks)
        {
            w.UpdateByFireControl();
        }
        foreach (WeaponSlotBase w in secondWeapons)
        {
            w.UpdateByFireControl();
        }

    }

    protected void UpdatePointPos()
    {
        pointPos = _control.MouseDes;
    }

    //reset functions should be called when any weapon was replaced
    protected void ResetAllWeapons()
    {
        ResetMainWeapons();
        Reset2ndWeapons();
        ResetFlaks();
        ResetVLS();
    }

    protected void ResetMainWeapons()
    {
        if (mainWeaponGroup == null)
            return;
        mainWeapons.Clear();
        foreach (TurretSeat ts in mainWeaponGroup.GetComponentsInChildren<TurretSeat>())
        {
            mainWeapons.Add(ts);
        }
        mainWeapons.Sort(SortTurret);
    }

    protected void Reset2ndWeapons()
    {
        if (secondWeaponGroup == null)
            return;
        secondWeapons.Clear();
        foreach (TurretSeat ts in secondWeaponGroup.GetComponentsInChildren<TurretSeat>())
        {
            secondWeapons.Add(ts);
        }
        secondWeapons.Sort(SortTurret);
    }

    protected void ResetFlaks()
    {
        if (flakGroup == null)
            return;
        flaks.Clear();
        foreach (TurretSeat ts in flakGroup.GetComponentsInChildren<TurretSeat>())
        {
            flaks.Add(ts);
        }
        flaks.Sort(SortTurret);
    }

    protected void ResetVLS()
    {
        if (vlsGroup == null)
            return;
        vls.Clear();
        foreach (VLSSeat ts in vlsGroup.GetComponentsInChildren<VLSSeat>())
        {
            vls.Add(ts);
        }
        vls.Sort(SortTurret);
    }

    protected int SortTurret(WeaponSlotBase t1, WeaponSlotBase t2)
    {
        if (t1.slotNum > t2.slotNum)
            return 1;
        else if (t1.slotNum < t2.slotNum)
            return -1;
        else
            return 0;
    }

    public void InputHandle_Player(bool mainFire,bool lockOn,bool missileFire)
    {
        if (mainFire)
        {
            FireMainWeapon();
        }
        if (lockOn)
        {
            TryToLock();
        }
        if (missileFire)
        {
            FireMissile();
        }
    }

    public void AI_FireCannon()
    {
        FireMainWeapon();
    }

    public void AI_LaunchMissile()
    {
        FireMissile();
    }

    protected void FireMainWeapon()
    {
        foreach(TurretSeat ts in mainWeapons)
        {
            ts.Fire();
        }
    }

    protected void FireAutoWeapon()
    {

    }

    protected void FireMissile()
    {
        foreach(VLSSeat vlss in vls)
        {
            if (vlss.Fire())
                return;
        }
    }

    protected void TryToLock()
    {
        Ray ray = new Ray(transform.position, _control.MouseDes - transform.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, DefaultPara.HideInfoDistance, LayerMask.GetMask("OnRadar")))
        {
            SetLockedTarget(hit.collider.GetComponentInParent<SpaceObject>().gameObject);
            uiManager.PlayerUnlock(hit.collider.GetComponentInParent<SpaceObject>());
            uiManager.PlayerLock(LockedObject.GetComponent<SpaceObject>());
        }
        else
        {
            if (LockedObject != null)
                uiManager.PlayerUnlock(LockedObject.GetComponent<SpaceObject>());
            LockedObject = null;
            lockedTarget = null;
        }
    }

    public void CheckLock()
    {
        if (LockedObject == null)
            return;
        if (Vector3.Distance(LockedObject.transform.position, transform.position) > maxRadarDis)
        {
            uiManager.PlayerUnlock(LockedObject.GetComponent<SpaceObject>());
            LockedObject = null;
            lockedTarget = null;
        }
    }

    protected void PredictTargetPos()
    {
        if (LockedObject == null)
            return;
        predictedPos = Algo.D2DBallisticPre(lockedTarget.velocity - _rigidbody.velocity,
            transform.position, lockedTarget.transform.position,
            ((TurretSeat)MainWeapons[0]).Turret.RoundSpeed / DefaultPara.ToKM);
    }

    public List<WeaponSlotBase> GetWeaponGroupSeat(WeaponStatus wpStatus)
    {
        switch (wpStatus)
        {
            case WeaponStatus.Main:
                if (MainWeapons.Count != 0)
                    return MainWeapons;
                else
                    return null;
            case WeaponStatus.Secondary:
                if (secondWeapons.Count != 0)
                    return secondWeapons;
                else
                    return null;
            case WeaponStatus.Flak:
                if (flaks.Count != 0)
                    return flaks;
                else
                    return null;
            case WeaponStatus.VLS:
                if (vls.Count != 0)
                    return vls;
                else
                    return null;
            default:
                return null;
        }
    }

    public bool UnequipWeapon(WeaponStatus wpStatus)
    {
        bool tempFlag = false;
        switch (wpStatus)
        {
            case WeaponStatus.Main:
                {
                    if (mainWeapons.Count == 0)
                        return false;
                    foreach (TurretSeat ts in mainWeapons)
                    {
                        if (ts.UnequipWeapon())
                            tempFlag = true;
                    }
                    return tempFlag;
                }
            case WeaponStatus.Secondary:
                {
                    if (secondWeapons.Count == 0)
                        return false;
                    foreach (TurretSeat ts in secondWeapons)
                    {
                        if (ts.UnequipWeapon())
                            tempFlag = true;
                    }
                    return tempFlag;
                }
            case WeaponStatus.Flak:
                {
                    if (flaks.Count == 0)
                        return false;
                    foreach (TurretSeat ts in flaks)
                    {
                        if (ts.UnequipWeapon())
                            tempFlag = true;
                    }
                    return tempFlag;
                }
            case WeaponStatus.VLS:
                {
                    if (vls.Count == 0)
                        return false;
                    foreach (VLSSeat ts in vls)
                    {
                        if (ts.UnequipWeapon())
                            tempFlag = true;
                    }
                    return tempFlag;
                }
            case WeaponStatus.Missile:
                {
                    if (vls.Count == 0 || vls[0].WPOnSeat == null)
                        return false;
                    foreach(VLSSeat ts in vls)
                    {
                        if (((VLS)ts.WPOnSeat).SetUsingMissile(null))
                            tempFlag = true;
                    }

                    return tempFlag;
                }
            default:
                return false;
        }
    }

    /// <summary>
    /// install weapons, returned value means if successfully equiped
    /// </summary>
    /// <param name="weaponID">weapon's ID</param>
    /// <param name="wpStatus">weapon's status, main weapons, secondary weapons, etc.</param>
    /// <param name="ifUnequiped">if unequip any weapons</param>
    /// <returns></returns>
    public bool InstallWeapon(int weaponID, WeaponStatus wpStatus, out bool ifUnequiped)
    {
        GameObject wp = ItemList.GetWeaponPrefab(weaponID);
        switch (wpStatus)
        {
            case WeaponStatus.Main:
                {
                    if (mainWeapons.Count == 0||
                        mainWeapons[0].SeatType != wp.GetComponent<WeaponBase>().WpType)
                    {
                        ifUnequiped = false;
                        return false;
                    }

                    ifUnequiped = UnequipWeapon(wpStatus);

                    foreach (TurretSeat ts in mainWeapons)
                    {
                        ts.SetTurret(wp);
                    }
                    return true;
                }
            case WeaponStatus.Secondary:
                {
                    if (secondWeapons.Count == 0 ||
                        secondWeapons[0].SeatType != wp.GetComponent<WeaponBase>().WpType)
                    {
                        ifUnequiped = false;
                        return false;
                    }
                    ifUnequiped = UnequipWeapon(wpStatus);
                    foreach (TurretSeat ts in secondWeapons)
                    {
                        ts.SetTurret(wp);
                    }
                    return true;
                }
            case WeaponStatus.Flak:
                {
                    if (flaks.Count == 0 ||
                        flaks[0].SeatType != wp.GetComponent<WeaponBase>().WpType)
                    {
                        ifUnequiped = false;
                        return false;
                    }
                    ifUnequiped = UnequipWeapon(wpStatus);
                    foreach (TurretSeat ts in flaks)
                    {
                        ts.SetTurret(wp);
                    }
                    return true;
                }
            case WeaponStatus.VLS:
                {
                    if (vls.Count == 0 ||
                        vls[0].SeatType != wp.GetComponent<WeaponBase>().WpType)
                    {
                        ifUnequiped = false;
                        return false;
                    }
                    int tempMissile = 0;
                    if (((VLSSeat)vls[0])._VLS != null && ((VLSSeat)vls[0])._VLS.UsingMissile != null)
                        tempMissile = ((VLSSeat)vls[0])._VLS.UsingMissile.weaponID;
                    ifUnequiped = UnequipWeapon(wpStatus);
                    
                    foreach (VLSSeat ts in vls)
                    {
                        ts.SetWeapon(wp);
                        if(tempMissile!=0)
                            ts._VLS.SetUsingMissile(ItemList.GetWeaponPrefab(tempMissile));
                    }
                    return true;
                }
            case WeaponStatus.Missile:
                {
                    if (vls.Count == 0 ||
                        vls[0].WPOnSeat == null ||
                        wp.GetComponent<WeaponBase>().WpType != WeaponType.Missile) 
                    {
                        ifUnequiped = false;
                        return false;
                    }
                    ifUnequiped = UnequipWeapon(wpStatus);
                    foreach (VLSSeat ts in vls)
                    {
                        ts._VLS.SetUsingMissile(wp);
                    }
                    return true;
                }
            default:
                ifUnequiped = false;
                return false;
        }
    }

    public void SetLockedTarget(GameObject lockedTARGET)
    {
        LockedObject = lockedTARGET;
        lockedTarget = LockedObject.GetComponent<Rigidbody>();
    }

    public void ResetLockedTarget()
    {
        LockedObject = null;
        lockedTarget = null;
    }

    public void UpadteWeaponUpgrade()
    {
        if (upTree == null)
        {
            cDamageFactor = 1.0f;
            mDamageFactor = 1.0f;
            cReloadFactor = 1.0f;
            mReloadFactor = 1.0f;
            return;
        }
        cDamageFactor = upTree.Ex_CannonDamage;
        mDamageFactor = upTree.Ex_MissileDamage;
        cReloadFactor = upTree.Ex_CannonReload;
        mReloadFactor = upTree.Ex_MissileReload;
    }

}
