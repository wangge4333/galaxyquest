﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : WeaponBase
{
    protected bool exploded = false;
    protected string targetTag = "";
    public string TargetTag { get => targetTag; set => targetTag = value; }

    [SerializeField]
    protected float damage = 0;
    public float Damage { get => damage; }
    [SerializeField,Range(0.0f, 1.0f)]
    protected float penetration = 0;
    public float Penetration { get => penetration; }

    public GameObject origin = null;

    [SerializeField]
    protected GameObject sparkle_VFX = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (targetTag != other.tag)
            return;
        if (exploded)
            return;
        if (other.GetComponent<SpaceObject>())
            other.GetComponent<SpaceObject>().BeDamaged(damage, penetration, origin, DamageType.HEAT);

        Explode();
        
    }

    protected void GenSparkle()
    {
        Instantiate(sparkle_VFX, transform.position,
            Quaternion.LookRotation(-transform.forward, transform.up), transform.parent);
    }

    //protected void OnCollisionEnter(Collision collision)
    //{
    //    if (targetTag != collision.collider.tag)
    //        return;
    //    if (exploded)
    //        return;
    //    if (collision.collider.GetComponent<SpaceObject>())
    //        collision.collider.GetComponent<SpaceObject>().BeDamaged(damage, penetration, origin);

    //    Explode();
    //}



    public void SetOrigin(GameObject _origin)
    {
        origin = _origin;
    }

    public void Explode()
    {
        exploded = true;
        //add some effect?
        GenSparkle();
        if (fireSFX != null)
            AudioSource.PlayClipAtPoint(fireSFX, transform.position);

        Destroy(gameObject);
    }

    public void SetDamageFactor(float factor)
    {
        damage *= factor;
    }
}
