﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileEngines : MonoBehaviour
{
    //engines
    //main engine
    [SerializeField]
    protected EngineEffect main = null;
    //attitude control engines
    [SerializeField]
    protected EngineEffect turnDown = null;
    [SerializeField]
    protected EngineEffect turnUp = null;
    [SerializeField]
    protected EngineEffect turnRight = null;
    [SerializeField]
    protected EngineEffect turnLeft = null;


    // Start is called before the first frame update
    void Start()
    {
        SetEngineStatus(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// set the effect of each engine
    /// </summary>
    /// <param name="mainEngine">main engine's thrust. 0~1.</param>
    /// <param name="vertical">attitude engines thrust. -1~1. negative = down, positive = up.</param>
    /// <param name="horizontal">attitude engines thrust. -1~1. negative = left, positive = right.</param>
    public void SetEngineStatus(float mainEngine,float vertical,float horizontal)
    {
        main.EngineOutput = Mathf.Clamp01(mainEngine);
        turnDown.EngineOutput = Mathf.Clamp(vertical, -1.0f, 0.0f);
        turnUp.EngineOutput = Mathf.Clamp01(vertical);
        turnLeft.EngineOutput= Mathf.Clamp(horizontal, -1.0f, 0.0f);
        turnRight.EngineOutput = Mathf.Clamp01(horizontal);
    }

    public void SetMainEngineStatus(float mainEngine)
    {
        main.EngineOutput = Mathf.Clamp01(mainEngine);
    }

    public void SetACEngineStatus(float vertical, float horizontal)
    {
        turnDown.EngineOutput = Mathf.Abs(Mathf.Clamp(vertical, -1.0f, 0.0f));
        turnUp.EngineOutput = Mathf.Clamp01(vertical);
        turnLeft.EngineOutput = Mathf.Abs(Mathf.Clamp(horizontal, -1.0f, 0.0f));
        turnRight.EngineOutput = Mathf.Clamp01(horizontal);
    }


}
