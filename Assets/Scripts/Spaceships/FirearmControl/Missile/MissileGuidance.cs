﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileGuidance : MonoBehaviour
{
    //target
    protected SpaceObject target = null;
    protected Transform targetPos = null;
    protected Rigidbody targetRigid = null;
    //target's predicted pos, in local space
    protected Vector3 prePos = new Vector3(0, 0, 0);
    //to simulate inertia
    protected Vector3 ini_Velocity = new Vector3(0, 0, 0);

    protected MissileEngines engines = null;
    protected Rigidbody _rigidbody = null;

    //the time since missile was launched
    protected float time = 0;
    [SerializeField]
    protected float life_Time = 30.0f;
    [SerializeField]
    protected float phase_1_time = 2.0f;
    [SerializeField]
    protected float phase_1_velo = 0.1f;
    [SerializeField]
    protected float start_guide_time = 1.0f;
    [SerializeField]
    protected float accel_phase_time = 2.0f;
    [SerializeField]
    protected float rotate_speed = 225.0f;

    [SerializeField, Range(0.1f, 1.0f)]
    protected float accelFactor = 0.65f;

    protected float targetPitch = 0;
    protected float targetYaw = 0;

    protected float currentPitch = 0;
    protected float currentYaw = 0;

    [SerializeField, Tooltip("Missile Speed, in kM/s")]
    protected float speedInKM = 80.0f;
    public float MaxSpeed { get => speedInKM; }
    //speed in unity
    protected float speed = 0;
    [SerializeField]
    protected Transform massCentre = null;

    protected List<Collider> colliders = new List<Collider>();

    // Start is called before the first frame update
    void Start()
    {
        Initiialization();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        SelfDestroy();
        if (target == null)
            return;
        Predict();
        Guide();
        
    }

    protected void FixedUpdate()
    {
        SetVelocity();
    }

    protected void Initiialization()
    {
        engines = GetComponent<MissileEngines>();
        _rigidbody = GetComponent<Rigidbody>();
        speed = speedInKM / DefaultPara.ToKM;
        time = 0;
        engines.SetEngineStatus(0, 0, 0);
        foreach(Collider c in GetComponents<Collider>())
        {
            colliders.Add(c);
        }
        SetCollidersEnable(false);
    }

    public void SetTarget(SpaceObject _target,Vector3 _ini_Velocity)
    {
        target = _target;
        targetPos = _target.transform;
        targetRigid = _target.GetComponent<Rigidbody>();
        ini_Velocity = _ini_Velocity;
        currentPitch = -transform.rotation.eulerAngles.x;
        currentYaw = transform.rotation.eulerAngles.y;
    }

    protected void Predict()
    {
        if (time < start_guide_time)
            return;
        prePos = /*transform.worldToLocalMatrix * */Algo.GuidedBallisticPreSimple(targetRigid.velocity,
            transform.position, targetPos.position,
            speed);
    }

    protected void Guide()
    {
        if (time < start_guide_time)
            return;
        if (target == null)
            return;

        currentYaw = currentYaw > 360.0f ? (currentYaw - 360.0f) : currentYaw;
        currentYaw = currentYaw < 0.0f ? (currentYaw + 360.0f) : currentYaw;
        currentPitch = currentPitch > 360.0f ? (currentPitch - 360.0f) : currentPitch;
        currentPitch = currentPitch < 0.0f ? (currentPitch + 360.0f) : currentPitch;

        Vector3 nextDir = (prePos - transform.position).normalized;
        

        targetYaw = Vector3.Angle(Vector3.forward, new Vector3(nextDir.x, 0, nextDir.z).normalized);
        targetYaw = nextDir.x < 0 ? (360.0f - targetYaw) : targetYaw;

        targetPitch = Vector3.Angle(new Vector3(nextDir.x, 0, nextDir.z).normalized, nextDir);
        targetPitch = nextDir.y < 0 ? (360.0f - targetPitch) : targetPitch;

        if (targetYaw == currentYaw && targetPitch == currentPitch)
            return;

        bool flagHorizontal = Algo.IfClockWise(currentYaw, targetYaw);
        bool flagVertical = Algo.IfClockWise(currentPitch, targetPitch);


        if (Mathf.Abs(currentYaw - targetYaw) < rotate_speed * Time.deltaTime)
            currentYaw = targetYaw;
        else
            currentYaw += flagHorizontal ? rotate_speed * Time.deltaTime : -rotate_speed * Time.deltaTime;
        if (Mathf.Abs(currentPitch - targetPitch) < rotate_speed * Time.deltaTime)
            currentPitch = targetPitch;
        else
            currentPitch += flagVertical ? rotate_speed * Time.deltaTime : -rotate_speed * Time.deltaTime;

        //engines.SetACEngineStatus(flagVertical ? 1 : -1, flagHorizontal ? 1 : -1);
        //Debug.Log(nextDir + " : " + transform.forward);

        //nextDir = (nextDir - transform.forward) * 3.0f;

        engines.SetACEngineStatus((targetPitch - currentPitch) / 15.0f, (targetYaw - currentYaw) / 15.0f);


        transform.rotation = Quaternion.Euler(-currentPitch, currentYaw, 0);
    }

    protected void LoseTarget()
    {
        if (target == null)
            return;
        if(Vector3.Dot(( target.transform.position-transform.position).normalized,transform.forward)<0)
        {
            target = null;
        }
    }

    protected void SetVelocity()
    {
        if(time<= phase_1_time)
        {
            float temp = Mathf.Pow((time / phase_1_time), accelFactor);

            _rigidbody.velocity = (1.0f - temp) * transform.forward * phase_1_velo + ini_Velocity;
            engines.SetMainEngineStatus(0);
            return;
        }
        if(time<= (phase_1_time + accel_phase_time))
        {
            float temp = Mathf.Pow(((time - phase_1_time) / accel_phase_time), accelFactor);

            _rigidbody.velocity = ini_Velocity * (1.0f - temp) + transform.forward * speed * temp;
            engines.SetMainEngineStatus(temp * 3.0f);
            if(!colliders[0].enabled)
                SetCollidersEnable(true);
            return;
        }
        LoseTarget();
        _rigidbody.velocity = transform.forward * speed;
        engines.SetMainEngineStatus(1);
    }

    protected void SelfDestroy()
    {
        if (time < life_Time)
            return;
        GetComponent<Missile>().Explode();
    }

    protected void SetCollidersEnable(bool isActived)
    {
        foreach(Collider c in colliders)
        {
            c.enabled = isActived;
        }
    }
}
