﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundFuse : MonoBehaviour
{
    protected CannonRound round = null;

    protected bool triggered = false;

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    protected void Init()
    {
        round = GetComponentInParent<CannonRound>();
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if (round.Origin.tag == collision.collider.tag) 
            return;
        if (triggered)
            return;
        triggered = true;
        round.Triggered(collision.collider.gameObject);
        Instantiate(new GameObject(), collision.GetContact(0).point, Quaternion.Euler(0, 0, 0));
    }
}
