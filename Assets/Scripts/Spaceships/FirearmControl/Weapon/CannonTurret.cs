﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurretState
{
    CLOCKWISE,
    ANTI_CLOCK,
    UP,
    DOWN,
    ONTARGET,
    CANTMOVE
}

public enum HorizontalStatus
{
    CLOCKWISE,
    ANTI_CLOCK,
    ONTARGET,
    CANTMOVE
}

public enum VerticalStatus
{
    UP,
    DOWN,
    ONTARGET,
    CANTMOVE
}

public class CannonTurret : WeaponBase
{
    public Transform GunBarrel = null;
    protected Animator animator = null;

    [SerializeField]
    protected string turretName = "";
    public string TurretTypeName { get => turretName; }

    //for moving
    //[SerializeField]
    //protected TurretState state = TurretState.ONTARGET;

    [SerializeField]
    protected HorizontalStatus hStatus = HorizontalStatus.ONTARGET;
    public HorizontalStatus HStatus { get => hStatus; }
    [SerializeField]
    protected VerticalStatus vStatus = VerticalStatus.ONTARGET;
    public VerticalStatus VStatus { get => vStatus; }

    //these limit caused by turret itself
    [SerializeField, Range(0.0f, 89.0f)]
    protected float elevaitonLimit = 15.0f;
    public float ElevationLimit { get => elevaitonLimit; }

    [SerializeField, Range(-89.0f, 0.0f)]
    protected float depressionLimit = -5.0f;
    public float DepressionLimit { get => depressionLimit; }

    //Traversing machine speed
    [SerializeField,Range(0.1f,150.0f)]
    protected float traMachineSpeed = 10.0f;
    public float TraMachineSpeed { get => traMachineSpeed; }

    //Elevation machine speed
    [SerializeField, Range(0.1f, 150.0f)]
    protected float eleMahineSpeed = 2.5f;
    public float EleMahineSpeed { get => eleMahineSpeed; }

    [SerializeField]
    protected float currentPitch = 0.0f;
    [SerializeField]
    protected float currentYaw = 0.0f;

    protected float targetPitch = 0.0f;
    protected float targetYaw = 0.0f;

    protected SpaceshipControl _control = null;

    [SerializeField]
    protected Vector3 targetPos = new Vector3(0, 0, 0);

    public Vector3 TargetPos { get => targetPos; }

    //public TurretState State { get => state; }

    //for firing
    [SerializeField, Tooltip("Times/min")]
    protected float firingRate = 5.0f;
    public float FiringRate { get => firingRate; }
    [SerializeField, Tooltip("damage/round")]
    protected float damage = 200;
    public float Damage { get => damage; }
    [SerializeField, Tooltip("Round's penetration, means how much defense will be ignored.")]
    protected float penetration = 0.0f;
    public float Penetration { get => penetration; }
    [SerializeField, Tooltip("Round speed, kM/s")]
    protected float roundSpeed = 90.0f;
    public float RoundSpeed { get => roundSpeed; }
    [SerializeField, Tooltip("Max Range, in unity. not kM.")]
    protected float maxRange = 60.0f;
    protected float roundLifeTime = 0.0f;

    //The time to load 1 round, second
    protected float baseLoadTime = 0.0f;
    public float FinalLoadTime { get => baseLoadTime *
            (seat == null ? 1.0f :
            (seat._FireControl == null ? 1.0f : seat._FireControl.CannonReloadFactor)); }

    protected float FinalDamage {
        get => damage *
        (seat == null ? 1.0f :
        (seat._FireControl == null ? 1.0f : seat._FireControl.CannonDamageFactor));
    }

    protected float cLoadTime = 0.0f;
    [SerializeField]
    protected bool loaded = true;

    [SerializeField]
    protected List<Transform> roundOrigin;

    //round prefab
    [SerializeField]
    protected GameObject round_0 = null;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        //TargetPitchYaw();
        //RotateStatus();
        //Rotate();
        //UpdateTargetPos();
        //LoadRound();
    }

    protected float GetFinalLoadTime()
    {
        if (seat._FireControl.UpgradeTree == null)
            return baseLoadTime;
        return seat._FireControl.UpgradeTree.GetFinalCannonReload(baseLoadTime);
    }

    public override void UpdateByFireControl()
    {
        TargetPitchYaw();
        RotateStatus();
        Rotate();
        UpdateTargetPos();
        LoadRound();
    }

    protected void Initialization()
    {
        seat = GetComponentInParent<WeaponSlotBase>();
        _control = GetComponentInParent<SpaceshipControl>();
        baseLoadTime = 60.0f / firingRate;

        roundLifeTime = maxRange / roundSpeed * DefaultPara.ToKM;
        animator = GetComponent<Animator>();

        cLoadTime = 0;
        loaded = true;
    }

    protected void TargetPitchYaw()
    {
        Vector3 _tar = seat.transform.InverseTransformVector(seat._FireControl.PointPos - seat.transform.position);
        _tar.Normalize();

        targetYaw = Vector3.Angle(Vector3.forward, new Vector3(_tar.x, 0, _tar.z).normalized);
        targetYaw = _tar.x < 0 ? (360.0f - targetYaw) : targetYaw;

        targetPitch = Vector3.Angle(new Vector3(_tar.x, 0, _tar.z).normalized, _tar);
        targetPitch = _tar.y < 0 ? -targetPitch : targetPitch;
    }

    protected void RotateStatus()
    {
        //如果 ABS（目标角度-当前角度）<DeltaTime*每秒转速
        // 当前角度=目标角度->OnTarget
        if (currentYaw == targetYaw && currentPitch == targetPitch)
        {
            //state = TurretState.ONTARGET;
            hStatus = HorizontalStatus.ONTARGET;
            vStatus = VerticalStatus.ONTARGET;
            return;
        }
            

        if (currentPitch > targetPitch)
        {
            //state = TurretState.DOWN;
            vStatus = VerticalStatus.DOWN;

        }
        if (currentPitch < targetPitch)
        {
            //state = TurretState.UP;
            vStatus = VerticalStatus.UP;
        }
            

        if (currentYaw != targetYaw) 
        {
            bool flag = Algo.IfClockWise(currentYaw, targetYaw);
            if (flag)
            {
                //state = TurretState.CLOCKWISE;
                hStatus = HorizontalStatus.CLOCKWISE;
            }
            else
            {
                //state = state = TurretState.ANTI_CLOCK;
                hStatus = HorizontalStatus.ANTI_CLOCK;
            }
                
            if (((TurretSeat)seat).startFiringArea != ((TurretSeat)seat).endFiringArea) 
            {
                if (Algo.IsInAvaAngle(targetYaw, ((TurretSeat)seat).LimitMid, ((TurretSeat)seat).Mid) &&
                         Algo.IsInAvaAngle(currentYaw, ((TurretSeat)seat).Mid, ((TurretSeat)seat).LimitMid))
                {
                    //state = TurretState.ANTI_CLOCK;
                    hStatus = HorizontalStatus.ANTI_CLOCK;
                }
                    

                if (Algo.IsInAvaAngle(currentYaw, ((TurretSeat)seat).LimitMid, ((TurretSeat)seat).Mid) &&
                    Algo.IsInAvaAngle(targetYaw, ((TurretSeat)seat).Mid, ((TurretSeat)seat).LimitMid))
                {
                    //state = TurretState.CLOCKWISE;
                    hStatus = HorizontalStatus.CLOCKWISE;
                }
                    
            }
        }
        
        //next Predict
        float nextYaw = currentYaw;
        float nextPitch = currentPitch;

        switch (hStatus)
        {
            case HorizontalStatus.CLOCKWISE:
                nextYaw += Time.deltaTime * traMachineSpeed;
                break;
            case HorizontalStatus.ANTI_CLOCK:
                nextYaw -= Time.deltaTime * traMachineSpeed;
                break;
            default:
                break;
        }

        switch (vStatus)
        {
            case VerticalStatus.UP:
                nextPitch += Time.deltaTime * eleMahineSpeed;
                break;
            case VerticalStatus.DOWN:
                nextPitch -= Time.deltaTime * eleMahineSpeed;
                break;
            default:
                break;
        }

        if (nextYaw >= 360.0f)
            nextYaw -= 360.0f;
        if (nextYaw < 0.0f)
            nextYaw += 360.0f;

        MinMaxPair nextLimit = GetPitchLimit(nextYaw);

        //if (currentYaw != targetYaw)
        //{
        if (nextPitch < nextLimit.MIN)
        {
            //state = TurretState.UP;
            hStatus = HorizontalStatus.CANTMOVE;
            vStatus = VerticalStatus.UP;
        }
        if (nextPitch > nextLimit.MAX)
        {
            //state = TurretState.DOWN;
            hStatus = HorizontalStatus.CANTMOVE;
            vStatus = VerticalStatus.DOWN;
        }
        if (nextLimit.MIN >= nextLimit.MAX)
        {
            //state = TurretState.CANTMOVE;
            hStatus = HorizontalStatus.CANTMOVE;
            vStatus = VerticalStatus.CANTMOVE;
        }
                
        //}
        //else if (nextPitch < nextLimit.MIN || nextPitch > nextLimit.MAX)
        //    state = TurretState.CANTMOVE;


    }

    protected void Rotate()
    {
        if (targetYaw >= 360.0f)
            targetYaw -= 360.0f;
        if (targetYaw < 0.0f)
            targetYaw += 360.0f;

        switch (hStatus)
        {
            case HorizontalStatus.CLOCKWISE:
                currentYaw += Time.deltaTime * traMachineSpeed;
                break;
            case HorizontalStatus.ANTI_CLOCK:
                currentYaw -= Time.deltaTime * traMachineSpeed;
                break;
            default:
                break;
        }

        switch (vStatus)
        {
            case VerticalStatus.UP:
                currentPitch += Time.deltaTime * eleMahineSpeed;
                break;
            case VerticalStatus.DOWN:
                currentPitch -= Time.deltaTime * eleMahineSpeed;
                break;
            default:
                break;
        }

        if (currentYaw >= 360.0f)
            currentYaw -= 360.0f;
        if (currentYaw < 0.0f)
            currentYaw += 360.0f;

        MinMaxPair limit = GetPitchLimit(currentYaw);
        currentPitch = Mathf.Clamp(currentPitch, limit.MIN, limit.MAX);

        if (((TurretSeat)seat).startFiringArea == ((TurretSeat)seat).endFiringArea ||
            Algo.IsInAvaAngle(targetYaw, ((TurretSeat)seat).startFiringArea, ((TurretSeat)seat).endFiringArea)) 
        {
            if (Mathf.Abs(currentYaw - targetYaw) < Time.deltaTime * TraMachineSpeed * 5.0f) 
                currentYaw = targetYaw;
        }
        if (Mathf.Abs(currentPitch - targetPitch) < Time.deltaTime * EleMahineSpeed * 5.0f) 
            currentPitch = targetPitch;

        if (currentYaw == targetYaw && currentPitch == targetPitch)
        {
            hStatus = HorizontalStatus.ONTARGET;
            vStatus = VerticalStatus.ONTARGET;
            //state = TurretState.ONTARGET;
        }
            

        transform.localRotation = Quaternion.Euler(0,
            currentYaw > 180.0f ? (currentYaw - 360.0f) : currentYaw,
            transform.localRotation.eulerAngles.z);
        if (GunBarrel != null)
        {
            GunBarrel.localRotation = Quaternion.Euler(
                -currentPitch,
                0, 0);
        }

    }

    public void Fire()
    {
        if (round_0 == null)
            return;
        //if (state != TurretState.ONTARGET)
        //    return;
        if (hStatus != HorizontalStatus.ONTARGET || vStatus != VerticalStatus.ONTARGET)
            return;
        if (!loaded)
            return;

        for(int i = 0; i < roundOrigin.Count; ++i)
        {
            GameObject temp = Instantiate(round_0, seat.RoundRoot, true);

            temp.transform.position = roundOrigin[i].position;
            //temp.GetComponent<CannonRound>().IniRoundDireciton(_control.MouseDes);
            temp.GetComponent<Rigidbody>().velocity = _control.GetComponent<Rigidbody>().velocity + 
                roundOrigin[i].forward.normalized * roundSpeed / DefaultPara.ToKM;

            CannonRound temp_0 = temp.GetComponent<CannonRound>();
            temp_0.iniVelocity = roundOrigin[i].forward.normalized * 
                roundSpeed / DefaultPara.ToKM;
            temp_0.maxLifeTime = roundLifeTime;
            temp_0.damage = FinalDamage;
            temp_0.SetOrigin(_control.gameObject);
        }
        if (fireSFX != null)
            AudioSource.PlayClipAtPoint(fireSFX, transform.position);

        animator.SetTrigger("Fire");
        loaded = false;
        cLoadTime = FinalLoadTime;
    }


    protected void LoadRound()
    {
        if (loaded)
            return;
        cLoadTime -= Time.deltaTime;
        if (cLoadTime <= 0)
        {
            loaded = true;
            cLoadTime = 0.0f;
        }
    }

    public void SetIniDirection(float _yaw,float _pitch)
    {
        currentYaw = _yaw;
        currentPitch = _pitch;
    }

    public MinMaxPair GetPitchLimit(float _yaw)
    {
        if (seat == null)
            return new MinMaxPair(depressionLimit, elevaitonLimit);
        MinMaxPair seatLimit = ((TurretSeat)seat).GetPitchLimit(_yaw);
        seatLimit.MIN = Mathf.Max(seatLimit.MIN, depressionLimit);
        seatLimit.MAX = Mathf.Min(seatLimit.MAX, elevaitonLimit);

        return seatLimit;
    }

    //return 0 means can, otherwise means which dir barrel need to move
    //1 means up, -1 means down
    protected float CanRoll(float _nextYaw)
    {
        MinMaxPair temp = GetPitchLimit(_nextYaw);
        if (currentPitch < temp.MIN)
            return 1.0f;
        if (currentPitch > temp.MAX)
            return -1.0f;
        return 0;
    }

    protected void UpdateTargetPos()
    {
        if (_control == null)
            return;
        if (_control.tag != "Player")
            return;

        Ray _ray = new Ray(GunBarrel.position, GunBarrel.forward);
        RaycastHit _hit;

        bool isHit = Physics.Raycast(_ray, out _hit, DefaultPara.FarAIM,
            ~(LayerMask.GetMask("PlayerShip") | LayerMask.GetMask("Round") | LayerMask.GetMask("OnRadar")));

        //Debug.DrawRay(_ray.origin, _ray.direction, Color.red);
        if (!isHit)
            targetPos = GunBarrel.position +
                GunBarrel.forward * Vector3.Distance(GunBarrel.position, _control.MouseDes);
        else
        {
            targetPos = _hit.point;
            //Debug.Log(seat.ID + " : !!!");
        }
            
    }

    public float GetLoadState()
    {
        if (loaded)
            return 1.0f;
        return 1.0f - cLoadTime / FinalLoadTime;
    }

    public string GetRestLoadTime()
    {
        if (loaded)
            return "Loaded";
        return cLoadTime.ToString("#0.0");
    }

}