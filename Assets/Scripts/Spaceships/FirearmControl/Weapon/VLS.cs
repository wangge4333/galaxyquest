﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VLS : WeaponBase
{
    protected FireControl fireControl = null;
    protected Rigidbody shipRigid = null;

    [SerializeField]
    protected string weaponName = "";
    public string WeaponName { get => weaponName; }

    protected bool firePermission = false;
    public bool FirePermission { get => firePermission; }

    [SerializeField]
    protected List<Transform> origins = new List<Transform>();
    public int MissileCapacity { get => origins.Count; }
    [SerializeField]
    protected float shortLoading = 5.0f;
    public float ShortLoading { get => shortLoading; }
    [SerializeField]
    protected float baseLongLoading = 50.0f;
    public float BaseLongLoading { get => baseLongLoading; }

    public float FinalLongLoading {
        get => baseLongLoading *
            (seat == null ? 1.0f :
            (seat._FireControl == null ? 1.0f : seat._FireControl.MissileReloadFactor));
    }

    public float MissileDamageFactor
    {
        get => (seat == null ? 1.0f :
                (seat._FireControl == null ? 1.0f : seat._FireControl.MissileDamageFactor));
    }


    protected int firstPick = 0;
    public int RestMissiles { get => (origins.Count - firstPick); }

    protected float cloadingTime = 0;
    public float RestLoadingTime { get => cloadingTime; }

    protected bool loaded = true;
    public bool IsLoaded { get => loaded; }

    protected bool inShortReloading = false;
    public bool InShortReloading { get => inShortReloading; }
    protected bool inLongReloading = false;
    public bool InLongReloading { get => inLongReloading; }


    [SerializeField]
    protected GameObject missile = null;
    public Missile UsingMissile { get => missile == null ? null : missile.GetComponent<Missile>(); }
    public GameObject UsingMissileObj { get => missile; }


    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        //CheckPermission();
        //Load();
    }

    protected void Initialization()
    {
        
    }

    public override void UpdateByFireControl()
    {
        CheckPermission();
        Load();
    }

    public void SetFireControl(FireControl fc)
    {
        fireControl = fc;
        shipRigid = fc.GetComponent<Rigidbody>();
    }

    public bool Fire()
    {
        if (!loaded || !firePermission)
            return false;
        if (missile == null)
            return false;

        GameObject temp = Instantiate(missile, seat.RoundRoot, true);
        temp.transform.position = origins[firstPick].position;

        if (Vector3.Dot(fireControl.LockedObject.transform.position - fireControl.transform.position,
            fireControl.transform.right) >= 0)
            temp.transform.LookAt(temp.transform.position + transform.up, -fireControl.transform.right);
        else
            temp.transform.LookAt(temp.transform.position + transform.up, fireControl.transform.right);

        temp.GetComponent<MissileGuidance>().SetTarget(
            fireControl.LockedObject.GetComponent<SpaceObject>(),
            shipRigid.velocity);
        Missile temp_0 = temp.GetComponent<Missile>();

        temp_0.SetOrigin(fireControl.gameObject);
        temp_0.TargetTag = fireControl.LockedObject.tag;
        temp_0.SetDamageFactor(MissileDamageFactor);

        temp.transform.localScale = temp.transform.localScale * seat.transform.lossyScale.x;

        FireAudio();

        //load
        StartShortLoad();

        return true;
    }

    protected void FireAudio()
    {
        if (fireSFX == null)
            return;
        AudioSource.PlayClipAtPoint(fireSFX, transform.position);
    }

    protected void Load()
    {
        if (loaded)
            return;
        cloadingTime -= Time.deltaTime;
        if (cloadingTime <= 0)
        {
            loaded = true;
            inLongReloading = false;
            inShortReloading = false;
        }
            
    }

    protected void StartLongLoad()
    {
        if (firstPick != origins.Count)
            return;
        cloadingTime = FinalLongLoading;
        firstPick = 0;
        inShortReloading = false;
        inLongReloading = true;
    }

    protected void StartShortLoad()
    {
        ++firstPick;
        cloadingTime = shortLoading;
        loaded = false;
        inShortReloading = true;
        inLongReloading = false;
        StartLongLoad();
    }

    protected void CheckPermission()
    {
        if (fireControl == null || fireControl.LockedObject == null)
        {
            firePermission = false;
            return;
        }
            
        if (Vector3.Dot(transform.up, fireControl.LockedObject.transform.position - transform.position) < -0.7f)
            firePermission = false;
        else
            firePermission = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_missile"></param>
    /// <returns>if any missile was replaced</returns>
    public bool SetUsingMissile(GameObject _missile)
    {
        if (missile == null)
        {
            missile = _missile;
            return false;
        }
        missile = _missile;
        return true;
    }
}
