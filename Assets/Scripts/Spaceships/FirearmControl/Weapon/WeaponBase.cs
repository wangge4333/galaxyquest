﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBase : MonoBehaviour
{
    public int weaponID = 2601;
    public WeaponSlotBase seat = null;

    [SerializeField]
    protected AudioClip fireSFX = null;

    [SerializeField]
    protected WeaponType weaponType = WeaponType.VLS;
    public WeaponType WpType { get => weaponType; }

    public virtual void UpdateByFireControl()
    {

    }
}
