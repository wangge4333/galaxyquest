﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MinMaxPair
{
    public float MIN;
    public float MAX;

    public MinMaxPair()
    {
        MIN = 0;
        MAX = 0;
    }

    public MinMaxPair(float _min, float _max)
    {
        MIN = _min;
        MAX = _max;
    }
}

public class TurretSeat : WeaponSlotBase
{
    //these limits caused by seat (perhaps blocked by building)
    [Range(0.0f, 360.0f), Tooltip("less than 360, should match with the depressionLimit and elevationLimit.")]
    public List<float> yawSection;

    [Range(-89.0f, 89.0f), Tooltip("depression and elevation are both 0 means this area is blocked")]
    public List<float> depressionLimit;
    [Range(-89.0f, 89.0f)]
    public List<float> elevationLimit;

    //these mean in this pitch, cannon can fire
    [Range(-89.0f, 89.0f)]
    public List<float> fireAllowDepLimit;
    [Range(-89.0f, 89.0f)]
    public List<float> fireAllowEleLimit;

    [Range(0.0f, 360.0f),Tooltip("start == end means full rotation")]
    public float startFiringArea = 0.0f;
    [Range(0.0f, 360.0f)]
    public float endFiringArea = 0.0f;
    //the mid of the angle of traverse
    protected float mid = 0.0f;
    //the mid of the limit area
    protected float limitMid = 0.0f;

    [Range(0.0f, 360.0f)]
    public float iniYaw = 0.0f;
    [Range(0.0f, 89.0f)]
    public float iniPitch = 0.0f;

    

    public CannonTurret Turret { get => (CannonTurret)wpOnSeat; }

    public float Mid { get => mid; }
    public float LimitMid { get => limitMid; }

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
        Initialization();
    }

    protected void Initialization()
    {
        wpOnSeat = GetComponentInChildren<CannonTurret>();
        if (wpOnSeat != null)
        {
            wpOnSeat.seat = this;
            Turret.SetIniDirection(iniYaw, iniPitch);
        }
        if (startFiringArea == endFiringArea) 
            return;
        if (startFiringArea < endFiringArea)
        {
            mid = (startFiringArea + endFiringArea) / 2.0f;
            limitMid = (startFiringArea + endFiringArea) / 2.0f + 180.0f;
        }
        else
        {
            mid = (startFiringArea + endFiringArea) / 2.0f + 180.0f;
            limitMid = (startFiringArea + endFiringArea) / 2.0f;
        }
        mid = mid >= 360.0f ? (mid - 360.0f) : mid;
        limitMid = limitMid >= 360.0f ? (limitMid - 360.0f) : limitMid;
        
           
    }

    public bool UnequipWeapon()
    {
        if (wpOnSeat == null)
            return false;
        Destroy(wpOnSeat.gameObject);
        wpOnSeat = null;
        return true;
    }

    public bool SetTurret(GameObject weapon)
    {
        if (wpOnSeat != null)
            return false;
        GameObject temp = Instantiate(weapon, transform);
        wpOnSeat = temp.GetComponent<CannonTurret>();
        wpOnSeat.seat = this;
        Turret.SetIniDirection(iniYaw, iniPitch);
        return true;
    }

    public MinMaxPair GetPitchLimit(float _yaw)
    {
        if (yawSection.Count < 2)
            return new MinMaxPair(depressionLimit[0], elevationLimit[0]);
        int temp = Algo.FindLowerBound(yawSection, _yaw);
        temp = (temp == (yawSection.Count - 1)) ? 0 : temp;

        return new MinMaxPair(depressionLimit[temp], elevationLimit[temp]);
    }

    public void Fire()
    {
        if (wpOnSeat == null)
            return;
        Turret.Fire();
    }

}
