﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VLSSeat : WeaponSlotBase
{
    public VLS _VLS { get => (VLS)wpOnSeat; }


    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
        Initialization();
    }

    protected void Initialization()
    {
        wpOnSeat = GetComponentInChildren<VLS>();

        //test
        wpOnSeat.seat = this;
        _VLS.SetFireControl(fireControl);

    }

    public bool UnequipWeapon()
    {
        if (wpOnSeat == null)
            return false;
        Destroy(wpOnSeat.gameObject);
        wpOnSeat = null;
        return true;
    }

    public bool SetWeapon(GameObject weapon)
    {
        if (wpOnSeat != null)
            return false;
        GameObject temp = Instantiate(weapon, transform);
        wpOnSeat = temp.GetComponent<VLS>();
        wpOnSeat.seat = this;
        _VLS.SetFireControl(fireControl);
        return true;
    }

    public bool Fire()
    {
        if (wpOnSeat == null)
            return false;
        return _VLS.Fire();
    }
}

