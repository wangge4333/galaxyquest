﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    BigCalibre = 1,                   //>=305mm
    BMCalibre = 2,                    //>=203mm
    MediumCalibre = 3,                //>=152mm
    SmallCalibre = 4,                 //>=76.2mm
    Flak = 5,
    VLS = 6,
    Missile = 7                        //missile need VLS to launch
}

public class WeaponSlotBase : MonoBehaviour
{
    public int slotNum = 1;

    [SerializeField]
    protected WeaponType seatType = WeaponType.BMCalibre;
    public WeaponType SeatType { get => seatType; }

    protected Transform roundRoot = null;
    public Transform RoundRoot { get => roundRoot; }

    protected WeaponBase wpOnSeat = null;
    public WeaponBase WPOnSeat { get => wpOnSeat; }

    protected FireControl fireControl = null;
    public FireControl _FireControl { get => fireControl; }

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        roundRoot = FindObjectOfType<WorldAdjust>().roundRoot;
        fireControl = GetComponentInParent<FireControl>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }

    public virtual void UpdateByFireControl()
    {
        if (wpOnSeat == null)
            return;
        wpOnSeat.UpdateByFireControl();
    }
}
