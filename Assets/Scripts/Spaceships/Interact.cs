﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    protected UIManager uiManager = null;
    protected InputManager inputManager = null;

    protected bool inInteracting = false;
    public bool InInteracting { get => inInteracting; }

    protected SpaceshipControl _control = null;
    protected Rigidbody _rigidbody = null;

    //for interact with planet
    [SerializeField]
    protected GameObject planet = null;
    protected PlanetRevolution planetRev = null;
    protected bool followPlanet = false;
    public bool FollowPlanet { get => followPlanet; set => followPlanet = value; }

    protected Vector3 relativePos = new Vector3(0, 0, 0);
    protected Vector3 lastPos = new Vector3(0, 0, 0);
    //with planet over

    // Start is called before the first frame update
    void Start()
    {
        uiManager = FindObjectOfType<UIManager>();
        _control = GetComponent<SpaceshipControl>();
        _rigidbody = GetComponent<Rigidbody>();
        inputManager = FindObjectOfType<InputManager>();
    }

    protected void Update()
    {
        if (tag == "Player")
        {
            Hint();
        }
            
        StayFollowPlanet();
    }

    public void SetInteractTarget(GameObject _planet)
    {
        planet = _planet;
        planetRev = planet.GetComponent<PlanetRevolution>();
    }

    public void ResetSetInteractTarget()
    {
        planet = null;
        planetRev = null;
    }

    protected void CancelHint()
    {

    }

    protected void Hint()
    {
        //if no hint
        if (planet == null)
        {
            uiManager.ShowHint("");
            return;
        }
        //add all hint into here
        if (planet != null)
        {
            uiManager.ShowHint("Press " + inputManager.KC_Interact.ToString() + " to open planet menu");
            
        }
        
    }

    public void OpenPlanetMenu()
    {
        if (planet == null)
            return;
        if (inInteracting)
            return;

        uiManager.OpenPlanetMenu(planet, GetComponent<SpaceObject>().ID);
        StartFollowPlanet();
    }

    public void StartFollowPlanet()
    {
        if (planet == null)
        {
            return;
        }

        inInteracting = true;

        //auto follow
        followPlanet = true;
        if (_control != null)
            _control.SetControlMode(ControlMode.TakingOver);

        relativePos = transform.position - planet.transform.position;
    }

    public void EndFollowPlanet()
    {
        inInteracting = false;
        followPlanet = false;
        relativePos = Vector3.zero;
        _control.SetTakingOverDes(Vector3.zero);
    }

    public void StayFollowPlanet()
    {
        if (!followPlanet)
        {
            if (_control.CurrentControlMode == ControlMode.TakingOver)
                _control.SetControlMode(ControlMode.Auto);
            return;
        }

        _control.SetTakingOverDes(planet.transform.position + planetRev.Velocity + relativePos);
        _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, planetRev.Velocity, 0.5f * Time.deltaTime);
    }


}
