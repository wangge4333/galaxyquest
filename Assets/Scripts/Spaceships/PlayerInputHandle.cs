﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputHandle : MonoBehaviour
{
    protected InputManager inputManager = null;

    protected SpaceshipControl control = null;
    protected FireControl fireControl = null;
    protected Interact interact = null;

    protected UIManager uiManager = null;

    [Range(0.1f, 5.0f),Tooltip("The sensitivity of W and S")]
    public float KeyAccel = 1.0f;

    protected List<float> inputInfo = new List<float>();

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        if (inputManager == null)
            return;
        SpaceshipControlInput();
        FireControlInput();
        InteractInput();
    }

    protected void Initialization()
    {
        inputManager = FindObjectOfType<InputManager>();
        uiManager = FindObjectOfType<UIManager>();
        control = GetComponent<SpaceshipControl>();
        fireControl = GetComponent<FireControl>();
        interact = GetComponent<Interact>();

    }

    protected void SpaceshipControlInput()
    {
        if (inputManager == null)
            return;
        inputInfo.Clear();

        inputInfo.Add(inputManager.Throttle * KeyAccel);        //Main throttle
        inputInfo.Add(inputManager.Horizontal);
        inputInfo.Add(inputManager.Vertical);
        inputInfo.Add(inputManager.YawAxis);
        inputInfo.Add(inputManager.PitchAxis);
        inputInfo.Add(inputManager.RollAxis);

        bool switchMode = false;
        if (inputManager.SwitchControlMode == ButtonState.DOWN)
            switchMode = true;
        control.InputHandle_Player(inputInfo, switchMode);
    }

    protected void FireControlInput()
    {
        bool fire = false, lockOn = false, missileFire = false;
        if (inputManager.MainFire == ButtonState.DOWN)
            fire = true;
        if (inputManager.LockOn == ButtonState.DOWN)
            lockOn = true;
        if (inputManager.MissileFire == ButtonState.DOWN)
            missileFire = true;
        fireControl.InputHandle_Player(fire, lockOn, missileFire);
        fireControl.CheckLock();
    }

    protected void InteractInput()
    {
        if (inputManager.Interact == ButtonState.DOWN)
        {
            interact.OpenPlanetMenu();
        }
    }
}
