﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GQ_BonusType
{
    EX_HP_VALUE,
    EX_HP_PER,
    EX_SPACE,
    C_DAMAGE,
    M_DAMAGE,
    C_RELOAD,
    M_RELOAD,
    NAN
}

public class GQ_UpTNode
{
    protected GQ_UpgradeTree belongTree = null;
    public GQ_UpgradeTree BelongTree { get => belongTree; }
    protected GQ_BonusType bonusType = GQ_BonusType.NAN;
    public GQ_BonusType BonusType { get => bonusType; }

    protected bool canBeUnlocked = false;
    public bool CanBeUnlocked { get => canBeUnlocked; }
    protected bool isUnlocked = false;
    public bool IsUnlocked { get => isUnlocked; }

    protected string techName = "";
    public string TechName { get => techName; }
    protected string description = "";
    public string Description { get => description; }

    protected GQ_UpTNode parent = null;
    public GQ_UpTNode Parent { get => parent; }
    protected List<GQ_UpTNode> children = null;
    public List<GQ_UpTNode> Children { get => children; }

    protected int unlockCost = 100;
    public int UnlockCost { get => unlockCost; }
    protected int depth = 0;
    public int Depth { get => depth; }

    //for UI only
    public int posColumn = 0;
    public int posRow = 0;

    public GQ_UpTNode(string name, GQ_UpgradeTree belongTree, int unlockCost = 100)
    {
        techName = name;
        this.unlockCost = unlockCost;
        this.belongTree = belongTree;
        children = new List<GQ_UpTNode>();
        CheckCanBeUnlocked();
    }

    public void SetParent(GQ_UpTNode parent)
    {
        this.parent = parent;
        depth = parent.Depth + 1;
        CheckCanBeUnlocked();
    }

    public void AddChild(GQ_UpTNode child)
    {
        children.Add(child);
        child.depth = depth + 1;
        child.SetParent(this);
    }

    public void CheckCanBeUnlocked()
    {
        if (parent == null)
        {
            canBeUnlocked = true;
            return;
        }
        if (parent.IsUnlocked)
        {
            canBeUnlocked = true;
            return;
        }
        canBeUnlocked = false;
    }

    public void UnlockFree()
    {
        if (!canBeUnlocked)
            return;
        isUnlocked = true;
        for (int i = 0; i < children.Count; ++i)
            children[i].SetCanUnlock();
        belongTree.UpdateBonus();
    }

    public void Unlock()
    {
        if (!canBeUnlocked)
            return;
        if (!belongTree.UnlockCost(unlockCost))
            return;
        
        isUnlocked = true;
        for (int i = 0; i < children.Count; ++i)
            children[i].SetCanUnlock();
        belongTree.UpdateBonus();
    }

    public void SetCanUnlock()
    {
        canBeUnlocked = true;   
    }
}

//enhance the HP with certain amount
public class GQ_ExtraHP : GQ_UpTNode
{
    protected float extraHP = 0.0f;
    public float ExtraHP { get => extraHP; }

    public GQ_ExtraHP(string techName, float extraHP, GQ_UpgradeTree belongTree, int upgradeCost = 100) 
        : base(techName, belongTree, upgradeCost)
    {
        this.extraHP = extraHP;
        bonusType = GQ_BonusType.EX_HP_VALUE;
        description = "Increase the hp of your spaceship by " + ExtraHP.ToString("#0");
    }
}

//enhance the HP with a percentage, finalHP = (baseHP + extraHP) * (1 + percentage)
public class GQ_PercenHP : GQ_UpTNode
{
    protected float exHP_Per = 0.0f;
    public float ExHP_Per { get => exHP_Per; }

    public GQ_PercenHP(string techName, float extraHPPer, GQ_UpgradeTree belongTree, int upgradeCost = 100) 
        : base(techName, belongTree, upgradeCost)
    {
        this.exHP_Per = extraHPPer;
        bonusType = GQ_BonusType.EX_HP_PER;
        description = "Increase the hp of your spaceship by " + ExHP_Per.ToString("#0.00") + "%";
    }
}

//
public class GQ_HoldSpace : GQ_UpTNode
{
    protected float extraSpace = 0.0f;
    public float ExtraSpace { get => extraSpace; }

    public GQ_HoldSpace(string techName, float extraSpace, GQ_UpgradeTree belongTree, int upgradeCost = 100) 
        : base(techName, belongTree, upgradeCost)
    {
        this.extraSpace = extraSpace;
        bonusType = GQ_BonusType.EX_SPACE;
        description = "Increase the space of your spaceship's cargohold by " + ExtraSpace.ToString("#0");
    }
}

//enhance the damage of cannon, percentage
public class GQ_CExDamage : GQ_UpTNode
{
    protected float exDamage = 0.0f;
    public float ExDamage { get => exDamage; }

    public GQ_CExDamage(string techName, float exDamage, GQ_UpgradeTree belongTree, int upgradeCost = 100) 
        : base(techName, belongTree, upgradeCost)
    {
        this.exDamage = exDamage;
        bonusType = GQ_BonusType.C_DAMAGE;
        description = "Increase the damage of cannon rounds by " + ExDamage.ToString("#0.00") + "%";
    }
}

//enhance the damage of missile, percentage
public class GQ_MExDamage : GQ_UpTNode
{
    protected float exDamage = 0.0f;
    public float ExDamage { get => exDamage; }

    public GQ_MExDamage(string techName, float exDamage, GQ_UpgradeTree belongTree, int upgradeCost = 100)
        : base(techName, belongTree, upgradeCost)
    {
        this.exDamage = exDamage;
        bonusType = GQ_BonusType.M_DAMAGE;
        description = "Increase the damage of missiles by " + ExDamage.ToString("#0.00") + "%";
    }
}

//reduce the reloading time of cannon, percentage
public class GQ_CFastReload : GQ_UpTNode
{
    protected float rdc_Time = 0.0f;
    public float ReduceTime { get => rdc_Time; }

    public GQ_CFastReload(string techName, float reducingPer, GQ_UpgradeTree belongTree, int upgradeCost = 100) 
        : base(techName, belongTree, upgradeCost)
    {
        rdc_Time = reducingPer;
        bonusType = GQ_BonusType.C_RELOAD;
        description = "Decrease the reloading time of turrets by " + ReduceTime.ToString("#0.00") + "%";
    }
}

//reduce the long reloading time of VLS, percentage
public class GQ_MFastReload : GQ_UpTNode
{
    protected float rdc_Time = 0.0f;
    public float ReduceTime { get => rdc_Time; }

    public GQ_MFastReload(string techName, float reducingPer, GQ_UpgradeTree belongTree, int upgradeCost = 100) 
        : base(techName, belongTree, upgradeCost)
    {
        rdc_Time = reducingPer;
        bonusType = GQ_BonusType.M_RELOAD;
        description = "Decrease the long reloading time of VLS(Vertical Launching System) by " + ReduceTime.ToString("#0.00") + "%";
    }
}

public abstract class GQ_UpgradeTree : MonoBehaviour
{
    //每次升级 遍历一遍树以获取全部加成的和

    //HP_FACTOR
    protected float ex_HP_P = 0;
    public float Ex_HP_Percentage { get => (1.0f + ex_HP_P / 100.0f); }
    //HP_EXTRA
    protected float ex_HP = 0;
    public float Ex_HP_Value { get => ex_HP; }
    //HOLD_EXTRA
    protected float ex_space = 0;
    public float Ex_HoldSpace { get => ex_space; }
    //Cannon damage EXTRA
    protected float ex_cdamage = 0;
    public float Ex_CannonDamage { get => (1.0f + ex_cdamage / 100.0f); }
    //missile damage extra
    protected float ex_mdamage = 0;
    public float Ex_MissileDamage { get => (1.0f + ex_cdamage / 100.0f); }

    //reducing cannon reloading time, percentage
    protected float ex_creload = 0;
    public float Ex_CannonReload { get => (1.0f - ex_creload / 100.0f); }
    //reducing missile reloading time, percentage
    protected float ex_mreload = 0;
    public float Ex_MissileReload { get => (1.0f - ex_mreload / 100.0f); }

    protected GQ_UpTNode root = null;
    public GQ_UpTNode Root { get => root; }

    protected int maxDepth = 0;
    public int MaxDepth { get => maxDepth; }
    protected int maxWidth = 0;
    public int MaxWidth { get => maxWidth; }

    protected int researchPoint = 10000;
    public int ResearchPoint { get => researchPoint; }

    protected virtual void Awake()
    {
        BuildTree();
    }

    protected virtual void Start()
    {
        UpdateBonus();
    }

    //do not forget to check the depth and width after building
    public abstract void BuildTree();

    public virtual void UpdateBonus()
    {
        IterateTree();
        OnUpdateBonus();
    }

    protected virtual void OnUpdateBonus()
    {

    }

    protected virtual void IterateTree()
    {
        InitBonus();
        IterateNode(root);
    }

    /// <summary>
    /// only check unlocked
    /// </summary>
    /// <param name="root"></param>
    protected void IterateNode(GQ_UpTNode root)
    {
        if (root == null)
            return;
        if (!root.IsUnlocked)
            return;
        switch (root.BonusType)
        {
            case GQ_BonusType.EX_HP_VALUE:
                ex_HP += ((GQ_ExtraHP)root).ExtraHP;
                break;
            case GQ_BonusType.EX_HP_PER:
                ex_HP_P += ((GQ_PercenHP)root).ExHP_Per;
                break;
            case GQ_BonusType.EX_SPACE:
                ex_space += ((GQ_HoldSpace)root).ExtraSpace;
                break;
            case GQ_BonusType.C_DAMAGE:
                ex_cdamage += ((GQ_CExDamage)root).ExDamage;
                break;
            case GQ_BonusType.M_DAMAGE:
                ex_mdamage += ((GQ_MExDamage)root).ExDamage;
                break;
            case GQ_BonusType.C_RELOAD:
                ex_creload += ((GQ_CFastReload)root).ReduceTime;
                break;
            case GQ_BonusType.M_RELOAD:
                ex_mreload += ((GQ_MFastReload)root).ReduceTime;
                break;
            case GQ_BonusType.NAN:
                break;
        }
        if (root.Children.Count == 0)
            return;
        foreach(GQ_UpTNode node in root.Children)
        {
            IterateNode(node);
        }
    }

    protected virtual void CheckDepthNWidth()
    {
        maxDepth = 0;
        maxWidth = 0;
        IterateEveryNode(root);
    }

    protected virtual void IterateEveryNode(GQ_UpTNode root)
    {
        if (root == null)
            return;
        maxDepth = Mathf.Max(root.Depth, maxDepth);

        if (root.Children.Count == 0)
        {
            ++maxWidth;
            return;
        }
        foreach (GQ_UpTNode node in root.Children)
        {
            IterateEveryNode(node);
        }
    }

    protected virtual void InitBonus()
    {
        ex_HP_P = 0;
        ex_HP = 0;
        ex_space = 0;
        ex_cdamage = 0;
        ex_mdamage = 0;
        ex_creload = 0;
        ex_mreload = 0;
    }

    public float GetFinalHP(float baseValue)
    {
        return (baseValue + Ex_HP_Value) * (1.0f + Ex_HP_Percentage / 100.0f);
    }

    public float GetFinalHoldSpace(float baseValue)
    {
        return baseValue + Ex_HoldSpace;
    }

    public float GetFinalCannonDamage(float baseValue)
    {
        return baseValue * (1.0f + ex_cdamage / 100.0f);
    }

    public float GetFinalMissileDamage(float baseValue)
    {
        return baseValue * (1.0f + ex_mdamage / 100.0f);
    }

    public float GetFinalCannonReload(float baseValue)
    {
        return baseValue * (1.0f - ex_creload / 100.0f);
    }

    public float GetFinalMissleReload(float baseValue)
    {
        return baseValue * (1.0f - ex_mreload / 100.0f);
    }

    public bool IsEnoughRP(int rp)
    {
        return rp <= researchPoint;
    }

    public bool UnlockCost(int rp)
    {
        if (!IsEnoughRP(rp))
            return false;
        researchPoint -= rp;
        return true;
    }
}
