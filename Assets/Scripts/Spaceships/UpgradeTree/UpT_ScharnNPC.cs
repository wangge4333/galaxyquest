﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpT_ScharnNPC : GQ_UpgradeTree
{
    public override void BuildTree()
    {
        GQ_UpTNode root = new GQ_UpTNode("ROOT", this, 0);
        root.Unlock();
        this.root = root;
    }
}
