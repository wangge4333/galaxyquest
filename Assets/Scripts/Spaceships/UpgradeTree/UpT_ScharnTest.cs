﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpT_ScharnTest : GQ_UpgradeTree
{
    protected SpaceObject spaceObject = null;
    protected ShipCargo shipCargo = null;
    protected FireControl fireControl = null;

    protected override void Awake()
    {
        spaceObject = GetComponent<SpaceObject>();
        shipCargo = GetComponent<ShipCargo>();
        fireControl = GetComponent<FireControl>();
        base.Awake();
    }

    protected override void OnUpdateBonus()
    {
        spaceObject.UpdateMaxHP();
        shipCargo.UpdateSpace();
        fireControl.UpadteWeaponUpgrade();
    }

    protected override void Start()
    {
        InitTree();
        base.Start();
    }

    protected void InitTree()
    {
        root.UnlockFree();
        root.Children[2].UnlockFree();
        root.Children[2].Children[0].UnlockFree();
        root.Children[2].Children[1].UnlockFree();
        root.Children[2].Children[1].Children[0].UnlockFree();

        root.Children[2].Children[0].Children[1].UnlockFree();
    }

    public override void BuildTree()
    {
        GQ_UpTNode root = new GQ_UpTNode("ROOT", this, 0);
        this.root = root;

        //hp line
        GQ_ExtraHP exHP_1 = new GQ_ExtraHP("Advanced Armour Mk.1", 8000, this, 100);
        root.AddChild(exHP_1);
        GQ_ExtraHP exHP_2 = new GQ_ExtraHP("Advanced Armour Mk.2", 6000, this, 200);
        exHP_1.AddChild(exHP_2);
        GQ_ExtraHP exHP_3 = new GQ_ExtraHP("Advanced Armour Mk.3", 8000, this, 400);
        exHP_2.AddChild(exHP_3);
        GQ_ExtraHP exHP_4 = new GQ_ExtraHP("Advanced Armour Mk.4", 12000, this, 800);
        exHP_3.AddChild(exHP_4);
        GQ_ExtraHP exHP_5 = new GQ_ExtraHP("Advanced Armour Mk.5", 16000, this, 1600);
        exHP_4.AddChild(exHP_5);

        GQ_PercenHP exPHP_1 = new GQ_PercenHP("Advanced Structure Type 21", 5, this, 200);
        exHP_1.AddChild(exPHP_1);
        GQ_PercenHP exPHP_2 = new GQ_PercenHP("Advanced Structure Type 25", 10, this, 800);
        exPHP_1.AddChild(exPHP_2);
        GQ_PercenHP exPHP_3 = new GQ_PercenHP("Advanced Structure Type 32", 15, this, 1600);
        exPHP_2.AddChild(exPHP_3);

        //space line
        GQ_HoldSpace exSpace_1 = new GQ_HoldSpace("Space Compression System Mk.1", 10, this, 150);
        root.AddChild(exSpace_1);
        GQ_HoldSpace exSpace_2 = new GQ_HoldSpace("Space Compression System Mk.5", 15, this, 300);
        exSpace_1.AddChild(exSpace_2);
        GQ_HoldSpace exSpace_3 = new GQ_HoldSpace("Space Compression System Mk.7", 20, this, 600);
        exSpace_2.AddChild(exSpace_3);
        GQ_HoldSpace exSpace_4 = new GQ_HoldSpace("Space Compression System Mk.13", 30, this, 800);
        exSpace_3.AddChild(exSpace_4);

        //damage
        //cannon damage line
        GQ_CExDamage ex_cdamage_1 = new GQ_CExDamage("DM13", 5.0f, this, 200);
        root.AddChild(ex_cdamage_1);
        GQ_CExDamage ex_cdamage_2 = new GQ_CExDamage("DM23", 8.0F, this, 300);
        ex_cdamage_1.AddChild(ex_cdamage_2);
        GQ_CExDamage ex_cdamage_3 = new GQ_CExDamage("DM33", 10.0F, this, 800);
        ex_cdamage_2.AddChild(ex_cdamage_3);
        GQ_CExDamage ex_cdamage_4 = new GQ_CExDamage("DM63", 12.0F, this, 1500);
        ex_cdamage_3.AddChild(ex_cdamage_4);
        //cannon reload
        GQ_CFastReload ex_creload_1 = new GQ_CFastReload("Loading Machine Type 23", 5.0f, this, 500); //sfadfas
        ex_cdamage_2.AddChild(ex_creload_1);
        GQ_CFastReload ex_creload_2 = new GQ_CFastReload("Loading Machine Type 24", 10.0f, this, 1000);
        ex_creload_1.AddChild(ex_creload_2);
        GQ_CFastReload ex_creload_3 = new GQ_CFastReload("Loading Machine Type 27", 15.0f, this, 1500);
        ex_creload_2.AddChild(ex_creload_3);

        //missile damage line
        GQ_MExDamage ex_mdamage_1 = new GQ_MExDamage("Warhead Enhance System Mk.1", 8.0f, this, 300);
        ex_cdamage_1.AddChild(ex_mdamage_1);
        GQ_MExDamage ex_mdamage_2 = new GQ_MExDamage("Warhead Enhance System Mk.3", 10.0f, this, 800);
        ex_mdamage_1.AddChild(ex_mdamage_2);
        GQ_MExDamage ex_mdamage_3 = new GQ_MExDamage("Warhead Enhance System Mk.11", 15.0f, this, 1500);
        ex_mdamage_2.AddChild(ex_mdamage_3);
        //missile reload line
        GQ_MFastReload ex_mreload_1 = new GQ_MFastReload("Missile Hoisting System Mk.3", 5.0f, this, 500);
        ex_mdamage_1.AddChild(ex_mreload_1);
        GQ_MFastReload ex_mreload_2 = new GQ_MFastReload("Missile Hoisting System Mk.7", 10.0f, this, 1000);
        ex_mreload_1.AddChild(ex_mreload_2);
        GQ_MFastReload ex_mreload_3 = new GQ_MFastReload("Missile Hoisting System Mk.13", 15.0f, this, 1500);
        ex_mreload_2.AddChild(ex_mreload_3);

        CheckDepthNWidth();
    }

}
