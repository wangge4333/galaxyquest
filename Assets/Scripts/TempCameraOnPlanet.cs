﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempCameraOnPlanet : MonoBehaviour
{
    public Transform target = null;
    public Transform stellar = null;
    public float dis = 10.0f;
    public float offset = 20.0f;

    public Transform target2 = null;
    public float dis2 = 1.0f;

    protected bool mode = true;

    [SerializeField]
    protected float obAngle = 0.0f;

    [Range(0.1f,50.0f)]
    public float rotateSpeed = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Camera>().nearClipPlane = 0.01f;
        GetComponent<Camera>().farClipPlane = 25000.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            mode = !mode;
        
        //if (Input.GetKey(KeyCode.A))
        //{
        //    transform.Rotate(new Vector3(0, 1, 0), Time.deltaTime * -50, Space.World);
        //}
        //if (Input.GetKey(KeyCode.D))
        //{
        //    transform.Rotate(new Vector3(0, 1, 0), Time.deltaTime * 50, Space.World);
        //}
        //if (Input.GetKey(KeyCode.S))
        //{
        //    transform.Rotate(transform.right, Time.deltaTime * 50, Space.World);
        //}
        //if (Input.GetKey(KeyCode.W))
        //{
        //    transform.Rotate(transform.right, Time.deltaTime * -50, Space.World);
        //}
    }

    protected void LateUpdate()
    {
        if (mode)
        {
            if (target != null)
            {
                Transform temp = transform;
                temp.LookAt(stellar);
                transform.position = target.position
                    - dis * (stellar.position - target.position).normalized +
                    (new Vector3(0, 0.3f, 0) - temp.right).normalized * offset;
                transform.LookAt(stellar);
            }
        }
        else
        {
            if (target2 != null)
            {
                RotateAngle();
                //transform.position = target2.position
                //    + dis2 * new Vector3(3, 1, 3).normalized;
                transform.LookAt(target2);
            }
        }


    }

    protected void RotateAngle()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            obAngle += Time.deltaTime * rotateSpeed;
        if (Input.GetKey(KeyCode.LeftArrow))
            obAngle -= Time.deltaTime * rotateSpeed;
        if (obAngle > 360.0f)
            obAngle -= 360.0f;
        if (obAngle < 0.0f)
            obAngle += 360.0f;

        Vector3 temp = new Vector3(0, 0, 0);
        if (obAngle == 90.0f)
            temp.z = 1;
        else if (obAngle == 270.0f)
            temp.z = -1;
        else if(obAngle<90.0f||obAngle>270.0f)
        {
            temp.x = 1;
            temp.z = Mathf.Tan(obAngle * Mathf.Deg2Rad);
        }
        else
        {
            temp.x = -1;
            temp.z = -Mathf.Tan(obAngle * Mathf.Deg2Rad);
        }
        temp.Normalize();
        temp.y = 0.3f;
        transform.position = target2.transform.position + dis2 * temp.normalized;
        
    }
}
