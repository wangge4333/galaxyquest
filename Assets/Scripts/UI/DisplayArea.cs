﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayArea : MonoBehaviour
{
    protected ShipDisplayArea shipDA = null;
    public ShipDisplayArea ShipDisplay { get => shipDA; }

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        shipDA = GetComponentInChildren<ShipDisplayArea>();
    }

    public void SetDisplayedShipWire(string typeName)
    {
        shipDA.SetDisplayedShip(typeName);
    }

    public void EndDisplayingShip()
    {
        shipDA.RemoveDisplayedShip();
    }
}
