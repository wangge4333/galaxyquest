﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DisplayImage : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{
    protected DockUI d_UI = null;

   
    public void SetDockUI(DockUI _d_ui)
    {
        d_UI = _d_ui;
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        if (d_UI != null)
            d_UI._InputManager.MoveDockCamera = true;
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        if (d_UI != null)
            d_UI._InputManager.MoveDockCamera = false;
    }


}
