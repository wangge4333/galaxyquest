﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayedShip : MonoBehaviour
{
    [SerializeField]
    protected GameObject mainWeaponIndex = null;
    [SerializeField]
    protected GameObject missileIndex = null;
    [SerializeField]
    protected GameObject secondWeaponIndex = null;
    [SerializeField]
    protected GameObject flakIndex = null;

    [SerializeField]
    protected List<WeaponPosHighLight> mainWeaponPos = null;
    [SerializeField]
    protected List<WeaponPosHighLight> missilePos = null;
    protected List<WeaponPosHighLight> sndWeaponPos = null;
    protected List<WeaponPosHighLight> flakPos = null;

    protected void Awake()
    {
        Initialization();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        mainWeaponPos = new List<WeaponPosHighLight>();
        missilePos = new List<WeaponPosHighLight>();
        sndWeaponPos = new List<WeaponPosHighLight>();
        flakPos = new List<WeaponPosHighLight>();

        if (mainWeaponIndex != null)
            foreach (WeaponPosHighLight wpl in mainWeaponIndex.GetComponentsInChildren<WeaponPosHighLight>())
                mainWeaponPos.Add(wpl);

        if (missileIndex != null)
            foreach (WeaponPosHighLight wpl in missileIndex.GetComponentsInChildren<WeaponPosHighLight>())
                missilePos.Add(wpl);

        if (secondWeaponIndex != null)
            foreach (WeaponPosHighLight wpl in secondWeaponIndex.GetComponentsInChildren<WeaponPosHighLight>())
                sndWeaponPos.Add(wpl);

        if (flakIndex != null)
            foreach (WeaponPosHighLight wpl in flakIndex.GetComponentsInChildren<WeaponPosHighLight>())
                flakPos.Add(wpl);
    }

    public void SetHighLightMainWeapons()
    {
        if (mainWeaponPos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in mainWeaponPos)
            wpl.SetHightLight();
        SetNormalMissiles();
        SetNormal2ndWeapons();
        SetNormalFlaks();
    }

    public void SetHightLightMissiles()
    {
        if (missilePos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in missilePos)
            wpl.SetHightLight();
        SetNormalMainWeapons();
        SetNormal2ndWeapons();
        SetNormalFlaks();
    }

    public void SetHightLight2ndWeapons()
    {
        if (sndWeaponPos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in sndWeaponPos)
            wpl.SetHightLight();
        SetNormalMainWeapons();
        SetNormalMissiles();
        SetNormalFlaks();
    }

    public void SetHightLightFlaks()
    {
        if (flakPos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in flakPos)
            wpl.SetHightLight();
        SetNormalMainWeapons();
        SetNormalMissiles();
        SetNormal2ndWeapons();
    }

    protected void SetNormalMainWeapons()
    {
        if (mainWeaponPos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in mainWeaponPos)
            wpl.UnsetHightlight();
    }

    protected void SetNormalMissiles()
    {
        if (missilePos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in missilePos)
            wpl.UnsetHightlight();
    }

    protected void SetNormal2ndWeapons()
    {
        if (sndWeaponPos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in sndWeaponPos)
            wpl.UnsetHightlight();
    }

    protected void SetNormalFlaks()
    {
        if (flakPos.Count == 0)
            return;
        foreach (WeaponPosHighLight wpl in flakPos)
            wpl.UnsetHightlight();
    }
}
