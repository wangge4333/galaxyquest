﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DockCamera : MonoBehaviour
{
    protected InputManager inputManager = null;

    [SerializeField]
    protected Transform target = null;
    protected float yaw = 90.0f;
    protected float pitch = 0;

    [SerializeField, Range(30.0f, 89.9f)]
    protected float maxPitch = 75.0f;
    [SerializeField, Range(-89.9f, 20.0f)]
    protected float minPitch = -75.0f;

    protected float distance = 0;

    // Start is called before the first frame update
    void Start()
    {
        inputManager = FindObjectOfType<InputManager>();
        distance = Vector3.Distance(transform.position, target.position);
        pitch = maxPitch;
    }

    // Update is called once per frame
    void Update()
    {
        InputHandle();
    }

    protected void LateUpdate()
    {
        DockCameraUI();
    }

    protected void InputHandle()
    {
        if (inputManager.Dock_rotate != ButtonState.HOLD)
        {
            return;
        }
        inputManager.MoveDockCamera = true;
        yaw -= inputManager.CHorizontal;
        pitch -= inputManager.CVertical;
    }

    protected void DockCameraUI()
    {
        if (target == null)
            return;

        if (yaw > 360.0f)
            yaw -= 360.0f;
        if (yaw < 0.0f)
            yaw += 360.0f;

        pitch = Mathf.Clamp(pitch, minPitch, maxPitch);

        Vector3 temp = new Vector3(0, 0, 0);

        //calculate XZ
        if (yaw == 90.0f)
            temp.z = 1;
        else if (yaw == 270.0f)
            temp.z = -1;
        else if (yaw < 90.0f || yaw > 270.0f)
        {
            temp.x = 1;
            temp.z = Mathf.Tan(yaw * Mathf.Deg2Rad);
        }
        else
        {
            temp.x = -1;
            temp.z = -Mathf.Tan(yaw * Mathf.Deg2Rad);
        }

        //calculate Y
        temp.y = Mathf.Tan(pitch * Mathf.Deg2Rad) * temp.magnitude;
        temp.Normalize();
        temp = Quaternion.AngleAxis(transform.rotation.eulerAngles.z, Vector3.forward) * temp;

        transform.position = target.transform.position + distance * temp;
        transform.LookAt(target.transform.position);
    }
}
