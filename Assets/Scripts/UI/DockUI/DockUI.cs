﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DockUI : MonoBehaviour
{
    protected UIManager uiManager = null;
    protected InputManager inputManager = null;
    public InputManager _InputManager { get => inputManager; }

    protected SpaceObject target = null;
    protected FireControl targetFC = null;
    protected ShipCargo targetHold = null;

    [SerializeField]
    protected Dock_EqWpEle eqEle = null;
    [SerializeField]
    protected DisplayImage displayArea = null;

    //ship displayed area
    protected DisplayArea display = null;

    //the transform of armament list
    [SerializeField]
    protected Transform armamentIndex = null;
    [SerializeField]
    protected GameObject wpListEle = null;
    protected List<Dock_WpListEle> wpList = null;

    [SerializeField]
    protected int iniPoolCapa = 6;

    [SerializeField]
    protected SlideInExpand dockPanel = null;

    //switch weapon status
    [SerializeField]
    protected Button bt_mainWp = null;
    [SerializeField]
    protected Button bt_secondWp = null;
    [SerializeField]
    protected Button bt_flak = null;
    [SerializeField]
    protected Button bt_vls = null;
    [SerializeField]
    protected Button bt_missile = null;

    protected WeaponStatus currentStatus = WeaponStatus.Main;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Initialization()
    {
        wpList = new List<Dock_WpListEle>();
        ExpandPool();
        display = FindObjectOfType<DisplayArea>();
        displayArea.SetDockUI(this);
        eqEle.SetDockUI(this);
        inputManager = FindObjectOfType<InputManager>();
        uiManager = FindObjectOfType<UIManager>();
    }

    public void SetTargetAndOpen(SpaceObject _target)
    {
        dockPanel.gameObject.SetActive(true);

        target = _target;
        targetFC = target.GetComponent<FireControl>();
        targetHold = target.GetComponent<ShipCargo>();
        display.SetDisplayedShipWire(target.ShipType);

        CheckWeaponSlots();
        SwitchWpStatus(currentStatus);
    }

    protected void CheckWeaponSlots()
    {
        //if spaceship doesn't have a kind of weapon slots
        if (targetFC.GetWeaponGroupSeat(WeaponStatus.Main) == null)
            bt_mainWp.interactable = false;
        else
            bt_mainWp.interactable = true;
        if (targetFC.GetWeaponGroupSeat(WeaponStatus.Secondary) == null)
            bt_secondWp.interactable = false;
        else
            bt_secondWp.interactable = true;
        if (targetFC.GetWeaponGroupSeat(WeaponStatus.Flak) == null)
            bt_flak.interactable = false;
        else
            bt_flak.interactable = true;
        if (targetFC.GetWeaponGroupSeat(WeaponStatus.VLS) == null)
            bt_vls.interactable = false;
        else
            bt_vls.interactable = true;
        if (targetFC.GetWeaponGroupSeat(WeaponStatus.VLS)[0].WPOnSeat == null)
            bt_missile.interactable = false;
        else
            bt_missile.interactable = true;
    }

    public void ResetAndHide()
    {
        target = null;
        targetFC = null;
        targetHold = null;
        dockPanel.StartOutAnimation();
    }

    public void SwitchWpStatus(WeaponStatus _wpStatus)
    {
        currentStatus = _wpStatus;
        if(_wpStatus!=WeaponStatus.Missile)
            display.ShipDisplay.SetHighLight(currentStatus);
        else
            display.ShipDisplay.SetHighLight(WeaponStatus.VLS);
        //set
        SetWeaponList();
    }

    public void SwitchToMain()
    {
        SwitchWpStatus(WeaponStatus.Main);
    }

    public void SwitchToSecondary()
    {
        SwitchWpStatus(WeaponStatus.Secondary);
    }

    public void SwitchToFlak()
    {
        SwitchWpStatus(WeaponStatus.Flak);
    }

    public void SwitchToVLS()
    {
        SwitchWpStatus(WeaponStatus.VLS);
    }

    public void SwitchToMissile()
    {
        SwitchWpStatus(WeaponStatus.Missile);
    }

    public void SetWeaponList()
    {
        ResetList();
        List<WeaponSlotBase> temp = null;
        if (currentStatus == WeaponStatus.Missile)
            temp = targetFC.GetWeaponGroupSeat(WeaponStatus.VLS);
        else
            temp = targetFC.GetWeaponGroupSeat(currentStatus);
        int equipedID = 0;

        if(currentStatus!= WeaponStatus.Missile)
        {
            if (temp == null || temp[0].WPOnSeat == null)
            {
                eqEle.SetTarget(currentStatus, null, target);
            }
            else
            {
                eqEle.SetTarget(currentStatus, temp[0], target);
                equipedID = temp[0].WPOnSeat.weaponID;
            }
        }
        else
        {
            if (temp == null || temp[0].WPOnSeat == null || ((VLS)temp[0].WPOnSeat).UsingMissile == null) 
            {
                eqEle.SetTarget(currentStatus, null, target);
            }
            else
            {
                eqEle.SetTarget(currentStatus, temp[0], target);
                equipedID = ((VLS)temp[0].WPOnSeat).UsingMissile.weaponID;
            }
        }
        

        //search equipable weapons in cargo hold
        List<int> equipableList = new List<int>();
        foreach(int weaponID in targetHold.WeaponItems)
        {
            WeaponItem wpItem = (WeaponItem)ItemList.GetItem(weaponID);
            if(currentStatus!=WeaponStatus.Missile)
            {
                if (wpItem.WeaponType == temp[0].SeatType)
                    equipableList.Add(weaponID);
            }
            else
            {
                if (wpItem.WeaponType == WeaponType.Missile)
                    equipableList.Add(weaponID);
            }
        }
        
        //if pool not enuogh expand it
        while (wpList.Count < equipableList.Count)
        {
            ExpandPool();
        }

        //add equipable weapons into the list
        for(int i = 0; i < equipableList.Count; ++i)
        {
            wpList[i].gameObject.SetActive(true);
            wpList[i].SetTarget(equipableList[i], equipedID, currentStatus, target);
        }
    }

    //when anything changed
    public void UpdateWeaponList()
    {
        ResetList();
        SetWeaponList();
    }

    protected void ResetList()
    {
        foreach(Dock_WpListEle dw in wpList)
        {
            dw.TotalReset();
            dw.gameObject.SetActive(false);
        }
    }

    protected void ExpandPool()
    {
        for (int i = 0; i < iniPoolCapa; ++i)
        {
            Dock_WpListEle wle = Instantiate(wpListEle, armamentIndex).GetComponent<Dock_WpListEle>();
            wpList.Add(wle);
            wle.SetDockUI(this);
            wle.SetBGColour((i % 2 == 0) ? DefaultPara.ItemUIColor : DefaultPara.AltItemUIColor);
            wle.gameObject.SetActive(false);
        }
    }

    public void Cancel()
    {
        uiManager.CloseDockUI();
    }

}
