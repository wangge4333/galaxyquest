﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dock_EqWpEle : MonoBehaviour
{
    protected WeaponStatus currentStatus = WeaponStatus.NaN;

    protected DockUI dockUI = null;

    protected WeaponSlotBase target = null;
    protected SpaceObject targetObject = null;
    protected ShipCargo targetHold = null;
    protected FireControl targetFC = null;

    [SerializeField]
    protected Text weaponName = null;
    [SerializeField]
    protected Image weaponIcon = null;
    [SerializeField]
    protected Button unequip = null;

    //base info
    [SerializeField]
    protected Text mass = null;
    [SerializeField]
    protected Text volume = null;

    //weapon info
    [SerializeField]
    protected Text vGuidance = null;
    [SerializeField]
    protected Text tSpeed = null;
    [SerializeField]
    protected Text tvSpeed = null;
    [SerializeField]
    protected Text ignDEF = null;
    [SerializeField]
    protected Text reloading = null;
    [SerializeField]
    protected Text damage = null;
    [SerializeField]
    protected Text muzVelo = null;

    [SerializeField]
    protected Text hint = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetDockUI(DockUI d_ui)
    {
        dockUI = d_ui;
    }

    public void SetTarget(WeaponStatus _wpStatus, WeaponSlotBase _target, SpaceObject _targetObj)
    {
        EmptyInfo();
        currentStatus = _wpStatus;
        target = _target;
        targetObject = _targetObj;
        targetHold = targetObject.GetComponent<ShipCargo>();
        targetFC = targetObject.GetComponent<FireControl>();
        UpdateWeaponInfo();
    }

    //public void TotalReset()
    //{
    //    target = null;
    //    targetObject = null;
    //    targetHold = null;
    //    targetFC = null;
    //    EmptyInfo();
    //}

    public void UpdateWeaponInfo()
    {
        if (target == null || target.WPOnSeat == null)
        {
            EmptyInfo();
            return;
        }
        if (currentStatus == WeaponStatus.Missile && ((VLSSeat)target)._VLS.UsingMissile == null) 
        {
            EmptyInfo();
            return;
        }
        switch (target.SeatType)
        {
            case WeaponType.VLS:
                {
                    if (currentStatus == WeaponStatus.VLS)
                    {
                        UpdateVLSInfo();
                        break;
                    }
                    if (currentStatus == WeaponStatus.Missile)
                    {
                        UpdateMissileInfo();
                        break;
                    }
                    break;
                }  
            default:
                UpdateTurretInfo();
                break;
        }
    }

    protected void UpdateTurretInfo()
    {

        WeaponItem item = (WeaponItem)ItemList.GetItem(((TurretSeat)target).Turret.weaponID);

        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        //if (item.WeaponType != WeaponType.Missile)
        //{
            vGuidance.text = ((TurretSeat)target).Turret.DepressionLimit.ToString("#0.0") + "° ~ " +
            ((TurretSeat)target).Turret.ElevationLimit.ToString("#0.0") + "°";
            tSpeed.text = ((TurretSeat)target).Turret.TraMachineSpeed.ToString("#0.0") + "°/s";
            tvSpeed.text = ((TurretSeat)target).Turret.EleMahineSpeed.ToString("#0.0") + "°/s";
        //}
        reloading.text = (60.0f / ((TurretSeat)target).Turret.FiringRate).ToString("#0.0") + "s";
        ignDEF.text = (((TurretSeat)target).Turret.Penetration * 100.0f).ToString("#0.00") + "%";
        damage.text = ((TurretSeat)target).Turret.Damage.ToString("#0");
        muzVelo.text = ((TurretSeat)target).Turret.RoundSpeed.ToString("#0.0") + " kM/s";
        bool enoughSpace = targetHold.CanAdd(target.WPOnSeat.weaponID);

        if (enoughSpace)
        {
            hint.enabled = false;
            volume.color = Color.white;
            unequip.interactable = true;
        }
        else
        {
            volume.color = Color.red;
            unequip.interactable = false;
            hint.text = "Not enough space to unequip this weapon!";
            hint.enabled = true;
        }
            
    }

    protected void UpdateVLSInfo()
    {
        WeaponItem item = (WeaponItem)ItemList.GetItem(((VLSSeat)target)._VLS.weaponID);

        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        vGuidance.text = "-";
        tSpeed.text = "-";
        tvSpeed.text = "-";
        ignDEF.text = "-";
        damage.text = "-";
        muzVelo.text = "-";
        hint.text = "";
        reloading.text = (((VLSSeat)target)._VLS.ShortLoading).ToString("#0.0") + "s / " +
            (((VLSSeat)target)._VLS.BaseLongLoading).ToString("#0.0") + "s";

        if (((VLSSeat)target)._VLS.UsingMissile != null)
        {
            hint.enabled = true;
            hint.text = "To unequip the VLS, missiles should be uninstalled.";
            unequip.interactable = false;
            return;
        }
        else
        {
            hint.enabled = false;
        }

        bool enoughSpace = targetHold.CanAdd(target.WPOnSeat.weaponID);

        if (enoughSpace)
        {
            hint.enabled = false;
            volume.color = Color.white;
            unequip.interactable = true;
        }
        else
        {
            volume.color = Color.red;
            unequip.interactable = false;
            hint.text = "Not enough space to unequip this weapon!";
            hint.enabled = true;
        }
    }

    protected void UpdateMissileInfo()
    {
        WeaponItem item = (WeaponItem)ItemList.GetItem(((VLSSeat)target)._VLS.UsingMissile.weaponID);

        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        vGuidance.text = "-";
        tSpeed.text = "-";
        tvSpeed.text = "-";

        ignDEF.text = (((VLSSeat)target)._VLS.UsingMissile.Penetration * 100.0f).ToString("#0.00") + "%";
        damage.text = ((VLSSeat)target)._VLS.UsingMissile.Damage.ToString("#0");
        muzVelo.text = ((VLSSeat)target)._VLS.UsingMissile.
            GetComponent<MissileGuidance>().MaxSpeed.ToString("#0.0") + " kM/s";

        reloading.text = "-";

        bool enoughSpace = targetHold.CanAdd(target.WPOnSeat.weaponID);

        if (enoughSpace)
        {
            hint.enabled = false;
            volume.color = Color.white;
            unequip.interactable = true;
        }
        else
        {
            volume.color = Color.red;
            unequip.interactable = false;
            hint.text = "Not enough space to unequip this weapon!";
            hint.enabled = true;
        }
    }

    public void EmptyInfo()
    {
        weaponName.text = "-";
        mass.text = "-";
        volume.text = "-";
        vGuidance.text = "-";
        tSpeed.text = "-";
        tvSpeed.text = "-";
        reloading.text = "-";
        ignDEF.text = "-";
        damage.text = "-";
        muzVelo.text = "-";

        unequip.interactable = false;
    }

    public void Unequip()
    {
        if (currentStatus == WeaponStatus.NaN)
            return;
        //tell firecontrol unequip
        int tempID = target.WPOnSeat.weaponID;
        if (currentStatus == WeaponStatus.Missile)
            tempID = ((VLS)target.WPOnSeat).UsingMissile.weaponID;
        if (targetFC.UnequipWeapon(currentStatus))
        {
            targetHold.Add(tempID, 1);
        }
        //reset
        UpdateWeaponInfo();
        if (dockUI != null) 
            dockUI.UpdateWeaponList();
    }
}
