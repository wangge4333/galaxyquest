﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dock_WpListEle : MonoBehaviour
{
    protected int itemID = 0;
    protected WeaponItem item = null;

    protected int equipedItemID = 0;
    protected WeaponItem equipedItem = null;

    protected WeaponStatus wpStatus = WeaponStatus.NaN;
    protected ShipCargo targetHold = null;
    protected FireControl targetFC = null;
    protected DockUI dockUI = null;

    [SerializeField]
    protected Text weaponName = null;
    [SerializeField]
    protected Image weaponIcon = null;
    [SerializeField]
    protected Button equip = null;

    [SerializeField]
    protected Image bg = null;

    //base info
    [SerializeField]
    protected Text mass = null;
    [SerializeField]
    protected Text volume = null;

    //weapon info
    [SerializeField]
    protected Text vGuidance = null;
    [SerializeField]
    protected Text tSpeed = null;
    [SerializeField]
    protected Text tvSpeed = null;
    [SerializeField]
    protected Text ignDEF = null;
    [SerializeField]
    protected Text reloading = null;
    [SerializeField]
    protected Text damage = null;
    [SerializeField]
    protected Text muzVelo = null;
    [SerializeField]
    protected Text hint = null;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDockUI(DockUI d_ui)
    {
        dockUI = d_ui;
    }

    public void SetBGColour(Color _color)
    {
        bg.color = _color;
    }

    public void SetTarget(int _itemID, int equipedID, WeaponStatus _wpStatus, SpaceObject target)
    {
        TotalReset();
        targetHold = target.GetComponent<ShipCargo>();
        targetFC = target.GetComponent<FireControl>();
        wpStatus = _wpStatus;

        itemID = _itemID;
        equipedItemID = equipedID;
        item = (WeaponItem)ItemList.GetItem(itemID);
        equipedItem = (WeaponItem)ItemList.GetItem(equipedItemID);

        UpdateWeaponInfo();
    }

    public void TotalReset()
    {
        itemID = 0;
        item = null;
        equipedItemID = 0;
        equipedItem = null;

        targetHold = null;
        targetFC = null;
        wpStatus = WeaponStatus.NaN;
    }

    public void UpdateWeaponInfo()
    {

        switch (item.WeaponType)
        {
            case WeaponType.VLS:
                UpdateVLSInfo();
                break;
            case WeaponType.Missile:
                UpdateMissileInfo();
                break;
            default:
                UpdateTurretInfo();
                break;
        }
    }

    protected void UpdateTurretInfo()
    {
        CannonTurret temp = ItemList.GetWeaponPrefab(itemID).GetComponent<CannonTurret>();

        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        //if (item.WeaponType != WeaponType.Missile)
        //{
            vGuidance.text = temp.DepressionLimit.ToString("#0.0") + "° ~ " +
            temp.ElevationLimit.ToString("#0.0") + "°";
            tSpeed.text = temp.TraMachineSpeed.ToString("#0.0") + "°/s";
            tvSpeed.text = temp.EleMahineSpeed.ToString("#0.0") + "°/s";
        //}

        reloading.text = (60.0f / temp.FiringRate).ToString("#0.0") + "s";
        ignDEF.text = (temp.Penetration * 100.0f).ToString("#0.00") + "%";
        damage.text = temp.Damage.ToString("#0");
        muzVelo.text = temp.RoundSpeed.ToString("#0.0") + " kM/s";
        bool enoughSpace = true;
        if (equipedItemID != 0)
            enoughSpace = targetHold.CanAdd(equipedItem.Volume - item.Volume);

        if (enoughSpace)
        {
            hint.enabled = false;
            equip.interactable = true;
        }
        else
        {
            hint.enabled = true;
            equip.interactable = false;
        }
    }

    protected void UpdateVLSInfo()
    {
        VLS temp = ItemList.GetWeaponPrefab(itemID).GetComponent<VLS>();

        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        //if (item.WeaponType != WeaponType.Missile)
        //{
        vGuidance.text = "-";
        tSpeed.text = "-";
        tvSpeed.text = "-";
        //}
        ignDEF.text = "-";
        damage.text = "-";
        muzVelo.text = "-";

        reloading.text = temp.ShortLoading.ToString("#0.0") + "s / " +
            temp.BaseLongLoading.ToString("#0.0") + "s";

        bool enoughSpace = true;
        if (equipedItemID != 0)
            enoughSpace = targetHold.CanAdd(equipedItem.Volume - item.Volume);

        if (enoughSpace)
        {
            hint.enabled = false;
            equip.interactable = true;
        }
        else
        {
            hint.enabled = true;
            equip.interactable = false;
        }
    }

    protected void UpdateMissileInfo()
    {
        Missile temp = ItemList.GetWeaponPrefab(itemID).GetComponent<Missile>();

        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        //if (item.WeaponType != WeaponType.Missile)
        //{
        vGuidance.text = "-";
        tSpeed.text = "-";
        tvSpeed.text = "-";
        //}
        ignDEF.text = (temp.Penetration * 100.0f).ToString("#0.00") + "%";
        damage.text = temp.Damage.ToString("#0");
        muzVelo.text = temp.GetComponent<MissileGuidance>().MaxSpeed.ToString("#0.0") + " kM/s";

        reloading.text = "-";

        bool enoughSpace = true;
        if (equipedItemID != 0)
            enoughSpace = targetHold.CanAdd(equipedItem.Volume - item.Volume);

        if (enoughSpace)
        {
            hint.enabled = false;
            equip.interactable = true;
        }
        else
        {
            hint.enabled = true;
            equip.interactable = false;
        }
    }


    //public void EmptyInfo()
    //{
    //    weaponName.text = "-";
    //    mass.text = "-";
    //    volume.text = "-";
    //    vGuidance.text = "-";
    //    tSpeed.text = "-";
    //    tvSpeed.text = "-";
    //    reloading.text = "-";
    //    ignDEF.text = "-";
    //    damage.text = "-";
    //    muzVelo.text = "-";

    //    unequip.interactable = false;
    //}

    public void Equip()
    {
        if (wpStatus == WeaponStatus.NaN)
            return;
        //tell firecontrol equip
        bool ifUnequiped = false;
        if (!targetFC.InstallWeapon(itemID, wpStatus, out ifUnequiped))
            return;
        targetHold.Discard(itemID, 1);
        if (ifUnequiped || equipedItemID != 0) 
        {
            targetHold.Add(equipedItemID, 1);
        }

        //update whole panel
        if (dockUI != null)
            dockUI.UpdateWeaponList();
    }
}
