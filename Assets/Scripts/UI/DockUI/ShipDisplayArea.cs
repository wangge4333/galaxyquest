﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDisplayArea : MonoBehaviour
{
    protected GameObject displayedShip = null;
    protected DisplayedShip ship = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RemoveDisplayedShip()
    {
        Destroy(displayedShip);
        displayedShip = null;
        ship = null;
    }


    public void SetDisplayedShip(string typeName)
    {
        RemoveDisplayedShip();
        displayedShip = Instantiate((GameObject)Resources.Load(
                GQ_Path.ShipPrefabs + typeName + "/" + typeName + "Wire"), transform);
        ship = displayedShip.GetComponent<DisplayedShip>();
    }

    public void SetHighLight(WeaponStatus wpStatus)
    {
        if (displayedShip == null)
            return;
        switch (wpStatus)
        {
            case WeaponStatus.Main:
                ship.SetHighLightMainWeapons();
                break;
            case WeaponStatus.Secondary:
                ship.SetHightLight2ndWeapons();
                break;
            case WeaponStatus.Flak:
                ship.SetHightLight2ndWeapons();
                break;
            case WeaponStatus.VLS:
                ship.SetHightLightMissiles();
                break;
            case WeaponStatus.NaN:
                break;
            default:
                break;
        }
    }
}
