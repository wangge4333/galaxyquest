﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPosHighLight : MonoBehaviour
{
    protected SpriteRenderer _r = null;
    protected float originScale = 0;
    public float scaleUpFactor = 1.2f;
    protected float upedScale = 0;
    public float emission = 0.5f;
    public static Color normalColor = new Color(0.85f, 0.85f, 0.85f, 0.85f);
    public static Color hlColor = new Color(1, 1, 1, 1);

    protected void Awake()
    {
        _r = GetComponent<SpriteRenderer>();
        originScale = transform.localScale.x;
        upedScale = originScale * scaleUpFactor;
    }

    public void SetHightLight()
    {
        _r.material.SetFloat("_Emission", emission);
        _r.material.SetColor("_Color", hlColor);
        transform.localScale.Set(upedScale, upedScale, upedScale);
    }

    public void UnsetHightlight()
    {
        _r.material.SetFloat("_Emission", 0);
        _r.material.SetColor("_Color", normalColor);
        transform.localScale.Set(originScale, originScale, originScale);
    }
}
