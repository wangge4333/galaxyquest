﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lab_LineCtrl : MonoBehaviour
{
    public Image left = null;
    public Image right = null;
    public Image up = null;
    public Image down = null;

    public void OnlyUP()
    {
        AllDisable();
        up.enabled = true;
    }

    public void OnlyDown()
    {
        AllDisable();
        down.enabled = true;
    }

    public void OnlyLeft()
    {
        AllDisable();
        left.enabled = true;
    }

    public void OnlyRight()
    {
        AllDisable();
        right.enabled = true;
    }

    public void AllDisable()
    {
        left.enabled = false;
        right.enabled = false;
        up.enabled = false;
        down.enabled = false;
        AllUnavailable();
    }

    public void LeftDown()
    {
        left.enabled = true;
        right.enabled = false;
        up.enabled = false;
        down.enabled = true;
    }

    public void LeftUp()
    {
        left.enabled = true;
        right.enabled = false;
        up.enabled = true;
        down.enabled = false;
    }

    public void LeftRight()
    {
        left.enabled = true;
        right.enabled = true;
        up.enabled = false;
        down.enabled = false;
    }

    public void RightUp()
    {
        left.enabled = false;
        right.enabled = true;
        up.enabled = true;
        down.enabled = false;
    }

    public void RightDown()
    {
        left.enabled = false;
        right.enabled = true;
        up.enabled = false;
        down.enabled = true;
    }

    public void UpDown()
    {
        left.enabled = false;
        right.enabled = false;
        up.enabled = true;
        down.enabled = true;
    }

    public void LeftAvailable(bool flag)
    {
        if (flag)
        {
            left.color = DefaultPara.AvailableColour;
            return;
        }
        left.color = DefaultPara.UnavaColour;
    }

    public void RightAvailable(bool flag)
    {
        if (flag)
        {
            right.color = DefaultPara.AvailableColour;
            return;
        }
        right.color = DefaultPara.UnavaColour;
    }

    public void UpAvailable(bool flag)
    {
        if (flag)
        {
            up.color = DefaultPara.AvailableColour;
            return;
        }
        up.color = DefaultPara.UnavaColour;
    }

    public void DownAvailable(bool flag)
    {
        if (flag)
        {
            down.color = DefaultPara.AvailableColour;
            return;
        }
        down.color = DefaultPara.UnavaColour;
    }

    public void AllUnavailable()
    {
        up.color = DefaultPara.UnavaColour;
        down.color = DefaultPara.UnavaColour;
        left.color = DefaultPara.UnavaColour;
        right.color = DefaultPara.UnavaColour;
    }

}
