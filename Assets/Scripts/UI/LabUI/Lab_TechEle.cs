﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//科技方阵中 第一个角标为列 第二个为行

public class Lab_TechEle : MonoBehaviour
{
    public Lab_LineCtrl lines = null;
    protected Lab_TechManager manager = null;

    protected GQ_UpTNode targetTech = null;

    public Text techName = null;
    public Image icon = null;
    public Image locked = null;
    public Image buttonArea = null;
    public Image BG = null;

    protected void Start()
    {
        manager = GetComponentInParent<Lab_TechManager>();
    }

    public void SetTarget(GQ_UpTNode tech)
    {
        if (tech == null)
        {
            AllDisable();
            return;
        }
        AllEnable();
        targetTech = tech;
        CheckStatus();
    }

    protected void CheckStatus()
    {
        if (targetTech.IsUnlocked)
            locked.enabled = false;
        else
            locked.enabled = true;

        techName.text = targetTech.TechName;
    }

    protected void AllEnable()
    {
        techName.enabled = true;
        icon.enabled = true;
        locked.enabled = true;
        buttonArea.enabled = true;
        lines.AllDisable();
    }

    protected void AllDisable()
    {
        techName.enabled = false;
        icon.enabled = false;
        locked.enabled = false;
        buttonArea.enabled = false;
        lines.AllDisable();
    }

    public void ButtonFunc()
    {
        //Intro set target
        
        if (manager == null || targetTech == null)
            return;
        manager.SetIntroTarget(targetTech);

    }


}
