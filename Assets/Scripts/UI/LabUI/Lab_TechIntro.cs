﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lab_TechIntro : MonoBehaviour
{
    protected GQ_UpTNode target = null;

    protected Lab_TechManager techManager = null;

    public Image icon = null;
    public Image locked = null;

    public Text techName = null;
    public Text techDescription = null;
    public Text hint = null;
    public Text cost = null;

    public Button unlock = null;

    protected void Start()
    {
        techManager = GetComponentInParent<Lab_TechManager>();
    }

    public void SetTarget(GQ_UpTNode target)
    {
        if (target == null)
            ClearInfo();
        this.target = target;
        SetInfo();
    }

    public void ClearInfo()
    {
        target = null;
        locked.enabled = true;
        techName.text = "";
        techDescription.text = "";
        hint.enabled = false;
        unlock.interactable = false;
        cost.text = "";
    }

    protected void SetInfo()
    {
        locked.enabled = !target.IsUnlocked;
        techName.text = target.TechName;
        techDescription.text = target.Description;
        cost.text = target.UnlockCost.ToString() + " RP";
        if (target.IsUnlocked)
        {
            hint.enabled = true;
            hint.text = "This tech has been unlocked.";
            unlock.interactable = false;
        }
        else
        {
            if (!target.Parent.IsUnlocked)
            {
                hint.text = "Need unlock the last tech!";
                hint.enabled = true;
                unlock.interactable = false;
            }
            else if (!techManager.IsEnoughRP(target.UnlockCost))
            {
                hint.text = "Not enough research points.";
                hint.enabled = true;
                unlock.interactable = false;
            }
            else
            {
                hint.enabled = false;
                unlock.interactable = true;
            }
            
        }
    }

    public void Click()
    {
        if (target == null)
            return;
        target.Unlock();
        if (techManager != null)
            techManager.Refresh();
        SetInfo();
    }
}
