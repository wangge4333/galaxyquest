﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lab_TechManager : MonoBehaviour
{
    protected UIManager uiManager = null;
    [SerializeField]
    protected SlideInExpand labPanel = null;
    [SerializeField]
    protected Text researchPoint = null;

    protected GQ_UpgradeTree target = null;

    [SerializeField]
    protected GameObject lab_techEle = null;
    [SerializeField]
    protected GridLayoutGroup contentTrans = null;
    [SerializeField]
    protected Lab_TechIntro introArea = null;

    protected List<List<Lab_TechEle>> matrix = new List<List<Lab_TechEle>>();

    public int initRow = 6;
    public int initColumn = 6;

    protected int rowAmount = 0;
    protected int columnAmount = 0;

    //test
    //public GQ_UpgradeTree temptarget = null;
    //protected float timer = 2.0f;
    //protected bool once = false;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    protected void Initialization()
    {
        uiManager = FindObjectOfType<UIManager>();
        ExpandRow();
        ExpandColumn();
    }

    public void SetTargetAndOpen(GQ_UpgradeTree target)
    {
        if (target == null)
            return;
        labPanel.gameObject.SetActive(true);
        this.target = target;
        UpdateInfo();
        introArea.ClearInfo();
    }

    protected void UpdateInfo()
    {
        if (target == null)
            return;
        CheckIfEnough();
        int currentWidth = 0;
        IterateNAssign(target.Root, ref currentWidth);
        researchPoint.text = target.ResearchPoint.ToString();
    }

    public void Refresh()
    {
        ResetInfo();
        UpdateInfo();
    }

    protected void ResetInfo()
    {
        for (int i = 0; i < matrix.Count; ++i)
            for (int j = 0; j < matrix[i].Count; ++j)
                matrix[i][j].SetTarget(null);
    }

    public void ResetAndHide()
    {
        labPanel.StartOutAnimation();
        target = null;
        ResetInfo();
    }

    protected void IterateNAssign(GQ_UpTNode node,ref int currentWidth)
    {
        if (node == null)
            return;
        if (node.Depth != 0)
        {
            //add todo, ignore root
            matrix[currentWidth][node.Depth - 1].SetTarget(node);
            node.posColumn = currentWidth;
            node.posRow = node.Depth - 1;

            if (node.Depth > 1) 
                CheckLine(node, node.Parent);
        }

        if (node.Children.Count == 0)
        {
            ++currentWidth;
            return;
        }
        foreach (GQ_UpTNode nodeTemp in node.Children)
        {
            IterateNAssign(nodeTemp, ref currentWidth);
        }
    }

    protected void CheckLine(GQ_UpTNode current, GQ_UpTNode parent)
    {
        int curRow = current.posRow - 1;
        int curColumn = current.posColumn;

        //self
        matrix[current.posColumn][current.posRow].lines.up.enabled = true;
        matrix[current.posColumn][current.posRow].lines.UpAvailable(parent.IsUnlocked);

        //parent line
        if (curColumn > parent.posColumn) 
        {
            matrix[parent.posColumn][parent.posRow].lines.right.enabled = true;
            matrix[parent.posColumn][parent.posRow].lines.RightAvailable(parent.IsUnlocked);
        }
        else if(curColumn == parent.posColumn)
        {
            matrix[parent.posColumn][parent.posRow].lines.down.enabled = true;
            matrix[parent.posColumn][parent.posRow].lines.DownAvailable(parent.IsUnlocked);
        }

        //updown line
        for (; curRow > parent.posRow; --curRow) 
        {
            matrix[curColumn][curRow].lines.up.enabled = true;
            matrix[curColumn][curRow].lines.down.enabled = true;
            matrix[curColumn][curRow].lines.UpAvailable(parent.IsUnlocked);
            matrix[curColumn][curRow].lines.DownAvailable(parent.IsUnlocked);
        }
        //corner
        if (curColumn > parent.posColumn)
        {
            matrix[curColumn][curRow].lines.down.enabled = true;
            matrix[curColumn][curRow].lines.left.enabled = true;
            matrix[curColumn][curRow].lines.LeftAvailable(parent.IsUnlocked);
            matrix[curColumn][curRow].lines.DownAvailable(parent.IsUnlocked);
            --curColumn;
        }

        //leftright line
        while (curColumn > parent.posColumn)
        {
            matrix[curColumn][curRow].lines.right.enabled = true;
            matrix[curColumn][curRow].lines.left.enabled = true;
            matrix[curColumn][curRow].lines.LeftAvailable(parent.IsUnlocked);
            matrix[curColumn][curRow].lines.RightAvailable(parent.IsUnlocked);
            --curColumn;
        }
    }

    protected void CheckIfEnough()
    {
        while (rowAmount < target.MaxDepth)
        {
            ExpandRow();
        }
        while (columnAmount < target.MaxWidth)
        {
            ExpandColumn();
        }
    }

    protected void ExpandRow()
    {
        rowAmount += initRow;
        contentTrans.constraintCount = rowAmount;
        AddMissed();
        ReassignMatrix();
    }

    protected void ExpandColumn()
    {
        columnAmount += initColumn;
        AddMissed();
        ReassignMatrix();
    }

    protected void ReassignMatrix()
    {
        if (columnAmount == 0)
            return;
        AllClear();
        matrix = new List<List<Lab_TechEle>>();
        for (int i = 0; i < columnAmount; ++i)
            matrix.Add(new List<Lab_TechEle>());

        int counter = 0;
        foreach(Lab_TechEle ele in contentTrans.GetComponentsInChildren<Lab_TechEle>())
        {
            matrix[counter / rowAmount].Add(ele);
            if ((counter % columnAmount) % 2 == 0)
                ele.BG.color = DefaultPara.AltItemUIColor;
            else
                ele.BG.color = DefaultPara.ItemUIColor;
            ++counter;
        }
    }

    protected void AllClear()
    {
        for(int i = 0; i < matrix.Count; ++i)
        {
            matrix[i].Clear();
        }
        matrix.Clear();
    }

    protected void AddMissed()
    {
        int currentColumn = matrix.Count;
        int currentRow = matrix.Count == 0 ? 0 : matrix[0].Count;

        for(int i = 0; i < (rowAmount * columnAmount - currentRow * currentColumn); ++i)
        {
            AddAnEle();
        }
    }

    protected void AddAnEle()
    {
        Lab_TechEle temp = Instantiate(lab_techEle, contentTrans.transform).GetComponent<Lab_TechEle>();
        temp.SetTarget(null);
    }

    public void SetIntroTarget(GQ_UpTNode tech)
    {
        //Debug.Log(introArea);
        if (introArea == null)
            return;
        introArea.SetTarget(tech);
    }

    public bool IsEnoughRP(int rp)
    {
        return target.IsEnoughRP(rp);
    }

}
