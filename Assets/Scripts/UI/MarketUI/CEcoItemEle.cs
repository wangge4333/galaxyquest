﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CEcoItemEle : MonoBehaviour
{
    [SerializeField]
    protected Image bg = null;
    [SerializeField]
    protected Text itemName = null;
    [SerializeField]
    protected Image itemIcon = null;
    [SerializeField]
    protected Text amountInHold = null;
    [SerializeField]
    protected Text priceUI = null;
    [SerializeField]
    protected Text amountSell = null;
    [SerializeField]
    protected Text mass = null;
    [SerializeField]
    protected Text volume = null;

    //purchase
    [SerializeField]
    protected Button add = null;
    [SerializeField]
    protected Button sub = null;
    [SerializeField]
    protected Button add_10 = null;
    [SerializeField]
    protected Button sub_10 = null;
    [SerializeField]
    protected Button sell = null;
    [SerializeField]
    protected Text priceHint = null;

    //total
    [SerializeField]
    protected Text totalPrice = null;
    [SerializeField]
    protected Text totalMass = null;
    [SerializeField]
    protected Text totalVolume = null;

    protected int itemID = 0;
    protected PlanetMarket market = null;
    protected int targetID = 0;
    protected ShipCargo target = null;
    protected EcoItem item = null;
    protected int amount = 0;
    protected int maxAmount = 0;
    protected int inHold = 0;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetBGColour(Color _color)
    {
        bg.color = _color;
    }

    public void SetTarget(int _itemID, PlanetMarket pm, int _targetID)
    {
        itemID = _itemID;
        item = (EcoItem)ItemList.GetItem(itemID);
        market = pm;
        targetID = _targetID;
        target = market.GetTradeTarget(_targetID);
        
        UpdatePerSelling();
    }

    public void ResetAndHide()
    {
        itemID = 0;
        market = null;
        targetID = 0;
        target = null;
        item = null;
        amount = 0;
        maxAmount = 0;
        inHold = 0;
    }

    public void UpdatePerSelling()
    {
        if (!target.EcoItems.ContainsKey(itemID))
            return;

        itemName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.000");
        volume.text = item.Volume.ToString("#0.000");

        amount = 0;
        maxAmount = market.GetBuyableAmount(itemID);
        inHold = target.EcoItems[itemID];
        priceUI.text = market.GetPrice(itemID).ToString();

        amountInHold.text = inHold.ToString() + " IN CARGO HOLD";
        UpdatePerAddSub();
    }

    protected void UpdatePerAddSub()
    {
        amountSell.text = amount.ToString();
        totalPrice.text = (market.GetPrice(itemID) * amount).ToString();
        totalMass.text = (item.Weight * amount).ToString("#0.00");
        totalVolume.text = (item.Volume * amount).ToString("#0.00");

        bool enoughSpace = target.CanAdd(item.Volume * (float)amount);
        bool enoughMoney = target.CanBuy(itemID, amount, market);

        if (amount == maxAmount) 
        {
            priceHint.enabled = true;
            add.interactable = false;
            add_10.interactable = false;
        }
        else
        {
            priceHint.enabled = false;
            add.interactable = true;
            add_10.interactable = true;
        }

        if (amount == inHold)
        {
            add.interactable = false;
            add_10.interactable = false;
        }
        else
        {
            add.interactable = true;
            add_10.interactable = true;
        }

        if (amount == 0)
        {
            sub.interactable = false;
            sub_10.interactable = false;
            sell.interactable = false;
        }
        else
        {
            sub.interactable = true;
            sub_10.interactable = true;
            sell.interactable = true;
        }
    }

    public void AddAmount()
    {
        ++amount;
        amount = Mathf.Min(maxAmount, amount);
        amount = Mathf.Min(amount, inHold);
        UpdatePerAddSub();
    }

    public void Add10()
    {
        amount = Mathf.Min(maxAmount, amount + 10);
        amount = Mathf.Min(amount, inHold);
        UpdatePerAddSub();
    }

    public void SubAmount()
    {
        --amount;
        amount = Mathf.Max(0, amount);
        UpdatePerAddSub();
    }

    public void Sub10()
    {
        amount = Mathf.Max(0, amount - 10);
        UpdatePerAddSub();
    }

    public void Sell()
    {
        market.Buy(itemID, amount, targetID);
        UpdatePerSelling();
    }

}
