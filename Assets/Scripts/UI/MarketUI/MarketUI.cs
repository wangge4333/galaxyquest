﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketUI : MonoBehaviour
{
    protected float updateTime = 0;

    protected UIManager uiManager = null;

    [SerializeField]
    protected GameObject tradePanel = null;

    [SerializeField]
    protected PlanetMarket market = null;
    [SerializeField]
    protected int targetID = 0;
    [SerializeField]
    protected ShipCargo target = null;

    //planet market
    [SerializeField]
    protected Button mCargoButton = null;
    [SerializeField]
    protected Button mWeaponButton = null;

    [SerializeField]
    protected Text marketName = null;

    [SerializeField]
    protected int iniEcoItemCapacity = 20;
    [SerializeField]
    protected List<EcoItemEle> marketEcoItems = new List<EcoItemEle>();
    [SerializeField]
    protected GameObject ecoItemEle = null;
    [SerializeField]
    protected Transform ecoIndex = null;
    protected int ecoActiveNum = 0;
    [SerializeField]
    protected GameObject cargoPanel = null;

    //coming soon..! now
    [SerializeField]
    protected int iniWeaponCapacity = 10;
    [SerializeField]
    protected List<WeaponEle> marketWeapons = new List<WeaponEle>();
    [SerializeField]
    protected GameObject weaponEle = null;
    [SerializeField]
    protected Transform weaponIndex = null;
    protected int wpActiveNum = 0;
    [SerializeField]
    protected GameObject weaponPanel = null;
    //planet market over

    //cargo hold
    [SerializeField]
    protected Button hCargoButton = null;
    [SerializeField]
    protected Button hWeaponButton = null;

    [SerializeField]
    protected Text money = null;
    [SerializeField]
    protected Text space = null;

    [SerializeField]
    protected List<CEcoItemEle> holdEcoItems = new List<CEcoItemEle>();
    [SerializeField]
    protected GameObject holdEcoItemEle = null;
    [SerializeField]
    protected Transform holdEcoIndex = null;
    protected int holdEcoActiveNum = 0;
    [SerializeField]
    protected GameObject holdEcoPanel = null;

    //weapons
    [SerializeField]
    protected List<WeaponEle> holdWeapons = new List<WeaponEle>();
    [SerializeField]
    protected GameObject holdWeaponEle = null;
    [SerializeField]
    protected Transform holdWeaponIndex = null;
    protected int holdWpActiveNum = 0;
    [SerializeField]
    protected GameObject holdWeaponPanel = null;

    //cargo hold over


    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        if (market == null || targetID == 0 || !tradePanel.activeSelf)
            return;

        AutoUpdateAllInfo();
    }

    protected void Initialization()
    {
        uiManager = FindObjectOfType<UIManager>();
        ExpandMarketEcoCapacity();
        ExpandHoldEcoCapacity();

    }

    //set the target market and spaceship, open UI
    public void SetTargetAndOpen(PlanetMarket _market,int _targetID)
    {
        tradePanel.SetActive(true);

        market = _market;
        targetID = _targetID;
        target = market.GetTradeTarget(targetID);
        updateTime = market.UpdateTime;

        SetEachEcoElement();
        SetEachHoldEcoElement();
        SetEachWeaponElement();
        SetEachHoldWeaponElement();

        space.text = target.UsedSpace.ToString("#0.0") + " / " + target.FinalMaxSpace.ToString("#0.0");
        money.text = target.Money.ToString();
        marketName.text = market.gameObject.name + " Market";

        ShowCargo();
    }

    //Generate the list of economy items in market
    protected void SetEachEcoElement()
    {
        if (market == null)
            return;
        if (market.StorageList.Count == 0)
            return;

        while(market.StorageList.Count> holdEcoItems.Count)
        {
            ExpandMarketEcoCapacity();
        }
        int i = 0;
        foreach(KeyValuePair<int,int> ei in market.StorageList)
        {
            if (ei.Value <= 0)
                continue;
            marketEcoItems[i].gameObject.SetActive(true);
            marketEcoItems[i].SetTarget(ei.Key, market, targetID);
            ++i;
        }
        ecoActiveNum = i;
    }

    //Generate the list of economy items in player's cargo hold
    protected void SetEachHoldEcoElement()
    {
        ShipCargo temp = market.GetTradeTarget(targetID);

        if (temp == null)
            return;
        if (temp.EcoItems.Count == 0)
            return;

        while (temp.EcoItems.Count > holdEcoItems.Count)
        {
            ExpandHoldEcoCapacity();
        }
        int i = 0;
        foreach (KeyValuePair<int, int> ei in temp.EcoItems)
        {
            if (ei.Value <= 0)
                continue;
            holdEcoItems[i].gameObject.SetActive(true);
            holdEcoItems[i].SetTarget(ei.Key, market, targetID);
            ++i;
        }
        holdEcoActiveNum = i;
    }

    //Generate the list of weapons in market
    protected void SetEachWeaponElement()
    {
        if (market == null)
            return;
        if (market.WeaponList.Count == 0)
            return;

        while (market.WeaponList.Count > marketWeapons.Count)
        {
            ExpandMarketWeaponCapacity();
        }
        int i = 0;
        foreach (int id in market.WeaponList)
        {
            marketWeapons[i].gameObject.SetActive(true);
            marketWeapons[i].SetTarget(id, market, targetID);
            ++i;
        }
        wpActiveNum = i;
    }

    //Generate the list of weapons in player's hold
    protected void SetEachHoldWeaponElement()
    {
        ShipCargo temp = market.GetTradeTarget(targetID);

        if (temp == null)
            return;
        if (temp.WeaponItems.Count == 0)
            return;

        while (temp.WeaponItems.Count > holdWeapons.Count)
        {
            ExpandHoldWeaponCapacity();
        }
        int i = 0;
        foreach (int id in temp.WeaponItems)
        {
            holdWeapons[i].gameObject.SetActive(true);
            holdWeapons[i].SetTarget(id, targetID, market);
            ++i;
        }
        holdWpActiveNum = i;
    }

    //reset and hide all list
    public void ResetAndHide()
    {
        for(int i=0;i< ecoActiveNum; ++i)
        {
            marketEcoItems[i].ResetAndHide();
            marketEcoItems[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < holdEcoActiveNum; ++i)
        {
            holdEcoItems[i].ResetAndHide();
            holdEcoItems[i].gameObject.SetActive(false);
        }

        market = null;
        targetID = 0;
        target = null;
        updateTime = 0;

        ecoActiveNum = 0;
        holdEcoActiveNum = 0;
        marketName.text = "";

        tradePanel.GetComponent<SlideInExpand>().StartOutAnimation();
    }

    public void Cancel()
    {
        uiManager.CloseMarketUI();
    }

    //initialise info
    protected void ResetInfo()
    {
        for (int i = 0; i < ecoActiveNum; ++i)
        {
            marketEcoItems[i].ResetAndHide();
            marketEcoItems[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < holdEcoActiveNum; ++i)
        {
            holdEcoItems[i].ResetAndHide();
            holdEcoItems[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < wpActiveNum; ++i)
        {
            marketWeapons[i].ResetAndHide();
            marketWeapons[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < holdWpActiveNum; ++i)
        {
            holdWeapons[i].ResetAndHide();
            holdWeapons[i].gameObject.SetActive(false);
        }

        ecoActiveNum = 0;
        holdEcoActiveNum = 0;
        wpActiveNum = 0;
        holdWpActiveNum = 0;
        marketName.text = "";
    }

    protected void AutoUpdateAllInfo()
    {
        if (market.UpdateTime == updateTime)
            return;
        ResetInfo();
        SetEachEcoElement();
        SetEachHoldEcoElement();
        SetEachWeaponElement();
        SetEachHoldWeaponElement();
        updateTime = market.UpdateTime;

        space.text = target.UsedSpace.ToString("#0.0") + " / " + target.FinalMaxSpace.ToString("#0.0");
        money.text = target.Money.ToString();
    }

    //when pool is not enough
    protected void ExpandMarketEcoCapacity()
    {
        for (int i = 0; i < iniEcoItemCapacity; ++i)
        {
            EcoItemEle eie = Instantiate(ecoItemEle, ecoIndex).GetComponent<EcoItemEle>();
            marketEcoItems.Add(eie);
            eie.SetBGColour((i % 2 == 0) ? DefaultPara.ItemUIColor : DefaultPara.AltItemUIColor);
            eie.gameObject.SetActive(false);
        }
    }

    //when pool is not enough
    protected void ExpandHoldEcoCapacity()
    {
        for (int i = 0; i < iniEcoItemCapacity; ++i)
        {
            CEcoItemEle ceie = Instantiate(holdEcoItemEle, holdEcoIndex).GetComponent<CEcoItemEle>();
            holdEcoItems.Add(ceie);
            ceie.SetBGColour((i % 2 == 0) ? DefaultPara.ItemUIColor : DefaultPara.AltItemUIColor);
            ceie.gameObject.SetActive(false);
        }
    }

    //when pool is not enough
    protected void ExpandMarketWeaponCapacity()
    {
        for (int i = 0; i < iniWeaponCapacity; ++i)
        {
            WeaponEle we = Instantiate(weaponEle, weaponIndex).GetComponent<WeaponEle>();
            marketWeapons.Add(we);
            we.SetBGColour((i % 2 == 0) ? DefaultPara.ItemUIColor : DefaultPara.AltItemUIColor);
            we.gameObject.SetActive(false);
        }
    }

    //when pool is not enough
    protected void ExpandHoldWeaponCapacity()
    {
        for (int i = 0; i < iniWeaponCapacity; ++i)
        {
            WeaponEle we = Instantiate(holdWeaponEle, holdWeaponIndex).GetComponent<WeaponEle>();
            holdWeapons.Add(we);
            we.SetBGColour((i % 2 == 0) ? DefaultPara.ItemUIColor : DefaultPara.AltItemUIColor);
            we.gameObject.SetActive(false);
        }
    }

    public void ShowCargo()
    {
        cargoPanel.SetActive(true);
        holdEcoPanel.SetActive(true);
        weaponPanel.SetActive(false);
        holdWeaponPanel.SetActive(false);
        mCargoButton.Select();
        hCargoButton.Select();
    }

    public void ShowWeapons()
    {
        cargoPanel.SetActive(false);
        holdEcoPanel.SetActive(false);
        weaponPanel.SetActive(true);
        holdWeaponPanel.SetActive(true);
        mWeaponButton.Select();
        hWeaponButton.Select();
    }



}
