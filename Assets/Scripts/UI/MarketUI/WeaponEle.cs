﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponEle : MonoBehaviour
{
    [SerializeField]
    protected Text weaponName = null;
    [SerializeField]
    protected Image weaponIcon = null;
    [SerializeField]
    protected Text price = null;
    [SerializeField]
    protected Button purchase = null;
    [SerializeField]
    protected Image bg = null;

    //base info
    [SerializeField]
    protected Text mass = null;
    [SerializeField]
    protected Text volume = null;

    //weapon info
    [SerializeField]
    protected Text vGuidance = null;
    [SerializeField]
    protected Text tSpeed = null;
    [SerializeField]
    protected Text tvSpeed = null;
    [SerializeField]
    protected Text ignDEF = null;
    [SerializeField]
    protected Text reloading = null;
    [SerializeField]
    protected Text damage = null;
    [SerializeField]
    protected Text muzVelo = null;
    [SerializeField]
    protected Text hint = null;

    protected int itemID = 0;
    protected PlanetMarket market = null;
    protected int targetID = 0;
    protected ShipCargo target = null;
    protected WeaponItem item = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetBGColour(Color _color)
    {
        bg.color = _color;
    }

    /// <summary>
    /// for market panel
    /// </summary>
    /// <param name="_itemID"></param>
    /// <param name="pm"></param>
    /// <param name="_targetID"></param>
    public void SetTarget(int _itemID, PlanetMarket pm, int _targetID)
    {
        itemID = _itemID;
        item = (WeaponItem)ItemList.GetItem(itemID);
        market = pm;
        targetID = _targetID;
        target = market.GetTradeTarget(_targetID);
        UpdatePerPurchasing();
    }

    /// <summary>
    /// for player panel
    /// </summary>
    /// <param name="_itemID"></param>
    /// <param name="_targetID"></param>
    /// <param name="pm"></param>
    public void SetTarget(int _itemID, int _targetID, PlanetMarket pm)
    {
        itemID = _itemID;
        item = (WeaponItem)ItemList.GetItem(itemID);
        market = pm;
        targetID = _targetID;
        target = market.GetTradeTarget(_targetID);
        UpdatePerSelling();
    }

    public void ResetAndHide()
    {
        itemID = 0;
        market = null;
        targetID = 0;
        target = null;
        item = null;
    }

    public void UpdatePerPurchasing()
    {
        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        price.text = market.GetPrice(itemID).ToString();

        UpdateWeaponInfo();
    }

    public void UpdatePerSelling()
    {
        weaponName.text = item.ItemName;
        mass.text = item.Weight.ToString("#0.0");
        volume.text = item.Volume.ToString("#0.0");

        price.text = market.GetPrice(itemID).ToString();

        UpdateCWeaponInfo();
    }

    /// <summary>
    /// for market panel
    /// </summary>
    public void UpdateWeaponInfo()
    {
        switch (((WeaponItem)ItemList.GetItem(itemID)).WeaponType)
        {
            case WeaponType.VLS:
                {
                    VLS temp = ItemList.GetWeaponPrefab(itemID).GetComponent<VLS>();
                    vGuidance.text = "-";
                    tSpeed.text = "-";
                    tvSpeed.text = "-";
                    reloading.text = temp.ShortLoading.ToString("#0.0") + "s / " +
                        temp.BaseLongLoading.ToString("#0.0") + "s";
                    ignDEF.text = "-";
                    damage.text = "-";
                    muzVelo.text = "-";
                    hint.text = "";

                    break;
                }
                
            case WeaponType.Missile:
                {
                    Missile temp = ItemList.GetWeaponPrefab(itemID).GetComponent<Missile>();
                    vGuidance.text = "-";
                    tSpeed.text = "-";
                    tvSpeed.text = "-";
                    reloading.text = "-";
                    ignDEF.text = (temp.Penetration * 100.0f).ToString("#0.00") + "%";
                    damage.text = temp.Damage.ToString("#0");
                    muzVelo.text = temp.GetComponent<MissileGuidance>().MaxSpeed.ToString("#0.0") + " kM/s";
                    hint.text = "To launch missile, VLS is required.";
                    break;
                }
            default:
                {
                    CannonTurret temp = ItemList.GetWeaponPrefab(itemID).GetComponent<CannonTurret>();

                    vGuidance.text = temp.DepressionLimit.ToString("#0.0") + "° ~ " +
                        temp.ElevationLimit.ToString("#0.0") + "°";
                    tSpeed.text = temp.TraMachineSpeed.ToString("#0.0") + "°/s";
                    tvSpeed.text = temp.EleMahineSpeed.ToString("#0.0") + "°/s";
                    reloading.text = (60.0f / temp.FiringRate).ToString("#0.0") + "s";
                    ignDEF.text = (temp.Penetration * 100.0f).ToString("#0.00") + "%";
                    damage.text = temp.Damage.ToString("#0");
                    muzVelo.text = temp.RoundSpeed.ToString("#0.0") + " kM/s";
                    hint.text = "! For ";

                    switch (temp.WpType)
                    {
                        case WeaponType.BigCalibre:
                            hint.text += "Big Calibre ";
                            break;
                        case WeaponType.BMCalibre:
                            hint.text += "Medium Big Calibre ";
                            break;
                        case WeaponType.MediumCalibre:
                            hint.text += "Medium Calibre ";
                            break;
                        case WeaponType.SmallCalibre:
                            hint.text += "Small Calibre ";
                            break;
                        case WeaponType.Flak:
                            hint.text += "Flak ";
                            break;
                        case WeaponType.Missile:
                            hint.text += "Missile ";
                            break;
                        case WeaponType.VLS:
                            break;
                        default:
                            break;
                    }

                    hint.text += "Seat !\n! Check the Type of Your Ship's Turret Seat !";
                    break;
                }  
        }
        

        bool enoughSpace = target.CanAdd(item.Volume);
        bool enoughMoney = target.CanBuy(itemID, 1, market);

        if (!enoughSpace)
            volume.color = Color.red;
        else
            volume.color = Color.white;

        if (!enoughMoney)
            price.color = Color.red;
        else
            price.color = Color.white;

        if (enoughMoney && enoughSpace)
            purchase.interactable = true;
        else
            purchase.interactable = false;
    }

    /// <summary>
    /// for player panel
    /// </summary>
    public void UpdateCWeaponInfo()
    {
        switch (((WeaponItem)ItemList.GetItem(itemID)).WeaponType)
        {
            case WeaponType.VLS:
                {
                    VLS temp = ItemList.GetWeaponPrefab(itemID).GetComponent<VLS>();
                    vGuidance.text = "-";
                    tSpeed.text = "-";
                    tvSpeed.text = "-";
                    reloading.text = temp.ShortLoading.ToString("#0.0") + "s / " +
                        temp.BaseLongLoading.ToString("#0.0") + "s";
                    ignDEF.text = "-";
                    damage.text = "-";
                    muzVelo.text = "-";
                    hint.text = "";

                    break;
                }

            case WeaponType.Missile:
                {
                    Missile temp = ItemList.GetWeaponPrefab(itemID).GetComponent<Missile>();
                    vGuidance.text = "-";
                    tSpeed.text = "-";
                    tvSpeed.text = "-";
                    reloading.text = "-";
                    ignDEF.text = (temp.Penetration * 100.0f).ToString("#0.00") + "%";
                    damage.text = temp.Damage.ToString("#0");
                    muzVelo.text = temp.GetComponent<MissileGuidance>().MaxSpeed.ToString("#0.0") + " kM/s";
                    hint.text = "To launch missile, VLS is required.";
                    break;
                }
            default:
                {
                    CannonTurret temp = ItemList.GetWeaponPrefab(itemID).GetComponent<CannonTurret>();

                    vGuidance.text = temp.DepressionLimit.ToString("#0.0") + "° ~ " +
                        temp.ElevationLimit.ToString("#0.0") + "°";
                    tSpeed.text = temp.TraMachineSpeed.ToString("#0.0") + "°/s";
                    tvSpeed.text = temp.EleMahineSpeed.ToString("#0.0") + "°/s";
                    reloading.text = (60.0f / temp.FiringRate).ToString("#0.0") + "s";
                    ignDEF.text = (temp.Penetration * 100.0f).ToString("#0.00") + "%";
                    damage.text = temp.Damage.ToString("#0");
                    muzVelo.text = temp.RoundSpeed.ToString("#0.0") + " kM/s";
                    hint.text = "! For ";

                    switch (temp.WpType)
                    {
                        case WeaponType.BigCalibre:
                            hint.text += "Big Calibre ";
                            break;
                        case WeaponType.BMCalibre:
                            hint.text += "Medium Big Calibre ";
                            break;
                        case WeaponType.MediumCalibre:
                            hint.text += "Medium Calibre ";
                            break;
                        case WeaponType.SmallCalibre:
                            hint.text += "Small Calibre ";
                            break;
                        case WeaponType.Flak:
                            hint.text += "Flak ";
                            break;
                        case WeaponType.Missile:
                            hint.text += "Missile ";
                            break;
                        case WeaponType.VLS:
                            break;
                        default:
                            break;
                    }

                    hint.text += "Seat !\n! Check the Type of Your Ship's Turret Seat !";
                    break;
                }
        }
    }

    /// <summary>
    /// means player purchase
    /// </summary>
    public void Purchase()
    {
        market.Sell(itemID, 1, targetID);
        UpdatePerPurchasing();
    }

    /// <summary>
    /// means player sell
    /// </summary>
    public void Sell()
    {
        market.Buy(itemID, 1, targetID);
        UpdatePerSelling();
    }
}
