﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockOnBox : MonoBehaviour
{
    [SerializeField]
    protected FireControl fireControl = null;
    protected bool locked = false;


    [SerializeField]
    protected Image preBox;
    [SerializeField]
    protected Image lockBox;
    [SerializeField]
    protected Image lockLine;

    // Start is called before the first frame update
    void Start()
    {
        preBox.enabled = false;
        lockBox.enabled = false;
        lockLine.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void FixedUpdate()
    {
        UpdateInfo();
    }

    public void SetTarget(FireControl fc)
    {
        fireControl = fc;
        Initialization();
    }

    protected void Initialization()
    {

    }

    protected void UpdateInfo()
    {
        if (fireControl == null)
            return;
        if (fireControl.LockedObject == null) 
        {
            if (locked)
            {
                locked = false;
                SetAllInvisible();
            }
            return;
        }
        if (!locked)
        {
            SetAllVisible();
            locked = true;
        }
        if (preBox.enabled &&
            Algo.IsOppoDirection(Camera.main.transform.forward,
            fireControl.LockedObject.transform.position - Camera.main.transform.position))
        {
            SetAllInvisible();
            return;
        }
        if (!preBox.enabled &&
            !Algo.IsOppoDirection(Camera.main.transform.forward,
            fireControl.LockedObject.transform.position - Camera.main.transform.position)) 
        {
            SetAllVisible();
        }

        //update info
        UpdatePos();

    }

    protected void UpdatePos()
    {

        Vector3 targetTemp = Camera.main.WorldToScreenPoint(fireControl.LockedObject.transform.position);
        Vector3 preTemp = Camera.main.WorldToScreenPoint(fireControl.PredictedPos);

        targetTemp.z = 0;
        preTemp.z = 0;
        lockBox.rectTransform.position = targetTemp;
        preBox.rectTransform.position = preTemp;

        lockLine.rectTransform.position = (preTemp + targetTemp) / 2.0f;
        lockLine.rectTransform.sizeDelta = new Vector2(
            (lockBox.rectTransform.anchoredPosition - preBox.rectTransform.anchoredPosition).magnitude, 1);

        Vector3 r = new Vector3(0, 0, 0);

        if (targetTemp.x == preTemp.x)
        {
            r.z = 90.0f;
        }
        else
        {
            r.z = Mathf.Rad2Deg * Mathf.Atan2(targetTemp.y - preTemp.y, targetTemp.x - preTemp.x);
        }
        lockLine.rectTransform.rotation = Quaternion.Euler(r);

    }

    protected void SetAllInvisible()
    {
        preBox.enabled = false;
        lockBox.enabled = false;
        lockLine.enabled = false;
    }

    protected void SetAllVisible()
    {
        preBox.enabled = true;
        lockBox.enabled = true;
        lockLine.enabled = true;
    }
}
