﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCInfoUI : MonoBehaviour
{
    [SerializeField,Range(10,50)]
    protected int maxNum = 20;

    protected Dictionary<SpaceObject, NPCSimpleInfoEle> activeEles;
    protected Queue<NPCSimpleInfoEle> sleepQueue;

    [SerializeField]
    protected GameObject simpleInfoElePrefab = null;

    // Start is called before the first frame update
    void Start()
    {
        Iniitialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Iniitialization()
    {
        activeEles = new Dictionary<SpaceObject, NPCSimpleInfoEle>();
        sleepQueue = new Queue<NPCSimpleInfoEle>();

        for(int i = 0; i < maxNum; ++i)
        {
            NPCSimpleInfoEle temp =
                Instantiate(simpleInfoElePrefab, transform).GetComponent<NPCSimpleInfoEle>();
            temp.gameObject.SetActive(false);
            sleepQueue.Enqueue(temp);
        }
    }

    public void RegisterNPC(SpaceObject so)
    {
        if (so == null)
            return;
        if (sleepQueue.Count == 0)
            return;
        if (activeEles.ContainsKey(so))
            return;
        NPCSimpleInfoEle temp = sleepQueue.Dequeue();
        temp.gameObject.SetActive(true);
        temp.SetTarget(so);
        activeEles.Add(so, temp);
    }

    public void DeregisterNPC(SpaceObject so)
    {
        if (so == null)
            return;
        if (!activeEles.ContainsKey(so))
            return;
        NPCSimpleInfoEle temp = activeEles[so];
        activeEles.Remove(so);
        temp.gameObject.SetActive(false);
        sleepQueue.Enqueue(temp);
    }

    public void Lock(SpaceObject so)
    {
        if (!activeEles.ContainsKey(so))
            RegisterNPC(so);
        activeEles[so].Lock();

    }

    public void Unlock(SpaceObject so)
    {
        if (activeEles.ContainsKey(so))
            activeEles[so].Unlock();
    }

}
