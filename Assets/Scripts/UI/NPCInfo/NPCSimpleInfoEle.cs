﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCSimpleInfoEle : MonoBehaviour
{
    [SerializeField]
    protected SpaceObject target = null;

    protected GameManager gameManager = null;
    protected RectTransform rectTran = null;

    [SerializeField]
    protected Image back = null;
    [SerializeField]
    protected Image delayBar = null;
    [SerializeField]
    protected Image bar = null;
    [SerializeField]
    protected Text nameOfNPC = null;
    [SerializeField]
    protected Text shipType = null;
    [SerializeField]
    protected Text hpInfo = null;
    [SerializeField]
    protected Text distance = null;
    [SerializeField]
    protected Text damage = null;

    [SerializeField, Range(0, 80)]
    protected float yOffset = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    protected void FixedUpdate()
    {
        UpdateInfo();
    }

    protected void Initialization()
    {
        gameManager = FindObjectOfType<GameManager>();
        rectTran = GetComponent<RectTransform>();
        damage.color = DefaultPara.ReadyColor;
    }

    public void SetTarget(SpaceObject so)
    {
        target = so;
        nameOfNPC.text = target.Clan + " ' " + target.Name;
        shipType.text = target.ShipType;
    }

    protected void SetAllInvisible()
    {
        back.enabled = false;
        delayBar.enabled = false;
        bar.enabled = false;
        nameOfNPC.enabled = false;
        shipType.enabled = false;
        hpInfo.enabled = false;
        distance.enabled = false;
    }

    protected void SetAllVisible()
    {
        back.enabled = true;
        delayBar.enabled = true;
        bar.enabled = true;
        nameOfNPC.enabled = true;
        shipType.enabled = true;
        hpInfo.enabled = true;
        distance.enabled = true;
    }

    public void Lock()
    {
        nameOfNPC.fontStyle = FontStyle.Bold;
        shipType.fontStyle = FontStyle.Bold;
        distance.fontStyle = FontStyle.Bold;
    }

    public void Unlock()
    {
        nameOfNPC.fontStyle = FontStyle.Normal;
        shipType.fontStyle = FontStyle.Normal;
        distance.fontStyle = FontStyle.Normal;
    }

    protected void UpdateInfo()
    {
        if (target == null)
            return;

        if (back.enabled &&
            Algo.IsOppoDirection(gameManager.MainCamera.transform.forward,
            target.transform.position - gameManager.MainCamera.transform.position))
        {
            SetAllInvisible();
            return;
        }
        if(!back.enabled &&
            !Algo.IsOppoDirection(gameManager.MainCamera.transform.forward,
            target.transform.position - gameManager.MainCamera.transform.position))
        {
            SetAllVisible();
        }

        //pos
        Vector2 temp = RectTransformUtility.WorldToScreenPoint(Camera.main, target.transform.position);
        temp.y += yOffset;

        rectTran.position = temp;

        delayBar.fillAmount = Mathf.Lerp(delayBar.fillAmount, target.GetDelayHPPercent(), Time.deltaTime);
        bar.fillAmount = target.GetHPPercent();
        hpInfo.text = target.CurrentHP.ToString("#0") + "/" + target.FinalMaxHP.ToString("#0");
        distance.text = (Vector3.Distance(gameManager.MainPlayer.transform.position,
            target.transform.position) * DefaultPara.ToKM).ToString("#0.0") + " kM";

        if (target.DamageInAWhile == 0)
            damage.text = "";
        else
            damage.text = "-" + target.DamageInAWhile.ToString("#0");
    }
}
