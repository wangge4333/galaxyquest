﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetMenuUI : MonoBehaviour
{
    protected UIManager uiManager = null;
    protected GameManager gameManager = null;

    protected GameObject planet = null;
    protected int playerID = 0;

    
    [SerializeField]
    protected GameObject menuPanel = null;
    [SerializeField]
    protected Button toMakret = null;
    [SerializeField]
    protected Button toDock = null;
    [SerializeField]
    protected Button toLab = null;
    [SerializeField]
    protected Button toIntro = null;

    [SerializeField]
    protected Text planetName = null;


    // Start is called before the first frame update
    void Start()
    {
        uiManager = FindObjectOfType<UIManager>();
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void FreezeControl()
    {
        

    }

    public void SetTarget(GameObject _planet,int _playerID)
    {
        planet = _planet;
        playerID = _playerID;
        planetName.text = _planet.name;
        menuPanel.SetActive(true);
        CheckAvailable();
    }

    protected void CheckAvailable()
    {
        if (!planet.GetComponent<PlanetMarket>())
            toMakret.interactable = false;
        else
            toMakret.interactable = true;
    }

    public void ResetAndHide()
    {
        if (planet.GetComponent<PlanetMarket>())
        {
            planet.GetComponent<PlanetMarket>().GetTradeTarget(playerID)
                .GetComponent<Interact>().EndFollowPlanet();
        }

        planet = null;
        playerID = 0;
        //menuPanel.SetActive(false);
        menuPanel.GetComponent<SlideInExpand>().StartOutAnimation();
        
    }

    public void Cancel()
    {
        uiManager.ClosePlanetMenu();
    }

    public void OpenMarket()
    {
        if (uiManager == null)
            return;
        uiManager.OpenMarketUI(planet.GetComponent<PlanetMarket>(), playerID);
    }

    public void OpenDock()
    {
        if (uiManager == null)
            return;
        uiManager.OpenDockUI(gameManager.SpaceObjectList[playerID]);
    }

    public void OpenLab()
    {
        if (uiManager == null)
            return;
        if(gameManager.SpaceObjectList[playerID].GetComponent<GQ_UpgradeTree>())
            uiManager.OpenLabUI(gameManager.SpaceObjectList[playerID].GetComponent<GQ_UpgradeTree>());
    }

    public void OpenIntro()
    {
        if (uiManager == null)
            return;
        if(planet.GetComponent<Star_SciInfo>())
            uiManager.OpenIntroUI(planet.GetComponent<Star_SciInfo>());
    }
}
