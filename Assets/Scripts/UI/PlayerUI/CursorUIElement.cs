﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorUIElement : MonoBehaviour
{
    [SerializeField]
    protected SpaceshipControl _control = null;
    protected RectTransform rectTran = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    protected void LateUpdate()
    {
        //UpdatePos();
    }

    public void SetTarget(SpaceshipControl ssc)
    {
        _control = ssc;
        rectTran = GetComponent<RectTransform>();
    }

    protected void UpdatePos()
    {
        if (_control == null)
            return;
        Vector3 temp = Camera.main.WorldToScreenPoint(_control.MouseDes);
        temp.z = 0;
        rectTran.position = temp;
    }
}
