﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPermUI : MonoBehaviour
{
    protected SpaceObject target = null;
    protected Rigidbody targetRigidbody = null;
    protected SpaceshipControl targetControl = null;

    [SerializeField]
    protected Text playerName = null;
    [SerializeField]
    protected Image hpBar = null;
    [SerializeField]
    protected Image delayBar = null;
    [SerializeField]
    protected Text hpValue = null;

    [SerializeField]
    protected Text forward = null;
    [SerializeField]
    protected Text back = null;
    [SerializeField]
    protected Text left = null;
    [SerializeField]
    protected Text right = null;
    [SerializeField]
    protected Text up = null;
    [SerializeField]
    protected Text down = null;

    [SerializeField]
    protected Text mainThrottle = null;
    [SerializeField]
    protected Text controlMode = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateInfo();
    }

    public void SetTarget(SpaceObject so)
    {
        target = so;
        targetRigidbody = target.GetComponent<Rigidbody>();
        targetControl = target.GetComponent<SpaceshipControl>();
        UpdateName();
    }

    public void UpdateName()
    {
        playerName.text = '[' + target.Clan + ']' + target.Name;
    }

    protected void UpdateInfo()
    {
        hpBar.fillAmount = target.GetHPPercent();
        delayBar.fillAmount = Mathf.Lerp(delayBar.fillAmount, target.GetDelayHPPercent(), Time.deltaTime);
        hpValue.text = target.CurrentHP.ToString("#0") + " / " + target.FinalMaxHP.ToString("#0");

        Vector3 temp = targetRigidbody.velocity;
        temp = target.transform.worldToLocalMatrix * temp * target.transform.lossyScale.x;

        forward.text = Mathf.Max(0.0f, temp.z * DefaultPara.ToKM).ToString("#0.0") + " kM/s";
        back.text = Mathf.Max(0.0f, -temp.z * DefaultPara.ToKM).ToString("#0.0") + " kM/s";
        right.text = Mathf.Max(0.0f, temp.x * DefaultPara.ToKM).ToString("#0.0") + " kM/s";
        left.text = Mathf.Max(0.0f, -temp.x * DefaultPara.ToKM).ToString("#0.0") + " kM/s";
        up.text = Mathf.Max(0.0f, temp.y * DefaultPara.ToKM).ToString("#0.0") + " kM/s";
        down.text = Mathf.Max(0.0f, -temp.y * DefaultPara.ToKM).ToString("#0.0") + " kM/s";

        mainThrottle.text = (100.0f * targetControl.MainThrottle).ToString("#0") + "%";
        controlMode.text = targetControl.CurrentControlMode.ToString();
    }
}
