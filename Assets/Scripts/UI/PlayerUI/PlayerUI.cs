﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    //moveable UI
    [SerializeField]
    protected GameObject cursorElement = null;
    [SerializeField]
    protected GameObject sightElement = null;

    protected GameObject _cursor = null;
    
    protected List<TurretTargetEle> sights = null;

    [SerializeField]
    protected Canvas cursorCanvas = null;
    [SerializeField]
    protected Canvas sightCanvas = null;
    [SerializeField]
    protected Canvas scopeCanvas = null;

    //permanent UI
    //Turrets status
    [SerializeField]
    protected List<TurretStateEle> status;
    [SerializeField]
    protected GameObject turretStateEle = null;
    [SerializeField]
    protected GameObject turretStateCanvas = null;

    //vls status
    [SerializeField]
    protected List<VLSStatusEle> vlsStatus;
    [SerializeField]
    protected GameObject vlsStatusEle = null;
    [SerializeField]
    protected GameObject vlsStatusCanvas = null;



    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        sights = new List<TurretTargetEle>();
        status = new List<TurretStateEle>();
        vlsStatus = new List<VLSStatusEle>();
    }

    public void AddCursor(SpaceshipControl ssc)
    {
        if (cursorElement == null || cursorCanvas == null)
            return;
        _cursor = Instantiate(cursorElement, cursorCanvas.transform);
        _cursor.GetComponent<CursorUIElement>().SetTarget(ssc);
    }

    public void AddSight(TurretSeat ts)
    {
        if (sightElement == null || sightCanvas == null)
            return;
        TurretTargetEle temp = Instantiate(sightElement, sightCanvas.transform).GetComponent<TurretTargetEle>();
        temp.SetTarget(ts);
        sights.Add(temp);
    }

    public void AddTurretStatus(TurretSeat ct)
    {
        if (turretStateEle == null || turretStateCanvas == null)
            return;
        TurretStateEle temp = Instantiate(turretStateEle, turretStateCanvas.transform).GetComponent<TurretStateEle>();
        temp.SetTarget(ct);
        status.Add(temp);
        ResetTStateLayout();
    }

    public void AddVLSStatus(VLSSeat vs)
    {
        if (vlsStatusEle == null || vlsStatusCanvas == null)
            return;
        VLSStatusEle temp = Instantiate(vlsStatusEle, vlsStatusCanvas.transform).GetComponent<VLSStatusEle>();
        temp.SetTarget(vs);
        vlsStatus.Add(temp);
        ResetVLSStateLayout();
    }

    protected int SortTurret(TurretStateEle t1,TurretStateEle t2)
    {
        if (t1.Target.slotNum > t2.Target.slotNum)
            return 1;
        else if (t1.Target.slotNum < t2.Target.slotNum)
            return -1;
        else
            return 0;
    }

    protected int SortVLS(VLSStatusEle t1, VLSStatusEle t2)
    {
        if (t1.Target.slotNum > t2.Target.slotNum)
            return 1;
        else if (t1.Target.slotNum < t2.Target.slotNum)
            return -1;
        else
            return 0;
    }

    protected void ResetTStateLayout()
    {
        status.Sort(SortTurret);

        int count = status.Count;
        int maxColomn = count / DefaultPara.TurretStateRaw;
        float midColomn = ((float)maxColomn) / 2.0f;

        List<float> yOffset = new List<float>();
        for (int i = 0; i < maxColomn + 1; i++)
        {
            yOffset.Add((midColomn - ((float)i)) * 30.0f);
        }
        List<Vector2> offset = new List<Vector2>(); 
        int colomn = 0;
        while (count > 0)
        {
            int raw = Mathf.Min(DefaultPara.TurretStateRaw, count);
            float midRaw = ((float)raw) / 2.0f - 0.5f;
            for(int i = 0; i < raw; i++)
            {
                offset.Add(new Vector2((((float)i) - midRaw) * 130.0f, yOffset[colomn]));
            }
            ++colomn;
            count -= raw;
        }

        for(int i = 0; i < status.Count; ++i)
        {
            status[i].GetComponent<RectTransform>().localPosition = new Vector3(offset[i].x, offset[i].y, 0);
        }
    }

    public void HideCursor()
    {
        cursorCanvas.enabled = false;
    }

    public void ShowCursor()
    {
        cursorCanvas.enabled = true;
    }

    public void HideScope()
    {
        scopeCanvas.enabled = false;
    }

    public void ShowScope()
    {
        scopeCanvas.enabled = true;
    }

    protected void ResetVLSStateLayout()
    {
        vlsStatus.Sort(SortVLS);

        int count = vlsStatus.Count;
        int maxColomn = count / DefaultPara.TurretStateRaw;
        float midColomn = ((float)maxColomn) / 2.0f;

        List<float> yOffset = new List<float>();
        for (int i = 0; i < maxColomn + 1; i++)
        {
            yOffset.Add((midColomn - ((float)i)) * 30.0f);
        }
        List<Vector2> offset = new List<Vector2>();
        int colomn = 0;
        while (count > 0)
        {
            int raw = Mathf.Min(DefaultPara.TurretStateRaw, count);
            float midRaw = ((float)raw) / 2.0f - 0.5f;
            for (int i = 0; i < raw; i++)
            {
                offset.Add(new Vector2((((float)i) - midRaw) * 130.0f, yOffset[colomn]));
            }
            ++colomn;
            count -= raw;
        }

        for (int i = 0; i < vlsStatus.Count; ++i)
        {
            vlsStatus[i].GetComponent<RectTransform>().localPosition = new Vector3(offset[i].x, offset[i].y, 0);
        }
    }

}
