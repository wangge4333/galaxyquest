﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretStateEle : MonoBehaviour
{
    [SerializeField]
    protected TurretSeat target = null;

    [SerializeField]
    protected Image loading = null;
    [SerializeField]
    protected Text second = null;

    public TurretSeat Target { get => target; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null || target.Turret == null)
            return;
        UpdateInfo();
    }

    protected void Initialization()
    {

    }

    public void SetTarget(TurretSeat ct)
    {
        target = ct;
        Initialization();
    }

    protected void UpdateInfo()
    {
        loading.fillAmount = target.Turret.GetLoadState();
        second.text = target.Turret.GetRestLoadTime();
        if (target.Turret.HStatus == HorizontalStatus.ONTARGET &&
            target.Turret.VStatus == VerticalStatus.ONTARGET &&
            target.Turret.GetLoadState() == 1) 
        {
            loading.color = DefaultPara.ReadyColor;
        }
        else
        {
            loading.color = DefaultPara.UnreadyColor;
        }
    }
}
