﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretTargetEle : MonoBehaviour
{
    protected TurretSeat target = null;
    protected Image sight = null;
    protected RectTransform rectTran = null;
    protected GameManager gameManager = null;

    // Start is called before the first frame update
    void Start()
    {
        //Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void FixedUpdate()
    {
        if (target == null || target.Turret == null)
            return;
        UpdateSight();
    }

    protected void Initialization()
    {
        gameManager = FindObjectOfType<GameManager>();
        rectTran = GetComponent<RectTransform>();
        sight = GetComponent<Image>();
    }

    public void SetTarget(TurretSeat ct)
    {
        target = ct;
        Initialization();
    }

    protected void UpdateSight()
    {
        sight.enabled = true;

        if (Algo.IsOppoDirection(gameManager.MainCamera.transform.forward,
            target.Turret.TargetPos - gameManager.MainCamera.transform.position))
        {
            sight.enabled = false;
            return;
        }

        Vector3 temp = Camera.main.WorldToScreenPoint(target.Turret.TargetPos);
        temp.z = 0;
        rectTran.position = temp;

        if (target.Turret.HStatus == HorizontalStatus.ONTARGET&&
            target.Turret.VStatus == VerticalStatus.ONTARGET)
        {
            sight.color = DefaultPara.ReadyColor;
        }
        else
        {
            sight.color = DefaultPara.UnreadyColor;
        }
    }
}
