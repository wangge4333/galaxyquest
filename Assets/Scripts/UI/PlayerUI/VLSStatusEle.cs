﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VLSStatusEle : MonoBehaviour
{
    [SerializeField]
    protected Image loadedBar = null;
    [SerializeField]
    protected Text cdTime = null;
    [SerializeField]
    protected Text restSum = null;
    [SerializeField]
    protected Text longReload = null;

    [SerializeField]
    protected GameObject gap = null;
    [SerializeField]
    protected Transform inBoxTrans = null;

    protected VLSSeat target = null;
    public VLSSeat Target { get => target; }


    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null || target._VLS == null)
            return;
        UpdataInfo();
    }

    protected void Initialization()
    {
        
    }

    protected void AddGap()
    {
        if (target == null || target._VLS == null)
            return;
        for(int i = 1; i < target._VLS.MissileCapacity; ++i)
        {
            float x = i * GetComponent<RectTransform>().rect.width / target._VLS.MissileCapacity;
            Instantiate(gap, inBoxTrans).GetComponent<RectTransform>().anchoredPosition= new Vector3(x, -5, 0);
        }
    }

    public void SetTarget(VLSSeat _vls)
    {
        target = _vls;
        AddGap();
    }

    protected void UpdataInfo()
    {
        if (target._VLS.UsingMissile == null)
        {
            cdTime.enabled = false;
            restSum.enabled = false;
            longReload.enabled = true;
            longReload.text = "No Missiles!";
            loadedBar.color = DefaultPara.UnreadyColor;
            return;
        }

        if (!target._VLS.InLongReloading)
        {
            cdTime.enabled = true;
            restSum.enabled = true;
            loadedBar.fillAmount = (float)target._VLS.RestMissiles / target._VLS.MissileCapacity;
            cdTime.text = target._VLS.RestLoadingTime.ToString("#0.0") + "s";
            restSum.text = target._VLS.RestMissiles.ToString() + " / " + target._VLS.MissileCapacity;
            longReload.enabled = false;
        }
        else
        {
            longReload.enabled = true;
            longReload.text = target._VLS.RestLoadingTime.ToString("#0.0");
            loadedBar.fillAmount = 1.0f - target._VLS.RestLoadingTime / target._VLS.BaseLongLoading;
            cdTime.enabled = false;
            restSum.enabled = false;
        }
        if (target._VLS.IsLoaded)
        {
            cdTime.enabled = false;
            longReload.enabled = false;
        }

        if (target._VLS.FirePermission)
            loadedBar.color = DefaultPara.ReadyColor;
        else
            loadedBar.color = DefaultPara.UnreadyColor;

    }
}
