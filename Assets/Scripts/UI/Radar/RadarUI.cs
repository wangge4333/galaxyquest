﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarUI : MonoBehaviour
{
    protected LockOnBox lockOnBox = null;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        lockOnBox = GetComponentInChildren<LockOnBox>();
    }

    public void RegisterLockOnBox(FireControl fc)
    {
        lockOnBox.SetTarget(fc);
    }
}
