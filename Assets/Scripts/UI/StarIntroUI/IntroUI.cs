﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroUI : MonoBehaviour
{
    protected UIManager uiManager = null;
    protected Star_SciInfo target = null;

    [SerializeField]
    protected Text starName = null;

    [SerializeField]
    protected Text baseInfo = null;

    [SerializeField]
    protected Text brief = null;

    [SerializeField]
    protected Button cancel = null;

    [SerializeField]
    protected SlideInExpand introPanel = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected void Initialization()
    {
        uiManager = FindObjectOfType<UIManager>();
    }

    public void SetTargetAndOpen(Star_SciInfo target)
    {
        if (target == null)
            return;
        introPanel.gameObject.SetActive(true);
        this.target = target;
        UpdateInfo();
    }

    protected void UpdateInfo()
    {
        if (target == null)
            return;
        starName.text = target.starName;

        baseInfo.text = "Aphelion : " + target.aphelion + '\n' +
            "Perihelion : " + target.perihelion + '\n' +
            "Orbital Period : " + target.orbitalPeriod + '\n' +
            "Orbital Inclination : " + target.inclination + '\n' + '\n' +

            "Mean Radius : " + target.meanRadius + '\n' +
            "Mass : " + target.mass + '\n' +
            "Surface Gravity : " + target.surfaceGravity + '\n' +
            "Rotation Period : " + target.rotationPeriod + '\n' +
            "Axial Tilt : " + target.axialTilt;

        brief.text = target.brief;
    }

    protected void ResetInfo()
    {
        starName.text = "";
        baseInfo.text = "";
        brief.text = "";
    }

    public void ResetAndHide()
    {
        introPanel.StartOutAnimation();
        target = null;
        ResetInfo();
    }

}
