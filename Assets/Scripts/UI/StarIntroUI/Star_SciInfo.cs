﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star_SciInfo : MonoBehaviour
{
    public string starName = "";

    //revolution info
    public string aphelion = "";
    public string perihelion = "";
    public string orbitalPeriod = "";
    public string inclination = "";

    //self info
    public string meanRadius = "";
    public string mass = "";
    public string surfaceGravity = "";
    public string rotationPeriod = "";
    public string axialTilt = "";

    [TextArea]
    public string brief = "";

    protected void Start()
    {
        ReplaceText();
    }

    protected void ReplaceText()
    {
        for (int i = 0; i < 10; ++i)
        {
            string c = "^" + i.ToString();

            aphelion = aphelion.Replace(c, GetUnicode(i));
            perihelion = perihelion.Replace(c, GetUnicode(i));
            orbitalPeriod = orbitalPeriod.Replace(c, GetUnicode(i));
            inclination = inclination.Replace(c, GetUnicode(i));
            meanRadius = meanRadius.Replace( c, GetUnicode(i));
            mass = mass.Replace(c, GetUnicode(i));
            surfaceGravity = surfaceGravity.Replace(c, GetUnicode(i));
            rotationPeriod = rotationPeriod.Replace(c, GetUnicode(i));
            axialTilt = axialTilt.Replace(c, GetUnicode(i));
            brief = brief.Replace(c, GetUnicode(i));
        }
    }

    protected string GetUnicode(int i)
    {
        switch (i)
        {
            case 0:
                return "\u2070";
            case 1:
                return "\u00B9";
            case 2:
                return "\u00B2";
            case 3:
                return "\u00B3";
            case 4:
                return "\u2074";
            case 5:
                return "\u2075";
            case 6:
                return "\u2076";
            case 7:
                return "\u2077";
            case 8:
                return "\u2078";
            case 9:
                return "\u2079";
            default:
                return "";
        }
    }
}
