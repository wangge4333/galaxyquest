﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddStar : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        AddIntoUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void AddIntoUI()
    {
        FindObjectOfType<UIManager>().RegisterStar(gameObject);
    }
}
