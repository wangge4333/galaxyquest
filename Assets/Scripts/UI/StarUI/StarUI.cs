﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarUI : MonoBehaviour
{
    [SerializeField]
    protected GameObject UIElement = null;

    protected List<StarUIEle> stars;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        stars = new List<StarUIEle>();
    }

    public void AddStarUI(GameObject star)
    {
        StarUIEle temp = Instantiate(UIElement, transform).GetComponent<StarUIEle>();
        temp.SetUIElement(star);
        stars.Add(temp);
    }
}
