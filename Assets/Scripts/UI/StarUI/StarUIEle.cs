﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarUIEle : MonoBehaviour
{
    [SerializeField]
    protected GameObject star = null;
    protected Renderer starRenderer = null;

    protected RectTransform rectTran = null;
    protected GameManager gameManager = null;

    [SerializeField]
    protected Text nameOfStar = null;
    [SerializeField]
    protected Text distance = null;

    [SerializeField]
    protected Vector2 offset = new Vector2(0, 0);

    [SerializeField]
    protected float sateDis = 1000.0f;

    [SerializeField]
    protected LayerMask coverLayer = new LayerMask();

    // Start is called before the first frame update
    void Start()
    {
        //Initialization();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (star == null)
            return;
        SetVisible();
        UpdateInfo();
    }

    protected void Initialization()
    {
        starRenderer = star.GetComponent<Renderer>();
        rectTran = GetComponent<RectTransform>();
        gameManager = FindObjectOfType<GameManager>();
    }

    public void SetUIElement(GameObject obj)
    {
        star = obj;
        Initialization();
    }

    protected void SetVisible()
    {
        nameOfStar.enabled = true;
        distance.enabled = true;

        if(Vector3.Dot(gameManager.MainCamera.transform.forward,
            star.transform.position - gameManager.MainCamera.transform.position) < 0)
        {
            nameOfStar.enabled = false;
            distance.enabled = false;
            return;
        }

        if (star.tag == "Satellite" &&
            (star.transform.position - gameManager.MainPlayer.transform.position).magnitude > sateDis) 
        {
            nameOfStar.enabled = false;
            distance.enabled = false;
            return;
        }

        RaycastHit temp;
        Ray ray = new Ray(star.transform.position,
            gameManager.MainPlayer.transform.position - star.transform.position);
        if (Physics.Raycast(ray, out temp,
            (gameManager.MainPlayer.transform.position - star.transform.position).magnitude, coverLayer))   
        {
            nameOfStar.enabled = false;
            distance.enabled = false;
        }
    }

    protected void UpdateInfo()
    {
        if (nameOfStar.enabled == false)
            return;

        Vector3 temp = Camera.main.WorldToScreenPoint(star.transform.position +
            new Vector3(0, starRenderer.bounds.size.y / 2.0f, 0));

        rectTran.position = new Vector3(temp.x + offset.x, temp.y + offset.y, 0);

        nameOfStar.text = star.name;
        distance.text = ((star.transform.position -
            gameManager.MainPlayer.transform.position).magnitude *
            DefaultPara.ToKM).ToString("#0.") + " kM";


    }
}
