﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SlideDir
{
    Horizontal,
    Vertical
}

public class SlideInExpand : MonoBehaviour
{
    public float slideTime = 0.5f;
    public float waitTime = 0.5f;
    public float expandTime = 0.5f;
    [SerializeField]
    protected float timer = 0.0f;

    public SlideDir dir = SlideDir.Horizontal;

    [Range(0.001f, 0.1f)]
    public float startScale = 0.005f;
    public Vector3 endScale = new Vector3(1, 1, 1);

    public Vector3 startPositon = new Vector3(0, 0, 0);
    public Vector3 endPositon = new Vector3(0, 0, 0);

    protected RectTransform trans = null;

    protected Vector3 _startScale;
    protected Vector3 _endScale;

    [SerializeField]
    protected bool slideEnd = true;
    [SerializeField]
    protected bool expandEnd = true;

    [SerializeField]
    protected bool slideOut = true;
    [SerializeField]
    protected bool shrinkOut = true;

    // Start is called before the first frame update
    void Start()
    {
        trans = GetComponent<RectTransform>();
        //StartAnimation();
    }

    protected void OnEnable()
    {
        StartAnimation();
    }

    // Update is called once per frame
    void Update()
    {
        if (!slideEnd || !expandEnd)
            Animate();
        if (!slideOut || !shrinkOut)
            OutAnimate();

    }

    public void StartAnimation()
    {
        timer = 0;
        slideEnd = false;
        expandEnd = false;
        GetComponent<RectTransform>().localPosition = startPositon;
        switch (dir)
        {
            case SlideDir.Horizontal:
                GetComponent<RectTransform>().localScale = new Vector3(1, startScale, 1);
                break;
            case SlideDir.Vertical:
                GetComponent<RectTransform>().localScale = new Vector3(startScale, 1, 1);
                break;
            default:
                break;
        }
        _startScale = GetComponent<RectTransform>().localScale;
    }

    public void StartOutAnimation()
    {
        slideOut = false;
        shrinkOut = false;
    }

    protected void Animate()
    {
        timer += Time.deltaTime;
        if(!slideEnd)
            trans.localPosition = /*endPositon +*/
                (1.0f - Mathf.Min(slideTime, timer) / slideTime) * (startPositon - endPositon);
        if (!expandEnd)
            trans.localScale = endScale +
                (1.0f - Mathf.Min(expandTime, Mathf.Max(0, timer - waitTime - slideTime)) / expandTime) *
                (_startScale - endScale);
        if (timer > slideTime)
            slideEnd = true;
        if (timer > (slideTime + waitTime + expandTime))
            expandEnd = true;
    }

    protected void OutAnimate()
    {
        timer -= Time.deltaTime;
        if (!slideOut)
            trans.localPosition = /*endPositon +*/
                (1.0f - Mathf.Min(slideTime, timer) / slideTime) * (startPositon - endPositon);
        if (!shrinkOut)
            trans.localScale = endScale +
                (1.0f - Mathf.Min(expandTime, Mathf.Max(0, timer - waitTime - slideTime)) / expandTime) *
                (_startScale - endScale);
        if (timer <= 0)
            slideOut = true;
        if (timer <= (slideTime + waitTime))
            shrinkOut = true;
        if(slideOut && shrinkOut)
        {
            gameObject.SetActive(false);
        }
    }


}
