﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    protected Text uni_Hint = null;

    protected InputManager inputManager = null;
    protected GameManager gameManager = null;

    protected StarUI starUI = null;
    protected PlayerUI playerUI = null;
    protected NPCInfoUI npcInfoUI = null;
    protected PlayerPermUI playerPermUI = null;
    protected RadarUI radarUI = null;
    protected MarketUI marketUI = null;
    protected PlanetMenuUI planetMenuUI = null;
    protected DockUI dockUI = null;
    protected Lab_TechManager labUI = null;
    protected IntroUI introUI = null;

    // Start is called before the first frame update
    void Start()
    {
        Initialization();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Initialization()
    {
        inputManager = FindObjectOfType<InputManager>();
        gameManager = FindObjectOfType<GameManager>();

        starUI = GetComponentInChildren<StarUI>();
        playerUI = GetComponentInChildren<PlayerUI>();
        npcInfoUI = GetComponentInChildren<NPCInfoUI>();
        playerPermUI = GetComponentInChildren<PlayerPermUI>();
        radarUI = GetComponentInChildren<RadarUI>();
        marketUI = GetComponentInChildren<MarketUI>();
        planetMenuUI = GetComponentInChildren<PlanetMenuUI>();
        dockUI = GetComponentInChildren<DockUI>();
        labUI = GetComponentInChildren<Lab_TechManager>();
        introUI = GetComponentInChildren<IntroUI>();
    }

    public void RegisterCursor(SpaceshipControl ssc)
    {
        playerUI.AddCursor(ssc);
    }

    public void RegisterStar(GameObject obj)
    {
        starUI.AddStarUI(obj);
    }

    public void RegisterPlayerSimpleInfo(SpaceObject so)
    {

    }

    public void RegisterNPCSimpleInfo(SpaceObject so)
    {
        npcInfoUI.RegisterNPC(so);
    }

    public void DeregisterNPCSimpleInfo(SpaceObject so)
    {
        npcInfoUI.DeregisterNPC(so);
    }

    public void RegisterCannonTurret(TurretSeat turret)
    {
        playerUI.AddSight(turret);
        playerUI.AddTurretStatus(turret);
    }

    public void RegisterVLS(VLSSeat vls)
    {
        playerUI.AddVLSStatus(vls);
    }

    public void RegisterSpaceship(SpaceObject spaceship)
    {
        if (spaceship.tag == "Player")
        {
            RegisterCursor(spaceship.GetComponent<SpaceshipControl>());
            RegisterPlayerSimpleInfo(spaceship.GetComponent<SpaceObject>());
            playerPermUI.SetTarget(spaceship);
            radarUI.RegisterLockOnBox(spaceship.GetComponent<FireControl>());
            foreach (TurretSeat ts in spaceship.GetComponent<FireControl>().MainWeapons)
                RegisterCannonTurret(ts);
            foreach (VLSSeat vs in spaceship.GetComponent<FireControl>().VLS)
                RegisterVLS(vs);

            return;
        }
        if (spaceship.tag == "Enemy")
        {
            //RegisterNPCSimpleInfo(spaceship);
            return;
        }
        if (spaceship.tag == "NPC")
        {
            //RegisterNPCSimpleInfo(spaceship);
            return;
        }
    }

    public void ShowHint(string hint)
    {
        uni_Hint.text = hint;
    }

    public void PlayerLock(SpaceObject so)
    {
        npcInfoUI.Lock(so);
    }

    public void PlayerUnlock(SpaceObject so)
    {
        npcInfoUI.Unlock(so);
    }

    public void OpenPlanetMenu(GameObject _planet, int _playerID)
    {
        planetMenuUI.SetTarget(_planet, _playerID);
        FreezeControl();
    }

    public void ClosePlanetMenu()
    {
        planetMenuUI.ResetAndHide();
        UnfreezeControl();
    }

    public void OpenMarketUI(PlanetMarket market,int targetID)
    {
        marketUI.SetTargetAndOpen(market, targetID);
    }

    public void CloseMarketUI()
    {
        marketUI.ResetAndHide();
    }

    public void OpenDockUI(SpaceObject player)
    {
        dockUI.SetTargetAndOpen(player);
    }

    public void CloseDockUI()
    {
        dockUI.ResetAndHide();
    }

    public void OpenLabUI(GQ_UpgradeTree player)
    {
        labUI.SetTargetAndOpen(player);
    }

    public void CloseLabUI()
    {
        labUI.ResetAndHide();
    }

    public void OpenIntroUI(Star_SciInfo star)
    {
        introUI.SetTargetAndOpen(star);
    }

    public void CloseIntroUI()
    {
        introUI.ResetAndHide();
    }

    public void FreezeShipControl()
    {
        inputManager.FreezeShipControl = true;
    }

    public void UnfreezeShipControl()
    {
        inputManager.FreezeShipControl = false;
    }

    //include camera
    public void FreezeControl()
    {
        inputManager.FreezeControl = true;
        Cursor.lockState = CursorLockMode.None;
    }

    //include camera
    public void UnfreezeControl()
    {
        inputManager.FreezeControl = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ShowAimCursor()
    {
        playerUI.ShowCursor();
    }

    public void HideAimCursor()
    {
        playerUI.HideCursor();
    }

    public void ShowScope()
    {
        playerUI.ShowScope();
    }

    public void HideScope()
    {
        playerUI.HideScope();
    }

}
