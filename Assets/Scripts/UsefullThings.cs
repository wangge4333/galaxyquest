﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Algo
{
    public static int FindLowerBound(List<float> _list,float element)
    {
        return FindLowerBound(_list, element, 0, _list.Count);
    }

    public static int FindLowerBound(List<float> _list, float element,int start,int end)
    {
        if (start == end)
            return start - 1;
        int mid = (start + end) / 2;
        if (element >= _list[mid])
            return FindLowerBound(_list, element, mid + 1, end);
        else
            return FindLowerBound(_list, element, start, mid);
    }

    //if min > max, it means another side
    //all between 0.0f ~ 359.999f, inputted means avalible angle
    public static bool IsInAvaAngle(float angle,float start,float end)
    {
        if (start < end)
        {
            if (angle >= start && angle <= end)
                return true;
            else
                return false;
        }
        else if (start > end)
        {
            if ((angle >= start && angle < 360.0f) || (angle <= end && angle >= 0))
                return true;
            else
                return false;
        }
        else
            return false;
    }

    public static bool IsOppoDirection(Vector3 vec_1,Vector3 vec_2)
    {
        if (Vector3.Dot(vec_1, vec_2) < 0)
            return true;
        return false;
    }

    public static bool IfClockWise(float start, float end)
    {
        float temp = start - end;
        if (temp < 0)
        {
            if (Mathf.Abs(temp) <= 180.0f)
                return true;
            else
                return false;
        }
        else
        {
            if (temp >= 180.0f)
                return true;
            else
                return false;
        }
    }

    static System.Random rand = new System.Random();

    //标准正态分布
    protected static float Normal()
    {
        float s = 0.0f, u = 0.0f, v = 0.0f;
        while (s > 1 || s == 0)
        {
            u = (float)rand.NextDouble() * 2.0f - 1.0f;
            v = (float)rand.NextDouble() * 2.0f - 1.0f;

            s = u * u + v * v;
        }

        var z = Mathf.Sqrt(-2 * Mathf.Log(s) / s) * u;
        return (z);
    }

    //符合要求的正态分布随机数
    public static float StandardNormalRandom(float miu, float sigma)
    {
        float z = Normal() * sigma + miu;
        return z;
    }

    public static float RandomFloat(float min,float max)
    {
        return UnityEngine.Random.Range(min, max);
    }

    //动对动无导引弹道预测
    /// <summary>
    /// 
    /// </summary>
    /// <param name="relaVelocity"></param>
    /// <param name="originPos"></param>
    /// <param name="targetPos"></param>
    /// <param name="speed">speed in unity, not km/s</param>
    /// <returns></returns>
    public static Vector3 D2DBallisticPre(Vector3 relaVelocity, Vector3 originPos,Vector3 targetPos,float speed)
    {
        Vector3 posDiff = targetPos - originPos;
        float temp_1 = relaVelocity.x * posDiff.x + relaVelocity.y * posDiff.y + relaVelocity.z * posDiff.z;
        float temp_2 = Mathf.Pow(speed, 2) -
            relaVelocity.x * relaVelocity.x - relaVelocity.y * relaVelocity.y - relaVelocity.z * relaVelocity.z;
        float preTime = Mathf.Sqrt((posDiff.x * posDiff.x + posDiff.y * posDiff.y + posDiff.z * posDiff.z) / temp_2
            + Mathf.Pow(temp_1 / temp_2, 2)) + temp_1 / temp_2;
        return targetPos + preTime * relaVelocity;
    }

    //有导引弹道简单预测
    /// <summary>
    /// 
    /// </summary>
    /// <param name="originPos"></param>
    /// <param name="targetPos"></param>
    /// <param name="speed">speed in unity, not km/s</param>
    /// <returns></returns>
    public static Vector3 GuidedBallisticPreSimple(Vector3 targetSpeed, Vector3 originPos, Vector3 targetPos, float speed)
    {
        float preTime = (targetPos - originPos).magnitude / speed;
        return targetPos + preTime * targetSpeed;
    } 

    public static bool IsApproximate(Vector3 v1,Vector3 v2, float difference)
    {
        if (Vector3.Distance(v1, v2) < difference) 
            return true;
        return false;
    }

    public static float ABS(float value)
    {
        return value > 0 ? value : -value;
    }
}


public class DefaultPara
{
    protected static float toKM = 30.0f;
    public static float ToKM { get => toKM; }

    protected static float farAIM = 20.0f;
    public static float FarAIM { get => farAIM; }

    protected static float lookUP = -10.0f;
    public static float LookUP { get => lookUP; }

    protected static Color readyColor = new Color(47.0f / 255.0f, 219.0f/255.0f, 213.0f / 255.0f, 0.8f);
    public static Color ReadyColor { get => readyColor; }

    protected static Color unreadyColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
    public static Color UnreadyColor { get => unreadyColor; }

    protected static Color itemUIColor =
        new Color(142.0f / 255.0f, 157.0f / 255.0f, 200.0f / 255.0f, 192.0f / 255.0f);
    public static Color ItemUIColor { get => itemUIColor; }

    protected static Color altItemUIColor = 
        new Color(203.0f / 255.0f, 205.0f / 255.0f, 241.0f / 255.0f, 192.0f / 255.0f);
    public static Color AltItemUIColor { get => altItemUIColor; }

    public static Color AvailableColour = new Color(108.0f / 255.0f, 90.0f / 255.0f, 241.0f / 255.0f, 192.0f / 255.0f);
    public static Color UnavaColour = new Color(100.0f / 255.0f, 100.0f / 255.0f, 100.0f / 255.0f, 128.0f / 255.0f);


    protected static int turretStateRaw = 6;
    public static int TurretStateRaw { get => turretStateRaw; }

    protected static float showInfoDistance = 50.0f;
    public static float ShowInfoDistance { get => showInfoDistance; }

    protected static float hideInfoDistance = 60.0f;
    public static float HideInfoDistance { get => hideInfoDistance; }

    protected static float lowestPriceFactor = 0.3f;
    public static float LowestPriceFactor { get => lowestPriceFactor; }

    protected static float outputSigma = 0.07f;
    public static float OutputSigma { get => outputSigma; }

    protected static float priceUpFactor = 1.15f;
    public static float PriceUpFactor { get => priceUpFactor; }

    protected static string mps = " m/s";
    public static string MPS { get => mps; }

    protected static string kmps = " kM/s";
    public static string KMPS { get => kmps; }
}

public class GQ_Path
{
    protected static string shipPrefabs = "Prefabs/Ships/";
    public static string ShipPrefabs { get => shipPrefabs; }

    protected static string weaponPrefabs = "Prefabs/Weapon/";
    public static string WeaponPrefabs { get => weaponPrefabs; }
}

public class RatePairs<T>
{
    //
    protected List<T> items = null;
    protected List<float> rateArea = null;

    protected float totalRate = 0;

    public RatePairs()
    {
        items = new List<T>();
        rateArea = new List<float>();
        rateArea.Add(0);
        totalRate = 0;
    }

    public void AddPair(T item,float rate)
    {
        items.Add(item);
        if (rateArea.Count == 0)
            rateArea.Add(rate);
        else
            rateArea.Add(rateArea[rateArea.Count - 1] + rate);
        totalRate += rate;
    }

    public void Clear()
    {
        items.Clear();
        rateArea.Clear();
        totalRate = 0;
    }

    public T GetRandomResult() 
    {
        float temp = Algo.RandomFloat(0.0f, totalRate);
        return items[Algo.FindLowerBound(rateArea, temp)];
    }

}