﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WorldAdjust : MonoBehaviour
{
    public Transform mainTarget = null;
    [SerializeField]
    protected List<GameObject> rootObjs = null;

    public float adjustUnit = 1000.0f;

    public Transform roundRoot = null;
    public Transform NPCRoot = null;

    Vector3 adjustFactor = new Vector3(0, 0, 0);

    protected void Awake()
    {
        CheckPositions();
    }

    // Start is called before the first frame update
    void Start()
    {
        SceneManager.GetActiveScene().GetRootGameObjects(rootObjs);
        foreach(GameObject obj in rootObjs)
        {
            if (obj.tag == "RoundRoot")
                roundRoot = obj.transform;
            if (obj.tag == "NPCRoot")
                NPCRoot = obj.transform;
        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    protected void LateUpdate()
    {
        CheckPositions();
    }

    protected void CheckPositions()
    {
        if (mainTarget.position.x > adjustUnit)
        {
            adjustFactor += (new Vector3(-adjustUnit, 0, 0)) * (Mathf.FloorToInt(mainTarget.position.x / adjustUnit));
        }
        if (mainTarget.position.x < -adjustUnit)
        {
            adjustFactor += (new Vector3(adjustUnit, 0, 0)) * (Mathf.FloorToInt(-mainTarget.position.x / adjustUnit));
        }
        if (mainTarget.position.y > adjustUnit)
        {
            adjustFactor += ((new Vector3(0, -adjustUnit, 0)) * (Mathf.FloorToInt(mainTarget.position.y / adjustUnit)));
        }
        if (mainTarget.position.y < -adjustUnit)
        {
            adjustFactor += ((new Vector3(0, adjustUnit, 0)) * (Mathf.FloorToInt(-mainTarget.position.y / adjustUnit)));
        }
        if (mainTarget.position.z > adjustUnit)
        {
            adjustFactor += ((new Vector3(0, 0, -adjustUnit)) * (Mathf.FloorToInt(mainTarget.position.z / adjustUnit)));
        }
        if (mainTarget.position.z < -adjustUnit)
        {
            adjustFactor += ((new Vector3(0, 0, adjustUnit)) * (Mathf.FloorToInt(-mainTarget.position.z / adjustUnit)));
        }

        AdjustPositions(adjustFactor);
    }

    protected void AdjustPositions(Vector3 factor)
    {
        //mainTarget.position += factor;
        foreach(GameObject obj in rootObjs)
        {
            if (obj == null)
                continue;
            if (obj.tag == "Unmovable")
                continue;
            obj.transform.position += factor;
        }
        adjustFactor = Vector3.zero;
    }
}
